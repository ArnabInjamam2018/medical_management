﻿/*
 Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 For licensing, see LICENSE.md or http://ckeditor.com/license
*/
// CKEDITOR.dialog.add( 'image2', function( editor ) {
//     return {
//         title:          'Test Dialog',
//         resizable:      CKEDITOR.DIALOG_RESIZE_BOTH,
//         minWidth:       500,
//         minHeight:      400,
//         contents: [
//             {
//                 id:         'tab1',
//                 label:      'First Tab',
//                 title:      'First Tab Title',
//                 accessKey:  'Q',
//                 elements: [
//                     {
//                         type:           'text',
//                         label:          'Test Text 1',
//                         id:             'testText1',
//                         'default':      'hello world!'
//                     }
//                 ]
//             }
//         ]
//     }
// });
