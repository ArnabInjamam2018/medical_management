<!--sidebar end-->
<!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <section class="col-md-7 row">
            <header class="panel-heading">
                <?php
                if (!empty($doctor->id))
                    echo lang('edit_doctor');
                else
                    echo lang('add_doctor');
                ?>
            </header> 
            <div class="panel-body">
                <div class="adv-table editable-table ">
                    <div class="clearfix">
                        <div class="col-lg-12">
                            <div class="col-lg-3"></div>
                            <div class="col-lg-6">
                                <?php echo validation_errors(); ?>
                                <?php echo $this->session->flashdata('feedback'); ?>
                            </div>
                            <div class="col-lg-3"></div>
                        </div>
                        <form role="form" action="doctor/addNew" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="name"><?php echo lang('name'); ?></label>
                                <input type="text" class="form-control" name="name" id="name" value='<?php
                                if (!empty($setval)) {
                                    echo set_value('name');
                                }
                                if (!empty($doctor->name)) {
                                    echo $doctor->name;
                                }
                                ?>' placeholder="">
                            </div>
                            
                   
                            <div class="form-group">
                                <label for="designation"><?php echo lang('designation'); ?></label>
                                <input type="text" class="form-control" name="designation" id="designation" value='<?php
                                if (!empty($setval)) {
                                    echo set_value('designation');
                                }
                                if (!empty($doctor->designation)) {
                                    echo $doctor->designation;
                                }
                                ?>' placeholder="">
                                
                            </div>
                       
                            <div class="form-group">
                                <label for="degree"><?php echo lang('degree'); ?></label>
                                <input type="text" class="form-control" name="degree" id="degree" value='<?php
                                if (!empty($setval)) {
                                    echo set_value('degree');
                                }
                                if (!empty($doctor->degree)) {
                                    echo $doctor->degree;
                                }
                                ?>' placeholder="">
                                
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1"><?php echo lang('image'); ?></label>
                                <input type="file" name="img_url">
                            </div>
                            <input type="hidden" name="id" value='<?php
                            if (!empty($doctor->id)) {
                                echo $doctor->id;
                            }
                            ?>'>
                            <button type="submit" name="submit" class="btn btn-info"><?php echo lang('submit'); ?></button>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
<!--footer start-->
