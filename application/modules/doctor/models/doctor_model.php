<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Doctor_model extends CI_model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function insertDoctor($data) {
        $this->db->insert('doctor', $data);
    }

    function getDoctor() {
        $query = $this->db->get('doctor');
        return $query->result();
    }

    function getDoctorByOPD() {
        $this->db->where('opd_flag', 1);
        $query = $this->db->get('doctor');
        return $query->result();
    }

    function getTimeSlotsDoctor($adoctors_id) {
        $this->db->select('opd_time_slot');
        $this->db->from('doctor');
        $this->db->where('id', $adoctors_id);
        $query = $this->db->get();
        return $query->row();
    }



public function getreg_no($reg_no,$patient)

{ 
    $this->db->select('reg_no');
    $this->db->where('reg_no', $reg_no);
    $this->db->where('patient_id', $patient);
    $query = $this->db->get("registration");

    return $query->row();
    // $str = $this->db->last_query();
   


}
    function getDoctorBySearch($search) {
        $this->db->order_by('id', 'desc');
        $this->db->like('id', $search);
        $this->db->or_like('name', $search);
        $this->db->or_like('phone', $search);
        $this->db->or_like('address', $search);
        $this->db->or_like('email', $search);
        $this->db->or_like('department', $search);
        $query = $this->db->get('doctor');
        return $query->result();
    }

    function getDoctorByLimit($limit, $start) {
        $this->db->order_by('id', 'desc');
        $this->db->limit($limit, $start);
        $query = $this->db->get('doctor');
        return $query->result();
    }

    function getDoctorByLimitBySearch($limit, $start, $search) {

        $this->db->like('id', $search);

        $this->db->order_by('id', 'desc');

        $this->db->or_like('name', $search);
        $this->db->or_like('phone', $search);
        $this->db->or_like('address', $search);
        $this->db->or_like('email', $search);
        $this->db->or_like('department', $search);

        $this->db->limit($limit, $start);
        $query = $this->db->get('doctor');
        return $query->result();
    }

    function getDoctorById($id) 
    {
        $this->db->where('id', $id);
        $query = $this->db->get('doctor');
        return $query->row();
    }
    
    function getDoctorByIonUserId($id) {
        $this->db->where('ion_user_id', $id);
        $query = $this->db->get('doctor');
        return $query->row();
    }




    function updateDoctor($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('doctor', $data);
      
    }

    function delete($id) {
        $this->db->where('id', $id);
        $this->db->delete('doctor');
    }

    function updateIonUser($username, $email, $password, $ion_user_id) {
        $uptade_ion_user = array(
            'username' => $username,
            'email' => $email,
            'password' => $password
        );
        $this->db->where('id', $ion_user_id);
        $this->db->update('users', $uptade_ion_user);
    }

//...........................................................patrhlogist doctor..............................................................................


function insertPathDoctor($data) {
    $this->db->insert('pathologist_master', $data);
}

function updatePathDoctor($id, $data) {
    $this->db->where('id', $id);
    $this->db->update('pathologist_master', $data);
}

function deletePath($id) {
    $this->db->where('id', $id);
    $this->db->delete('pathologist_master');
}


    function getPathologyDoctor() 
    {
        $query = $this->db->get('pathologist_master');
        //echo $query;die;
        return $query->result();
    }


    function getPathDoctorById($id) {
        $this->db->where('id', $id);
        $query = $this->db->get('pathologist_master');
        return $query->row();
    }


    function getPathDoctorBySearch($search) {
        $this->db->order_by('id', 'desc');
        $this->db->like('id', $search);
        $this->db->or_like('name', $search);
        $this->db->or_like('designation', $search);
        $query = $this->db->get('pathologist_master');
        return $query->result();
    }

    function getPathDoctorByLimit($limit, $start) {
        $this->db->order_by('id', 'desc');
        $this->db->limit($limit, $start);
        $query = $this->db->get('pathologist_master');
        return $query->result();
    }

    function getPathDoctorByLimitBySearch($limit, $start, $search) {

        $this->db->like('id', $search);

        $this->db->order_by('id', 'desc');

        $this->db->or_like('name', $search);;
        $this->db->or_like('designation', $search);

        $this->db->limit($limit, $start);
        $query = $this->db->get('pathologist_master');
        return $query->result();
    }



}
