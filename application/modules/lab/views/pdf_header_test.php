            
        <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial,sans-serif;">
            <tr>
                <td align="center">
                        <div class="report_header text-center" style="text-align:center;">
                            <img alt="" src="<?php echo site_url() . $settings->logo; ?>">
                            <h4 style="font-weight: bold; margin-top: 20px; text-transform: uppercase;margin:5px 0;font-family:'Arial">
                                <?php echo lang('lab_report') ?>
                            </h4>
                        </div>
                </td>
            </tr>  
         </table>
          <hr />
         <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin:0;padding:0;" class="table">
            <tr>
              <td width="50%">
                <p>
                    <?php //$patient_info = $this->db->get_where('patient', array('patient' => $patients_payments['id']))->row(); ?>
                    <label class="control-label"><?php echo lang('patient'); ?> <?php echo lang('name'); ?> </label>
                    <span style="text-transform: uppercase;"> : 
                        <?php
                        if (!empty($patients_payments)) {
                            echo $patients_payments->patient_name . ' <br>';
                        }
                        ?>
                    </span>
                </p>
                <p>
                    <label class="control-label"><?php echo lang('patient_id'); ?>  </label>
                    <span style="text-transform: uppercase;"> : 
                        <?php
                        if (!empty($patients_payments)) {
                            echo $patients_payments->patient . ' <br>';
                        }
                        ?>
                    </span>
                </p>
                <p>
                    <label class="control-label"> <?php echo lang('address'); ?> </label>
                    <span style="text-transform: uppercase;"> : 
                        <?php
                        if (!empty($patients_payments)) {
                            echo $patients_payments->patient_address . ' <br>';
                        }
                        ?>
                    </span>
                </p>
                <p>
                    <label class="control-label"><?php echo lang('phone'); ?>  </label>
                    <span style="text-transform: uppercase;"> : 
                        <?php
                        if (!empty($patients_payments)) {
                            echo $patients_payments->patient_phone . ' <br>';
                        }
                        ?>
                    </span>
                </p>
              </td>
              <td width="50%">
                <p>
                    <label class="control-label"> <?php echo lang('age'); ?>  </label>
                    <span style="text-transform: uppercase;"> : 
                        <?php
                        if (!empty($patients_payments)) {
                            echo $patients_payments->age;
                        }
                        ?>
                    </span>
                </p>
                <p>
                    <label class="control-label"><?php echo lang('date'); ?>  </label>
                    <span style="text-transform: uppercase;"> : 
                        <?php
                        if (!empty($patients_payments->date_string)) {
                            echo date('d-m-Y', $patients_payments->date_string) . ' <br>';
                        }
                        ?>
                    </span>
                </p>
                <p>
                    <label class="control-label"><?php echo lang('doctor'); ?>  </label>
                    <span style="text-transform: uppercase;"> : 
                        <?php
                        if (!empty($patients_payments)) {
                            echo $patients_payments->doctor_name;
                        }
                        ?>
                    </span>
                </p>
              </td>                                
            </tr>
        </table>
        <br>