<div id="pdf-content">
    <div class="report_body"  style="width:100%;">
            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin:0;padding:0;" class="table">
            <tr>
              <td width="50%">
                <p>
                    <?php $patient_info = $this->db->get_where('patient', array('id' => $lab->patient))->row(); ?>
                    <label class="control-label"><?php echo lang('patient'); ?> <?php echo lang('name'); ?> </label>
                    <span style="text-transform: uppercase;"> : 
                        <?php
                        if (!empty($patient_info)) {
                            echo $patient_info->name . ' <br>';
                        }
                        ?>
                    </span>
                </p>
                <p>
                    <label class="control-label"><?php echo lang('patient_id'); ?>  </label>
                    <span style="text-transform: uppercase;"> : 
                        <?php
                        if (!empty($patient_info)) {
                            echo $patient_info->id . ' <br>';
                        }
                        ?>
                    </span>
                </p>
                <p>
                    <label class="control-label"> <?php echo lang('address'); ?> </label>
                    <span style="text-transform: uppercase;"> : 
                        <?php
                        if (!empty($patient_info)) {
                            echo $patient_info->address . ' <br>';
                        }
                        ?>
                    </span>
                </p>
                <p>
                    <label class="control-label"><?php echo lang('phone'); ?>  </label>
                    <span style="text-transform: uppercase;"> : 
                        <?php
                        if (!empty($patient_info)) {
                            echo $patient_info->phone . ' <br>';
                        }
                        ?>
                    </span>
                </p>
              </td>
              <td width="50%">
                <p>
                    <label class="control-label"> <?php echo lang('age'); ?>  </label>
                    <span style="text-transform: uppercase;"> : 
                        <?php
                        if (!empty($patient_info->id)) {
                            echo $patient_info->age;
                        }
                        ?>
                    </span>
                </p>
                <p>
                    <label class="control-label"> <?php echo lang('lab'); ?> <?php echo lang('report'); ?> <?php echo lang('id'); ?>  </label>
                    <span style="text-transform: uppercase;"> : 
                        <?php
                        if (!empty($lab->id)) {
                            echo $lab->id;
                        }
                        ?>
                    </span>
                </p>
                <p>
                    <label class="control-label"><?php echo lang('date'); ?>  </label>
                    <span style="text-transform: uppercase;"> : 
                        <?php
                        if (!empty($lab->date)) {
                            echo date('d-m-Y', $lab->date) . ' <br>';
                        }
                        ?>
                    </span>
                </p>
                <p>
                    <label class="control-label"><?php echo lang('doctor'); ?>  </label>
                    <span style="text-transform: uppercase;"> : 
                        <?php
                        if (!empty($lab->doctor)) {
                            $doctor_details = $this->doctor_model->getDoctorById($lab->doctor);
                            if (!empty($doctor_details)) {
                                echo $doctor_details->name. '<br>';
                            }
                        }
                        ?>
                    </span>
                </p>
              </td>                                
            </tr>
        </table>

        <div class="col-md-12 panel-body" width="100" length="200">
            <?php
            if (!empty($lab->report)) {
                echo $lab->report;
            }
            ?>
        </div> 
    </div>
</div>

<style>

</style>