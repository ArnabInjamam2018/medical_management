<?php //echo "<pre>"; print_r($test_parameter); die;?>
<div id="pdf-content">
	<?php
	 $i = 0;
	foreach($test_parameters as $grouped) {
      ?>
      <?php 

        if($i > 0)

        {
      ?>
      <pagebreak>
      <?php 

        } 
      ?>
    <div class="report_body"  style="width:100%;">
    <table width="100%" cellspacing="5" cellpadding="5" border="0" style="text-align: center;">
        <tr>
            <td align="center">
               <span style="text-transform: uppercase;"> <strong><u>
                    <?php 
        			echo $grouped['group_name'];
        			echo "<br>";
        	       ?>
                    </u>
                   </strong></span>
            </td>
        </tr>
      </table>
      <?php 
          foreach($grouped['test_heading'] as $key => $test_heading) {
        ?>
      <table width="100%" cellspacing="5" cellpadding="5" border="0" style="text-align: center;">
        <tr>
            <td align="center" style="text-align: center; font-weight:bold; text-decoration:undeline;text-transform: uppercase;">
               <span style="text-transform: uppercase;"><strong><u><?php 
        			echo $test_heading;
        			echo "<br>";
        		?>
        	    </u>
                   </strong></span>
            </td>
        </tr>
    </table>
		  
           <table cellspacing="5" cellpadding="5" class="table" width="100%"  style="page-break-before:always">
               <thead>
           		<tr>
           			<th width="35%" align="left">Test Name</th>
           			<th width="25%">Result</th>
           			<th width="15%">Unit</th>
           			<th width="25%" align="left">Ref. Range</th>
           			
           		</tr>
                   </thead>
               
           			<?php

           				foreach($grouped['test_parameter'] as $param)
           				{
      							if($param->test_heading == $test_heading)
                    {
                 			?>
                 			<tbody>
                          <tr>
            	           			<td><?php echo $param->parameter_name; ?></td>
                                       <?php 

                                        if ($param->actual_value_bold == 1) 
                                        {
                                        ?>
            	           			<td align="center"><b><?php echo $param->actual_value; ?></b></td>

                                        <?php    
                                            }
                                            else 
                                            {
                                            ?>
                                            <td align="center"><?php echo $param->actual_value; ?></td>

                                            <?php    
                                                }
                                            ?>

            	           			<td align="center"><?php echo $param->test_unit; ?></td>
                                            
            	           			<td><?php echo $param->reference_range; ?></td>
      	           			
                 			    </tr>
                    </tbody>

                 			<?php
      							}
           				}
           			?>
           </table>
           <br><br><br><br>
		  
           <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin:0;padding:0; page-break-before:always" class="table" >
          <?php 

            if (!empty($grouped['note'][$key])) 
            {
          ?>
            
              <tr>
                <td class="notes">
                  <p>
                         <label class="control-label"><strong><?php echo lang('note'); ?></strong></label>
                          <span> : 
                              <?php
                              
                                  //echo $grouped['note'][$key];
                                  echo preg_replace("/[\n\r]/", '</span></p></td></tr><tr><td class="notes"><p><span>', $grouped['note'][$key]).'<br>';
                              
                              ?>
                          </span>
                      </p><br><br>
                </td>
              </tr>
        <?php 
          
          }
        ?>
        <?php 

          if (!empty($grouped['interpretation'][$key])) 
          {

        ?>
          <tr>
            <td class="notes">
              <p>
                      <label class="control-label"><strong><?php echo lang('interpretations'); ?></strong></label>
                      <span> : 
                          <?php
                            //  echo nl2br($grouped['interpretation'][$key]) . ' <br>';
                            echo preg_replace("/[\n\r]/", '</span></p></td></tr><tr><td class="notes"><p><span>', $grouped['interpretation'][$key]).'<br>';
                          ?>
                      </span>
                    
                  </p>
            </td>
          </tr>
          <?php 

          }
            ?>

 <?php 
/*
if (!empty($grouped['interpretation1'][$key])) 
{

?>
<tr>
  <td class="notes">
    <p>
            <!-- <label class="control-label"><strong><?php //echo lang('interpretations'); ?>1</strong></label> -->
            <span>  
                <?php
                
                    echo nl2br($grouped['interpretation1'][$key]) . ' <br>';
                
                ?>
            </span>
          
        </p><br><br>
  </td>
</tr>
<?php 

}*/
  ?>

            <?php
          if (!empty($grouped['comments'][$key])) 
          {
          ?>
        <tr>
          <td class="notes">
            <p>
                <label class="control-label"><strong><?php echo lang('comments'); ?></strong></label>
                  <span> : 
                      <?php
                        
                         // echo nl2br($grouped['comments'][$key]). ' <br>';
                          echo preg_replace("/[\n\r]/", '</span></p></td></tr><tr><td class="notes"><p><span>', $grouped['comments'][$key]). ' <br>';
                        
                      ?>
                    </span>
                </p><br><br>
          </td>
        </tr>
        <?php 
        }
        ?>
    </table> <br><br>
    <?php } ?>
 <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin:0;padding:0;">
      <tr>
        <?php
//$cnt = count($pathologys);
        
    foreach($pathologys as $pathology)
{
  ?>
          <td width="<?php echo (100 / $cnt); ?>%">  
<?php
if($grouped['group_name'] == $pathology['group_name']){
if($pathology['sig_required'] == 'Y'){
 
    ?>

      
            <div class="imgsize">               
                
                <img style="height:100px;width:100px;" alt="" src="<?php echo site_url() . $pathology['img_url']; ?>"> 
                             
            </div>
            <?php
                }
                 else{
                    ?>
                        <div class="imgsize" style="display:none;">               
                    
                    <img style="height:100px;width:100px;" alt="" src="<?php echo site_url() . $pathology['img_url']; ?>"> 
                             
            </div>
                
                <?php
                }
                ?>
            <div>
                  
                 <?php
                    if (!empty($pathology)) 
                    {
                        echo $pathology['patho_name'] . '';
                    }
                ?>
                    
                </div>


                    <div>
                    
                        <?php
                        if (!empty($pathology)) 
                        {
                            echo $pathology['degree'] . '';
                        }
                        ?>
              
                    </div>

                <div>
                    
                        <?php
                        if (!empty($pathology)) 
                        {
                            echo $pathology['designation'] . '';
                        }
                        ?>
                   
                </div>

            
      
                      <?php }
                      ?>


			
        </td>
<?php
       }
?>
          </tr>
      </table>
    </div>
    <?php
    $i++;
    }?>
</div>
