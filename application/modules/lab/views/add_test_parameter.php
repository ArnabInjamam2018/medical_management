<!--sidebar end-->
<!--main content start-->
<script> var row_cnt = 1; </script>
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <section class="panel col-md-12 no-print">
            <header class="panel-heading no-print">
                <?php
                if (!empty($category->id))
                    echo lang('add_test_parameter');
                else
                    echo lang('add_test_parameter');
                ?>
                <button type="button" id="updateButton" class="btn btn-primary" onclick="window.location.href='finance/paymentCategory'" style="float:right;margin:0;" > Back</button>
            </header>
            <div class="panel-body">
                <div class="no-print">
                <div class="adv-table editable-table ">
                    <div class="clearfix">
                        <style> 
                            .lab{
                                padding-top: 10px;
                                padding-bottom: 20px;
                                border: none;

                            }
                            .pad_bot{
                                padding: 0 15px 0 0;;
                            }  

                            form{
                                background: #ffffff;
                                padding: 20px 0px;
                            }

                            .modal-body form{
                                background: #fff;
                                padding: 21px;
                            }

                            .remove{
                                float: right;
                                margin-top: -45px;
                                margin-right: 42%;
                                margin-bottom: 41px;
                                width: 94px;
                                height: 29px;
                            }

                            .remove1 span{
                                width: 33%;
                                height: 50px !important;
                                padding: 10px

                            }

                            .qfloww {
                                padding: 10px 0px;
                                height: 370px;
                                background: #f1f2f9;
                                overflow: auto;
                            }

                            .remove1 {
                                background: #5A9599;
                                color: #fff;
                                padding: 5px;
                            }


                            .span2{
                                padding: 6px 12px;
                                font-size: 14px;
                                font-weight: 400;
                                line-height: 1;
                                color: #555;
                                text-align: center;
                                background-color: #eee;
                                border: 1px solid #ccc
                            }

                            .addrow{
                                margin-top:10px;
                                margin-left:20px;
                                padding: 6px 20px;
                                background-color: orange;
                            }
                            .new_row
                            {
                                
                                margin-top:20px;
                                font-size: 14px;
                                font-weight: 400;
                                line-height: 1;
                                color: #555;
                                text-align: center;
                                
                            }
                            .del_row
                            {
                                margin-top:0px;

                            }
                            .show_message
                            {
                                background-color: green;
                                padding: 20px,50px;
                            }

                        </style>

                        <form role="form" id="editTest" class="clearfix" action="" method="post" enctype="multipart/form-data">
                            <input type="hidden" name="test" value="<?php echo $category->id; ?>">
                            <div id="show_message" class="show_message"></div>

                            <div class="col-md-12">
                            <div class="row">

                                <div class="col-md-6 lab pad_bot">
                                    <label for="exampleInputEmail1"><?php echo lang('test_code'); ?> <?php echo lang('name'); ?></label>
                                    <input type="text" class="form-control" name="category" id="exampleInputEmail1" value='<?php
                                        if (!empty($setval)) {
                                            echo set_value('category');
                                        }
                                        if (!empty($category->category)) {
                                            echo $category->category;
                                        }
                                        ?>' placeholder="" readonly>
                                </div> 
                                
                                <div class="col-md-6 lab pad_bot">
                                    <label for="exampleInputEmail1"> <?php echo lang('Heading'); ?></label>
                                    <input type="text" class="form-control pay_in" name="h_name"  id="h_name"  value='<?php
                                    if (!empty($category->test_heading)) {
                                        echo $category->test_heading;
                                    }
                                
                                    ?>' placeholder="" required >
                                </div>
                               
                            </div>
                                </div>
                                <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-6 lab pad_bot">
                                        <label for="exampleInputEmail1"> <?php echo lang('note'); ?></label>
                                        <textarea class="form-control pay_in" rows="5" name="note"  id="note" placeholder="" value="" style="height:70px!important;"><?php
                                    if (!empty($category->note)) {
                                        echo $category->note;
                                    }
                                
                                    ?></textarea>
                                      
                                    </div>
                                    <div class="col-md-6 lab pad_bot">
                                        <label for="exampleInputEmail1"> <?php echo lang('interpretations'); ?></label>
                                        <textarea class="form-control pay_in" name="interpretations" rows="5" cols="5"  id="interpretations" placeholder="" style="height:70px!important;"><?php if (!empty($category->interpretation)) {
                                        echo $category->interpretation;
                                    }?></textarea>
                                    </div>
                                </div>
                            
                                </div>

                                <div class="col-md-12 lab pad_bot">
                                    <label for="exampleInputEmail1"> <?php echo lang('comments'); ?></label>
                                    <textarea class="form-control pay_in" name="comments" rows="5" cols="5"  id="comments" placeholder="" style="height:70px!important;"><?php if (!empty($category->comments)) {
                                        echo $category->comments;
                                    }?></textarea>
                                </div>
                           

                                <!--<div class="col-md-8 panel"> 
                                </div>-->
                                <div class="append_doc_div col-md-12 lab pad_bot">
                                <?php 

                                    $count = count($test_parameter); 
                                    //echo $count;die;
                                    if($count == 0)
                                    {
                                ?>


                                <div id="a1"> 
                                    <input type="hidden" name="parameter_id[]" value=''>
                                    <div class="row">
                                    <div class="col-md-4 lab pad_bot">
                                       <label for="exampleInputEmail1"> <?php echo lang('parameter'); ?> <?php echo lang('name'); ?></label>
                                        <textarea name="parameter[]" id="parameter_0" class="ckeditor-basic form-control pay_in " rows="10" value=""></textarea>
                                    </div>
                                    <div class="col-md-4 lab pad_bot">
                                        <label for="exampleInputEmail1"> Unit <?php echo lang('name'); ?></label>
                                        <textarea name="unit[]" id="unit_0" class="ckeditor-basic form-control pay_in " rows="10" value=""></textarea>
                                    </div>
                                    <div class="col-md-4 lab pad_bot">
                                        <label for="exampleInputEmail1"> <?php echo lang('ref_range'); ?></label>
                                        <textarea name="ref_range[]" id="ref_range_0" class="ckeditor-basic form-control pay_in " rows="10" value=""></textarea>
                                    </div>    
                                    </div>
                                    </div>
                                    </div>
                                    
<!--
                                    <div class=" col-md-12 lab pad_bot">
                                        <button type="button" id="updateButton" class="addrow" onclick="rowAdd();"> Add </button>
                                    </div>
-->
                                    

                                <?php
                                    }

                                    elseif($count > 0)
                                    {
                                        $i = 0;
                                        foreach ($test_parameter as $parameter)
                                        {
                                 ?>

                                 <div id="param_row_<?php echo $parameter->id; ?>">
                                 <input type="hidden" name="parameter_id[]" value='<?php
                                if (!empty($parameter->id)) {
                                    echo $parameter->id;
                                }
                                ?>'> 
                                     <div class="row">
                                    <div class="col-md-4 lab pad_bot">
                                        <?php if($i == 0){ ?>
                                         <label for="exampleInputEmail1"> <?php echo lang('parameter'); ?> <?php echo lang('name'); ?></label> 
                                        <?php } ?>
                                         <textarea name="parameter[]" id="parameter_<?php echo $i; ?>" class="form-control ckeditor-basic" style rows="10"><?php echo $parameter->parameter_name; ?></textarea>
                                    </div>
                                     <div class="col-md-4 lab pad_bot">
                                        <?php if($i == 0){ ?>
                                          <label for="exampleInputEmail1"> Unit <?php echo lang('name'); ?></label> 
                                        <?php } ?>
                                         <textarea name="unit[]" id="unit_<?php echo $i; ?>" class="form-control ckeditor-basic" style rows="10"><?php echo $parameter->test_unit; ?></textarea>
                                        </div>
                                     <div class="col-md-4 lab pad_bot">
                                        <?php if($i == 0){ ?>
                                          <label for="exampleInputEmail1"> <?php echo lang('ref_range'); ?> </label> 
                                          <?php } ?>
                                         <textarea name="ref_range[]" id="ref_range_<?php echo $i; ?>" class="form-control ckeditor-basic" style rows="10"><?php echo $parameter->reference_range; ?></textarea>
                                     </div>
                                     </div>
                                    

                                    
                                    
                                </div>
                                <?php if($i == 0){ ?>
<!--
                                    <div class="col-md-12">
                                        <button type="button" id="updateButton" style="position: absolute;right: 0;top: -47px;" class="btn btn-primary" onclick="rowAdd();"> Add </button>
                                     </div>
-->
                                <?php } else{ ?>
                            <div class="col-md-12">
                                    <button type="button" id="updateButton" style="position: absolute;right: 0;top: -47px;" class="btn btn-danger" onclick="parameterDelete(<?php echo $parameter->id; ?>);"> Delete </button>
                            </div>
                            <script> row_cnt++; </script>
                                <?php } ?>
                                 <?php
                                     $i++;
                                     }
                                 ?>

                                 <?php
                                 }

                                ?>
                                      
                            </div>
                              

                                <!--<div class="col-md-8 panel">
                                </div>-->



                            

                            <input type="hidden" name="redirect" value="lab">

                            <input type="hidden" name="id" value='<?php
                            if (!empty($lab->id)) {
                                echo $lab->id;
                            }
                            ?>'>


                            <div class="col-md-12 lab">
                            	<button type="button" id="updateButton" class="btn btn-primary " onclick="rowAdd();"> Add </button> 
                                <button type="submit" name="submit" class="btn btn-info pull-right"><?php echo lang('submit'); ?></button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>


        </div>
        </section>
</section>
</section>
<!--main content end-->
<!--footer start-->

<script src="common/js/codearistos.min.js"></script>
<!-- <script type="text/javascript" src="ckeditor/ckeditor.js"></script> -->


<script type="text/javascript">
    

    

    function rowAdd() 
    
        {
            for ( instance in CKEDITOR.instances )
                    CKEDITOR.instances[instance].updateElement();

            //var parameter = $("#parameter").val();
            //alert(parameter);
            //if(parameter != '')
            //{
                //alert(parameter);
                var html_new = '<div class="new_row row"> <div class=col-md-4><input type ="hidden" name="parameter_id[]" value=""><input type="text" required name="parameter[]" id="parameter_'+ row_cnt +'" class=" ckeditor-basic form-control pay_in"></div><div class=col-md-4> <input type="text" name="unit[]" id="unit_'+ row_cnt +'" class="ckeditor-basic form-control pay_in"></div><div class=col-md-4> <input type="text" name="ref_range[]" id="ref_range_'+ row_cnt +'" class="ckeditor-basic form-control pay_in"></div> <div class="col-md-2 text-right"><button type="button" id="updateButton" class="btn btn-danger del_row" onclick="rowDelete(this);"> Delete </button> </div></div>';
                
                $('.append_doc_div').append(html_new);

                $('.ckeditor-basic').each(function(e){
                    if(!CKEDITOR.instances[this.id]){
                        CKEDITOR.replace(this.id, {
                            width: 300,
                            height: 50,
                            toolbar: [
                            { name: 'document', items: [ 'Source', '-', 'NewPage', 'Preview', '-', 'Templates' ] },	// Defines toolbar group with name (used to create voice label) and items in 3 subgroups.
                            [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ],			// Defines toolbar group without name.
                            '/',																					// Line break - next group will be placed in new line.
                            { name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', 'Superscript', 'Subscript' ] },
                            { name: 'Featured Formula', type: 'widget', widget: 'mathjax', attributes: { 'class': 'math-featured' } }
                        ]
                      });
                    }
                });
               

            //}
            //else
            //{
              
            //alert("Parameter name should not be blank");
            //}
            row_cnt++;

        }

        function rowDelete(e) 
                {
                    $(e).parents('.new_row').remove();
                }

        function parameterDelete(id)
        {
            if(confirm("Are you sure want to delete?")){
            $.ajax({
                type: 'POST',
                    url: "lab/delete_parameter",
                    data: {parameter_id : id},
                    dataType: "json",
                    encode: true
            })
            .done(function(response){
                    if (response == 1) {
                        $("#param_row_" + id).remove();
                    }
                    else{
                        alert('Sorry');
                    }
                })
                .fail(function(response){
                    console.log(response);
                });
            }
        }


        $('#editTest').submit(function(e){
                e.preventDefault();
                for ( instance in CKEDITOR.instances )
                    CKEDITOR.instances[instance].updateElement();
    
            var formData = $('#editTest').serializeArray();
            //var formData = new FormData($("#editTest")[0]);
            //formData.append('parameter[]', CKEDITOR.instances['parameter[]'].getData());
                console.log(formData);

                $.ajax({
                    type: 'POST',
                    url: "lab/add_new_test",
                    data: formData,
                    dataType: "json",
                    encode: true
                })
                .done(function(response){
                    if (response == 1) {

                        //$("#show_message").html('Successfully Inserted !');
                        setTimeout(function () {
                            //var href="";
                            window.location.href="finance/paymentCategory";
                        }, 2000);
                        //alert('Success');
                    }
                    else{
                        alert('Sorry');
                    }
                })
                .fail(function(response){
                    console.log(response);
                });

            });
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $('.ckeditor-basic').each(function(e){
            CKEDITOR.replace(this.id, {
                width: 300,
                height: 50,
                toolbar: [
                    { name: 'document', items: [ 'Source', '-', 'NewPage', 'Preview', '-', 'Templates' ] },	// Defines toolbar group with name (used to create voice label) and items in 3 subgroups.
                    [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ],			// Defines toolbar group without name.
                    '/',																					// Line break - next group will be placed in new line.
                    { name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', 'Superscript', 'Subscript' ] },
                    { name: 'Featured Formula', type: 'widget', widget: 'mathjax', attributes: { 'class': 'math-featured' } }
                ]
            });
        });
    });
</script>