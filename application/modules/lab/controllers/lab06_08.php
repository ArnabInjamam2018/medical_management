<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Lab extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('Ion_auth');
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->library('upload');
        //$this->load->model('email_model');
        $this->load->model('ion_auth_model');
        //$this->load->library('mpdf');
        $this->load->model('lab_model');
        $this->load->model('doctor/doctor_model');
        $language = $this->db->get('settings')->row()->language;
        $this->lang->load('system_syntax', $language);
        $this->load->model('patient/patient_model');
        $this->load->model('accountant/accountant_model');
        $this->load->model('receptionist/receptionist_model');
        $this->load->model('settings/settings_model');
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        }
        if (!$this->ion_auth->in_group(array('admin', 'Accountant', 'Receptionist', 'Nurse', 'Laboratorist', 'Doctor', 'Patient'))) {
            redirect('home/permission');
        }
    }

    public function index() 
    {

        if (!$this->ion_auth->logged_in()) 
        {
            redirect('auth/login', 'refresh');
        }

        if ($this->ion_auth->in_group(array('Receptionist'))) 
        {
            redirect('lab/lab1');
        }

        $id = $this->input->get('id');

        $data['settings'] = $this->settings_model->getSettings();
        $data['labs'] = $this->lab_model->getLab();

        if (!empty($id)) 
        {
            $data['lab_single'] = $this->lab_model->getLabById($id);
        }

        $data['templates'] = $this->lab_model->getTemplate();
        $data['settings'] = $this->settings_model->getSettings();
        $data['categories'] = $this->lab_model->getLabCategory();
        $data['patients'] = $this->patient_model->getPatient();
        $data['doctors'] = $this->doctor_model->getDoctor();

        $this->load->view('home/dashboard'); // just the header file
        $this->load->view('lab', $data);
        $this->load->view('home/footer'); // just the header file
    }

    public function lab() 
    {
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        }

        $id = $this->input->get('id');



        $data['templates'] = $this->lab_model->getTemplate();
        $data['settings'] = $this->settings_model->getSettings();
        $data['categories'] = $this->lab_model->getLabCategory();
        $data['patients'] = $this->patient_model->getPatient();
        $data['doctors'] = $this->doctor_model->getDoctor();

        $data['settings'] = $this->settings_model->getSettings();
        $data['labs'] = $this->lab_model->getLab();

        if (!empty($id)) {
            $this->load->view('home/dashboard'); // just the header file
            $this->load->view('add_lab_view', $data);
            $this->load->view('home/footer'); // just the header file
        } else {
            $this->load->view('home/dashboard'); // just the header file
            $this->load->view('lab', $data);
            $this->load->view('home/footer'); // just the header file
        }
    }

    public function lab1() {

        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        }

        $id = $this->input->get('id');

        $data['settings'] = $this->settings_model->getSettings();
        $data['labs'] = $this->lab_model->getLab();

        if (!empty($id)) {
            $data['lab_single'] = $this->lab_model->getLabById($id);
        }

        $data['templates'] = $this->lab_model->getTemplate();
        $data['settings'] = $this->settings_model->getSettings();
        $data['categories'] = $this->lab_model->getLabCategory();
        $data['patients'] = $this->patient_model->getPatient();
        $data['doctors'] = $this->doctor_model->getDoctor();

        $this->load->view('home/dashboard'); // just the header file
        $this->load->view('lab_1', $data);
        $this->load->view('home/footer'); // just the header file
    }

    public function addLabView() {
        $data = array();


        $id = $this->input->get('id');

        if (!empty($id)) {
            $data['lab'] = $this->lab_model->getLabById($id);
        }

        $data['templates'] = $this->lab_model->getTemplate();
        $data['settings'] = $this->settings_model->getSettings();
        $data['categories'] = $this->lab_model->getLabCategory();
        $data['patients'] = $this->patient_model->getPatient();
        $data['doctors'] = $this->doctor_model->getDoctor();
        $this->load->view('home/dashboard'); // just the header file
        $this->load->view('add_lab_view', $data);
        $this->load->view('home/footer'); // just the header file
    }

    public function test_parameter() 
    {
        $data = array();
        $id = $this->input->get('id');
        //echo $id;die;
        $data['category'] = $this->lab_model->getPaymentCategory($id);
        $data['test_parameter'] = $this->lab_model->getTestParameter($id);
        //print_r($data['category']);die;
        $this->load->view('home/dashboard'); // just the header file
        $this->load->view('add_test_parameter', $data);
        $this->load->view('home/footer'); // just the header file
    
    }

    public function test_report() 
    {
        $data = array();
        $return_data=array();
        $id = $this->input->post('v_id');
        $invoice_id = $this->input->post('inv_d_id');
        $action_type = $this->input->post('action_type');
        $category_id= $this->input->post('cat_id');
      //  echo $category_id;die;

        
        //echo $action_type;die;
        $data['category_report'] = $this->lab_model->getPaymentCategoryReport($id,$invoice_id);
        // echo "<pre>";
         //print_r($data['category_report']);die;
        if($action_type == 'generate_report')
        {
            $data['path_doctor'] = $this->doctor_model->getPathologyDoctor();
        
        }
      // print_r($data['path_doctor']);die;
       // $this->load->view('home/dashboard'); // just the header file
     //   $this->load->view('test_report', $data);
     //  $this->load->view('home/footer'); // just the header file
        $return_data = array("status" => true, "category_report_list" =>$data['category_report'],"path_doctor_list"=>$data['path_doctor']);
        echo json_encode($return_data);
    }


    public function delete_parameter()
    {
        $id = $this->input->post('parameter_id');
        $this->db->where(array('id' => $id ))->delete('test_parameter');
        echo json_encode("1");
    }


    public function addLab() {
        $id = $this->input->post('id');

        $report = $this->input->post('report');

        $patient = $this->input->post('patient');

        $redirect = $this->input->post('redirect');

        $p_name = $this->input->post('p_name');
        $p_email = $this->input->post('p_email');
        if (empty($p_email)) {
            $p_email = $p_name . '-' . rand(1, 1000) . '-' . $p_name . '-' . rand(1, 1000) . '@example.com';
        }
        if (!empty($p_name)) {
            $password = $p_name . '-' . rand(1, 100000000);
        }
        $p_phone = $this->input->post('p_phone');
        $p_age = $this->input->post('p_age');
        $p_gender = $this->input->post('p_gender');
        $add_date = date('m/d/y');


        $patient_id = rand(10000, 1000000);



        $d_name = $this->input->post('d_name');
        $d_email = $this->input->post('d_email');
        if (empty($d_email)) 
        {
            $d_email = $d_name . '-' . rand(1, 1000) . '-' . $d_name . '-' . rand(1, 1000) . '@example.com';
        }
        if (!empty($d_name)) 
        {
            $password = $d_name . '-' . rand(1, 100000000);
        }
        $d_phone = $this->input->post('d_phone');

        $doctor = $this->input->post('doctor');
        $date = $this->input->post('date');
        if (!empty($date)) 
        {
            $date = strtotime($date);
        } 
        else 
        {
            $date = time();
        }
        $date_string = date('d-m-y', $date);
        $discount = $this->input->post('discount');
        $amount_received = $this->input->post('amount_received');
        $user = $this->ion_auth->get_user_id();

        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

// Validating Category Field
// $this->form_validation->set_rules('category_amount[]', 'Category', 'min_length[1]|max_length[100]');
// Validating Price Field
        $this->form_validation->set_rules('patient', 'Patient', 'trim|min_length[1]|max_length[100]|xss_clean');
// Validating Price Field
        $this->form_validation->set_rules('discount', 'Discount', 'trim|min_length[1]|max_length[100]|xss_clean');

        if ($this->form_validation->run() == FALSE) {
            redirect('lab/addLabView');
        } else {
            if (!empty($p_name)) {

                $data_p = array(
                    'patient_id' => $patient_id,
                    'name' => $p_name,
                    'email' => $p_email,
                    'phone' => $p_phone,
                    'sex' => $p_gender,
                    'age' => $p_age,
                    'add_date' => $add_date,
                    'how_added' => 'from_pos'
                );
                $username = $this->input->post('p_name');
// Adding New Patient
                if ($this->ion_auth->email_check($p_email)) {
                    $this->session->set_flashdata('feedback', 'This Email Address Is Already Registered');
                } else {
                    $dfg = 5;
                    $this->ion_auth->register($username, $password, $p_email, $dfg);
                    $ion_user_id = $this->db->get_where('users', array('email' => $p_email))->row()->id;
                    $this->patient_model->insertPatient($data_p);
                    $patient_user_id = $this->db->get_where('patient', array('email' => $p_email))->row()->id;
                    $id_info = array('ion_user_id' => $ion_user_id);
                    $this->patient_model->updatePatient($patient_user_id, $id_info);
                }
//    }
            }

            if (!empty($d_name)) {

                $data_d = array(
                    'name' => $d_name,
                    'email' => $d_email,
                    'phone' => $d_phone,
                );
                $username = $this->input->post('d_name');
// Adding New Patient
                if ($this->ion_auth->email_check($d_email)) {
                    $this->session->set_flashdata('feedback', 'This Email Address Is Already Registered');
                } else {
                    $dfgg = 4;
                    $this->ion_auth->register($username, $password, $d_email, $dfgg);
                    $ion_user_id = $this->db->get_where('users', array('email' => $d_email))->row()->id;
                    $this->doctor_model->insertDoctor($data_d);
                    $doctor_user_id = $this->db->get_where('doctor', array('email' => $d_email))->row()->id;
                    $id_info = array('ion_user_id' => $ion_user_id);
                    $this->doctor_model->updateDoctor($doctor_user_id, $id_info);
                }
            }


            if ($patient == 'add_new') {
                $patient = $patient_user_id;
            }

            if ($doctor == 'add_new') {
                $doctor = $doctor_user_id;
            }

            if (!empty($patient)) {
                $patient_details = $this->patient_model->getPatientById($patient);
                $patient_name = $patient_details->name;
                $patient_phone = $patient_details->phone;
                $patient_address = $patient_details->address;
            } else {
                $patient_name = 0;
                $patient_phone = 0;
                $patient_address = 0;
            }

            if (!empty($doctor)) {
                $doctor_details = $this->doctor_model->getDoctorById($doctor);
                $doctor_name = $doctor_details->name;
            } else {
                $doctor_name = 0;
            }

            $data = array();

            if (empty($id)) {
                $data = array(
                    // 'category_name' => $category_name,
                    'report' => $report,
                    'patient' => $patient,
                    'date' => $date,
                    'doctor' => $doctor,
                    'user' => $user,
                    'patient_name' => $patient_name,
                    'patient_phone' => $patient_phone,
                    'patient_address' => $patient_address,
                    'doctor_name' => $doctor_name,
                    'date_string' => $date_string
                );


                $this->lab_model->insertLab($data);
                $inserted_id = $this->db->insert_id();

                $this->session->set_flashdata('feedback', 'Added');
                redirect($redirect);
            } else {
                $data = array(
                    //   'category_name' => $category_name,
                    'report' => $report,
                    'patient' => $patient,
                    'doctor' => $doctor,
                    'user' => $user,
                    'patient_name' => $patient_details->name,
                    'patient_phone' => $patient_details->phone,
                    'patient_address' => $patient_details->address,
                    'doctor_name' => $doctor_details->name,
                );
                $this->lab_model->updateLab($id, $data);
                $this->session->set_flashdata('feedback', 'Updated');
                redirect($redirect);
            }
        }
    }

    function editLab() {
        if ($this->ion_auth->in_group(array('admin', 'Doctor', 'Laboratorist', 'Nurse', 'Patient'))) {
            $data = array();
            $data['settings'] = $this->settings_model->getSettings();
            $data['categories'] = $this->lab_model->getLabCategory();
            $data['patients'] = $this->patient_model->getPatient();
            $data['doctors'] = $this->doctor_model->getDoctor();
            $id = $this->input->get('id');
            //$id_patient = $this->input->get('patient');
            $data['lab'] = $this->lab_model->getLabById($id);
            $this->load->view('home/dashboard'); // just the header file
            $this->load->view('add_lab_view', $data);
            $this->load->view('home/footer'); // just the footer file
        }
    }

    function delete() {
        $id = $this->input->get('id');
        $this->lab_model->deleteLab($id);
        $this->session->set_flashdata('feedback', 'Deleted');
        redirect('lab/lab');
    }

    public function template() {
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        }
        $data['settings'] = $this->settings_model->getSettings();
        $data['templates'] = $this->lab_model->getTemplate();

        $this->load->view('home/dashboard'); // just the header file
        $this->load->view('template', $data);
        $this->load->view('home/footer'); // just the header file
    }

    public function addTemplateView() {
        $data = array();
        $id = $this->input->get('id');
        if (!empty($id)) {
            $data['template'] = $this->lab_model->getTemplateById($id);
        }

        $data['settings'] = $this->settings_model->getSettings();
        $this->load->view('home/dashboard'); // just the header file
        $this->load->view('add_template', $data);
        $this->load->view('home/footer'); // just the header file
    }

    function getTemplateByIdByJason() {
        $id = $this->input->get('id');
        $data['template'] = $this->lab_model->getTemplateById($id);
        echo json_encode($data);
    }

    public function addTemplate() {
        $id = $this->input->post('id');
        $name = $this->input->post('name');
        $template = $this->input->post('template');
        $user = $this->ion_auth->get_user_id();


        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->form_validation->set_rules('report', 'Report', 'trim|min_length[1]|max_length[10000]|xss_clean');
// Validating Price Field
        $this->form_validation->set_rules('user', 'User', 'trim|min_length[1]|max_length[100]|xss_clean');

        if ($this->form_validation->run() == FALSE) {
            redirect('lab/addTemplate');
        } else {
            $data = array();
            if (empty($id)) {
                $data = array(
                    'name' => $name,
                    'template' => $template,
                    'user' => $user,
                );
                $this->lab_model->insertTemplate($data);
                $inserted_id = $this->db->insert_id();
                $this->session->set_flashdata('feedback', 'Added');
                redirect("lab/addTemplateView?id=" . "$inserted_id");
            } else {
                $data = array(
                    'name' => $name,
                    'template' => $template,
                    'user' => $user,
                );
                $this->lab_model->updateTemplate($id, $data);
                $this->session->set_flashdata('feedback', 'Updated');
                redirect("lab/addTemplateView?id=" . "$id");
            }
        }
    }

    function editTemplate() {
        if ($this->ion_auth->in_group(array('admin', 'Doctor', 'Laboratorist', 'Nurse', 'Patient'))) {
            $data = array();
            $data['settings'] = $this->settings_model->getSettings();
            $id = $this->input->get('id');
            $data['template'] = $this->lab_model->getTemplateById($id);
            $this->load->view('home/dashboard'); // just the header file
            $this->load->view('add_template', $data);
            $this->load->view('home/footer'); // just the footer file
        }
    }

    function deleteTemplate() {
        $id = $this->input->get('id');
        $this->lab_model->deleteTemplate($id);
        $this->session->set_flashdata('feedback', 'Deleted');
        redirect('lab/template');
    }

    public function labCategory() {
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        }
        $data['categories'] = $this->lab_model->getLabCategory();
        $data['settings'] = $this->settings_model->getSettings();
        $this->load->view('home/dashboard'); // just the header file
        $this->load->view('lab_category', $data);
        $this->load->view('home/footer'); // just the header file
    }

    public function addLabCategoryView() {
        $this->load->view('home/dashboard'); // just the header file
        $this->load->view('add_lab_category');
        $this->load->view('home/footer'); // just the header file
    }

    public function addLabCategory() {
        $id = $this->input->post('id');
        $category = $this->input->post('category');
        $description = $this->input->post('description');
        $reference = $this->input->post('reference_value');


        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
// Validating Category Name Field
        $this->form_validation->set_rules('category', 'Category', 'trim|required|min_length[1]|max_length[100]|xss_clean');
// Validating Description Field
        $this->form_validation->set_rules('description', 'Description', 'trim|required|min_length[1]|max_length[100]|xss_clean');
        // Validating Description Field
        $this->form_validation->set_rules('reference_value', 'Reference Value', 'trim|required|min_length[1]|max_length[1000]|xss_clean');
// Validating Description Field
        $this->form_validation->set_rules('type', 'Type', 'trim|min_length[1]|max_length[100]|xss_clean');

        if ($this->form_validation->run() == FALSE) {
            if (!empty($id)) {
                $this->session->set_flashdata('feedback', 'Validation Error !');
                redirect('lab/editLabCategory?id=' . $id);
            } else {
                $data = array();
                $data['setval'] = 'setval';
                $this->load->view('home/dashboard'); // just the header file
                $this->load->view('add_lab_category', $data);
                $this->load->view('home/footer'); // just the header file
            }
        } else {
            $data = array();
            $data = array('category' => $category,
                'description' => $description,
                'reference_value' => $reference,
            );
            if (empty($id)) {
                $this->lab_model->insertLabCategory($data);
                $this->session->set_flashdata('feedback', 'Added');
            } else {
                $this->lab_model->updateLabCategory($id, $data);
                $this->session->set_flashdata('feedback', 'Updated');
            }
            redirect('lab/labCategory');
        }
    }

    function editLabCategory() {
        $data = array();
        $id = $this->input->get('id');
        $data['category'] = $this->lab_model->getLabCategoryById($id);
        $this->load->view('home/dashboard'); // just the header file
        $this->load->view('add_lab_category', $data);
        $this->load->view('home/footer'); // just the footer file
    }

    function deleteLabCategory() {
        $id = $this->input->get('id');
        $this->lab_model->deleteLabCategory($id);
        redirect('lab/labCategory');
    }

    function invoice() {
        $data = array();
        $id = $this->input->get('id');
        $data['settings'] = $this->settings_model->getSettings();
        $data['lab'] = $this->lab_model->getLabById($id);
        $this->load->view('home/dashboard'); // just the header file
        $this->load->view('invoice', $data);
        $this->load->view('home/footer'); // just the footer fi
    }

    function pdf_generate(){
        
        ini_set('memory_limit','32M');
        $data = array();
        $id = $this->input->get('id');
        $data['settings'] = $this->settings_model->getSettings();
        $data['lab'] = $this->lab_model->getLabById($id);
        //$data['discount_type'] = $this->finance_model->getDiscountType();
        //$data['payment'] = $this->finance_model->getPaymentById($id);
        //$this->load->view('home/dashboard'); // just the header file
        //$this->load->view('home/footer');

        //load the view and saved it into $html variable
        $stylesheet = file_get_contents(site_url() . 'common/css/pdf.css'); // Get css content

        $html=$this->load->view('Pdf_generate', $data, true);
        $html_header=$this->load->view('pdf_header', $data, true);
        $html_footer=$this->load->view('pdf_footer', $data, true);

        //load mPDF library
        $this->load->library('pdf');
        $pdf = $this->pdf->load('utf-8', 'A4-L');
        $pdf->setAutoTopMargin = 'stretch'; // Set pdf top margin to stretch to avoid content overlapping
        $pdf->setAutoBottomMargin = 'stretch'; // Set pdf bottom margin to stretch to avoid content overlapping

        /*$pdf->WriteHTML($stylesheet, 1); // Writing style to pdf
        
        $header_html = '<htmlpageheader name="MyCustomHeader"><div class="pdf-header" style"height:auto;"><div class="header">'.$html_header.'</div></div></htmlpageheader>';
        $pdf->SetHTMLHeader($header_html);
        
        //$footer = '<htmlpagefooter name="MyCustomFooter"><div class="pdf-footer"><div class="footer">'.$html_footer.'</div></div></htmlpagefooter>';
		//$pdf->SetFooter($footer);
        //echo $stylesheet; echo $header_html; echo $html; echo $footer; exit();
        $pdf->SetHTMLFooter('<div class="pdf-footer" style"height:auto;"><div class="footer">'.$html_footer.'</div></div>');
        // $pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822)); // Add a footer for good measure ;)
       
        $pdf->WriteHTML($html); // write the HTML into the PDF
        */
        //echo $html_header; echo $html; echo $html_footer; exit();
        //$pdf->WriteHTML($stylesheet, 'HTML');
        $pdf->SetHTMLHeader('<htmlpageheader name="MyCustomHeader"><div class="pdf-header" style"height:auto;"><div class="header">'.$html_header.'</div></div></htmlpageheader>');
        $pdf->SetHTMLFooter('<htmlpagefooter name="MyCustomFooter"><div class="pdf-footer"><div class="footer">'.$html_footer.'</div></div></htmlpagefooter>');
        $pdf->WriteHTML($stylesheet, 1);
        $pdf->WriteHTML($html);

        //this the the PDF filename that user will get to download
        $pdfFilePath = time() . "_output_pdf_test.pdf";

        $pdf->Output($pdfFilePath, 'D'); // D for downloading file


 
    }

    // -------------------------------------- ------------------------  For MAIL SEND  --------------------- --------------------------//

    // function send_mail(){

    //     ini_set('memory_limit','32M');
    //     $data = array();
    //     $id = $this->input->get('id');
    //     $data['settings'] = $this->settings_model->getSettings();
    //     $data['lab'] = $this->lab_model->getLabById($id);
        
    //     $per_email = $data['lab']->email;      // for fatching mail from array
    //     //echo $per_email;die;

    //     // $servername = "43.255.154.112";
    //     // $username = "developer";
    //     // $password = "pass@1234";
    //     // $database="bibha_db";

    //     // // Create connection
    //     // $db = mysqli_connect($servername, $username, $password, $database);

    //     // // Check connection
    //     // if (!$db) {
    //     //     die("Connection failed: " . mysqli_connect_error());
    //     // }


    //     $this->load->library('PHPMailer_Lib');
    //     echo '___________';die;
    //     $mail = $this->PHPMailer_Lib->load();
    //     //echo '_________';die;
    //     // $mail = new PHPMailer();
    //     // die;
    //     try{
    //         /*
    //         $mail->isSMTP();
    //         $mail->Host		= "smtpout.asia.secureserver.net";
    //         $mail->Port		= 465;
    //         $mail->SMTPSecure	= "ssl";
    //         $mail->SMTPAuth	= true;
    //         $mail->SMTPDebug  	= 0;
    //         $mail->SMTPOptions = array(
    //                                 'ssl' => array(
    //                                 'verify_peer' => false,
    //                                 'verify_peer_name' => false,
    //                                 'allow_self_signed' => true
    //                             )
    //                         );
            
    //         $mail->Username	= 'admin@bibhahealthcare.com';
    //         $mail->Password	= 'India@1947';
    //         */
    //         $mail->SetFrom ('admin@bibhahealthcare.com');
            
    //         $mail->From      = ('admin@bibhahealthcare.com');	// 'admin@bibhahealthcare.com'; 
    //         $mail->FromName  = ('Bibha Healthcare');	//'bibhahealthcare';
    //         //$email->AddCC('admin@bibhahealthcare.com');
        
    //         $mail->AddReplyTo ('admin@bibhahealthcare.com');
            
    //         $mail->AddAddress($per_email);
                       
    //         $mail->Subject   = 'Bibha Healthcare Contact Enquiry';
    //         $messagebody = '<html><body>';
    //             $messagebody .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
    //             $messagebody .= '<tr style="background: #eee;"><td><strong>Name:</strong></td><td>BAPPA KARMAKAR</td></tr>';
    //             $messagebody .= '<tr><td><strong>Phone:</strong> </td><td>99-88-77-66-55</td></tr>';
    //             $messagebody .= '<tr><td><strong>Email:</strong> </td><td>'. $per_email .'</td></tr>';
    //             $messagebody .= '<tr><td><strong>Subject:</strong> </td><td>KI BOLBO</td></tr>';
    //             $messagebody .= '<tr><td><strong>Message:</strong> </td><td>NA MASSEGE NEI</td></tr>';
    //             $messagebody .= '</table>';
    //             $messagebody .= '</body></html>';
                
    //         $mail->Body      = $messagebody;
    //         $mail->isHTML(true);
    //         if ($mail->Send()){
    //             $msg ='Mail sent successfully';
                
    //         }
    //         else{
    //             $msg= 'Unable to send mail. There is a server issue.';
    //         }
             
    //         } 
    //         catch( phpmailerException $e ) {
    //             $msg= $e->errorMessage();
    //             // die;
    //             // exit(0);
    //         }

    // }



     function send_mail(){                                //  Start a send mail function call 
        
        //echo FCPATH; exit();
        ini_set('memory_limit','32M');
        $data = array();
        $id = $this->input->get('id');
        $data['settings'] = $this->settings_model->getSettings();
        $data['lab'] = $this->lab_model->getLabById($id);
        
        $per_email = $data['lab']->email;      // for fatching mail from array
        //echo $per_email;die;
        // //echo $data['lab']->email;die; 

        //load the view and saved it into $html variable
        $stylesheet = file_get_contents(site_url() . 'common/css/pdf.css'); // Get css content

        $html=$this->load->view('Pdf_generate', $data, true);
        $html_header=$this->load->view('pdf_header', $data, true);
        $html_footer=$this->load->view('pdf_footer', $data, true);

        //load mPDF library
        $this->load->library('pdf');
        $pdf = $this->pdf->load('utf-8', 'A4-L');
        $pdf->setAutoTopMargin = 'stretch'; // Set pdf top margin to stretch to avoid content overlapping
        $pdf->setAutoBottomMargin = 'stretch'; // Set pdf bottom margin to stretch to avoid content overlapping

        
        //echo $html_header; echo $html; echo $html_footer; exit();
        //$pdf->WriteHTML($stylesheet, 'HTML');
        $pdf->SetHTMLHeader('<htmlpageheader name="MyCustomHeader"><div class="pdf-header" style"height:auto;"><div class="header">'.$html_header.'</div></div></htmlpageheader>');
        $pdf->SetHTMLFooter('<htmlpagefooter name="MyCustomFooter"><div class="pdf-footer"><div class="footer">'.$html_footer.'</div></div></htmlpagefooter>');
        $pdf->WriteHTML($stylesheet, 1);
        $pdf->WriteHTML($html);

        //this the the PDF filename that user will get to download
        $pdfFilePath = time() . "_output_pdf_test.pdf";
        // echo $pdfFilePath;die;

        $dir = FCPATH.'uploads/';
        $pdf->Output($dir . $pdfFilePath, 'F');
        //echo $dir;die;

        //$content = $pdf->Output('', 'S'); //	generate on the fly and get the content in a veriable
       // echo $content;die;
        
                            // =========================   Send mail  =============================//
            //$userId = $this->ion_auth->get_user_id();
            //echo $userId;die;
            //$emailSettings = $this->email_model->getEmailSettings();
            


            //$to = 'bappa.flamingostech@gmail.com';
            if (!empty($per_email)) {
                $message = 'TEST A FOR SENT MAIL TO PATIENT CONVENIENCE';
                $subject = 'Bibha Subject';
    
    
                $this->email->from('admin@bibhahealthcare.com');
                $this->email->to($per_email);
                $this->email->subject($subject);
                $this->email->message($message);
                
                $this->email->attach($dir.$pdfFilePath);
    
                if ($this->email->send()) {
                    $data = array();
                    $date = time();
                    // $data = array(
                    //     'subject' => $subject,
                    //     'message' => $message
                    //     //'date' => $date,
                    //     //'reciepient' => $recipient,
                    //     //'user' => $this->ion_auth->get_user_id()
                    // );
                    //$this->email_model->insertEmail($data);
                    $this->session->set_flashdata('feedback', 'Message Sent');
                } else {
                    $this->session->set_flashdata('feedback', 'Email Failed');
                }
            } else {
                $this->session->set_flashdata('feedback', 'Not Sent');
            }
            redirect('lab/invoice?id='.$id);

     }



    // -------------------------------------- ------------------------  END MAIL SEND Function  --------------------- ---------------------------- //

    function patientLabHistory() {
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        }
        $patient = $this->input->get('patient');
        if (empty($patient)) {
            $patient = $this->input->post('patient');
        }

        $date_from = strtotime($this->input->post('date_from'));
        $date_to = strtotime($this->input->post('date_to'));
        if (!empty($date_to)) {
            $date_to = $date_to + 86399;
        }

        $data['date_from'] = $date_from;
        $data['date_to'] = $date_to;

        if (!empty($date_from)) {
            $data['labs'] = $this->lab_model->getLabByPatientIdByDate($patient, $date_from, $date_to);
            $data['deposits'] = $this->lab_model->getDepositByPatientIdByDate($patient, $date_from, $date_to);
        } else {
            $data['labs'] = $this->lab_model->getLabByPatientId($patient);
            $data['pharmacy_labs'] = $this->pharmacy_model->getLabByPatientId($patient);
            $data['ot_labs'] = $this->lab_model->getOtLabByPatientId($patient);
            $data['deposits'] = $this->lab_model->getDepositByPatientId($patient);
        }



        $data['patient'] = $this->patient_model->getPatientByid($patient);
        $data['settings'] = $this->settings_model->getSettings();



        $this->load->view('home/dashboard'); // just the header file
        $this->load->view('patient_deposit', $data);
        $this->load->view('home/footer'); // just the header file
    }

    function financialReport() {
        $date_from = strtotime($this->input->post('date_from'));
        $date_to = strtotime($this->input->post('date_to'));
        if (!empty($date_to)) {
            $date_to = $date_to + 86399;
        }
        $data = array();
        $data['lab_categories'] = $this->lab_model->getLabCategory();
        $data['expense_categories'] = $this->lab_model->getExpenseCategory();


// if(empty($date_from)&&empty($date_to)) {
//    $data['labs']=$this->lab_model->get_lab();
//     $data['ot_labs']=$this->lab_model->get_ot_lab();
//     $data['expenses']=$this->lab_model->get_expense();
// }
// else{

        $data['labs'] = $this->lab_model->getLabByDate($date_from, $date_to);
        $data['ot_labs'] = $this->lab_model->getOtLabByDate($date_from, $date_to);
        $data['deposits'] = $this->lab_model->getDepositsByDate($date_from, $date_to);
        $data['expenses'] = $this->lab_model->getExpenseByDate($date_from, $date_to);
// } 
        $data['from'] = $this->input->post('date_from');
        $data['to'] = $this->input->post('date_to');
        $data['settings'] = $this->settings_model->getSettings();
        $this->load->view('home/dashboard'); // just the header file
        $this->load->view('financial_report', $data);
        $this->load->view('home/footer'); // just the footer fi
    }

    function getLab() {
        $requestData = $_REQUEST;
        $start = $requestData['start'];
        $limit = $requestData['length'];
        $search = $this->input->post('search')['value'];

        if ($limit == -1) {
            if (!empty($search)) {
                $data['labs'] = $this->lab_model->getLabBysearch($search);
            } else {
                $data['labs'] = $this->lab_model->getLab();
            }
        } else {
            if (!empty($search)) {
                $data['labs'] = $this->lab_model->getLabByLimitBySearch($limit, $start, $search);
            } else {
                $data['labs'] = $this->lab_model->getLabByLimit($limit, $start);
            }
        }
        //  $data['labs'] = $this->lab_model->getLab();

        foreach ($data['labs'] as $lab) {
            $date = date('d-m-y', $lab->date);
            if ($this->ion_auth->in_group(array('admin', 'Laboratorist', 'Doctor'))) {
                $options1 = ' <a class="btn btn-info btn-xs editbutton" title="' . lang('edit') . '" href="lab?id=' . $lab->id . '"><i class="fa fa-edit"> </i> ' . lang('') . '</a>';
            } else {
                $options1 = '';
            }

            $options2 = '<a class="btn btn-xs invoicebutton" title="' . lang('lab') . '" style="color: #fff;" href="lab/invoice?id=' . $lab->id . '"><i class="fa fa-file-text"></i> ' . lang('') . '</a>';

            if ($this->ion_auth->in_group(array('admin', 'Doctor', 'Laboratorist'))) {
                $options3 = '<a class="btn btn-info btn-xs delete_button" title="' . lang('delete') . '" href="lab/delete?id=' . $lab->id . '" onclick="return confirm(\'Are you sure you want to delete this item?\');"><i class="fa fa-trash-o"></i>' . lang('') . '</a>';
            } else {
                $options3 = '';
            }

            $doctor_info = $this->doctor_model->getDoctorById($lab->doctor);
            if (!empty($doctor_info)) {
                $doctor = $doctor_info->name;
            } else {
                if (!empty($lab->doctor_name)) {
                    $doctor = $lab->doctor_name;
                } else {
                    $doctor = ' ';
                }
            }


            $patient_info = $this->patient_model->getPatientById($lab->patient);
            if (!empty($patient_info)) {
                $patient_details = $patient_info->name . '</br>' . $patient_info->address . '</br>' . $patient_info->phone . '</br>';
            } else {
                $patient_details = ' ';
            }
            $info[] = array(
                $lab->id,
                $patient_details,
                $date,
                $options1 . ' ' . $options2 . ' ' . $options3,
                    // $options2 . ' ' . $options3
            );
        }


        if (!empty($data['labs'])) {
            $output = array(
                "draw" => intval($requestData['draw']),
                "recordsTotal" => $this->db->get('lab')->num_rows(),
                "recordsFiltered" => $this->db->get('lab')->num_rows(),
                "data" => $info
            );
        } else {
            $output = array(
                // "draw" => 1,
                "recordsTotal" => 0,
                "recordsFiltered" => 0,
                "data" => []
            );
        }




        echo json_encode($output);
    }
	
	public function paymentWiseReport() {

        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        }

        $payment_id = $this->input->get('payment_id');
		
		if (empty($payment_id)) {
            redirect('lab');
        }

        $data['settings'] = $this->settings_model->getSettings();
        $data['labs'] = $this->lab_model->getLab();

        if (!empty($payment_id)) {
            $data['payment_id'] = $payment_id;
            $data['lab_single'] = $this->lab_model->getPatientDoctorByPayment($payment_id);
        }

        $data['templates'] = $this->lab_model->getTemplate();
        $data['settings'] = $this->settings_model->getSettings();
        $data['categories'] = $this->lab_model->getLabCategory();
        $data['patients'] = $this->patient_model->getPatient();
        $data['doctors'] = $this->doctor_model->getDoctor();

        $this->load->view('home/dashboard'); // just the header file
        $this->load->view('lab_payment_wise', $data);
        $this->load->view('home/footer'); // just the header file
    }
	
	function getLabByPayment() {
        $payment_id = $this->input->post('payment_id');
		$data['labs'] = $this->lab_model->getPaymentWiseLabReport($payment_id);

        foreach ($data['labs'] as $lab) {
            $date = date('d-m-y', $lab->date);
            if ($this->ion_auth->in_group(array('admin', 'Laboratorist', 'Doctor'))) {
                $options1 = ' <a class="btn btn-info btn-xs editbutton" title="' . lang('edit') . '" href="lab?id=' . $lab->id . '"><i class="fa fa-edit"> </i> ' . lang('') . '</a>';
            } else {
                $options1 = '';
            }

            $options2 = '<a class="btn btn-xs invoicebutton" title="' . lang('lab') . '" style="color: #fff;" href="lab/invoice?id=' . $lab->id . '"><i class="fa fa-file-text"></i> ' . lang('') . '</a>';

            if ($this->ion_auth->in_group(array('admin', 'Doctor', 'Laboratorist'))) {
                $options3 = '<a class="btn btn-info btn-xs delete_button" title="' . lang('delete') . '" href="lab/delete?id=' . $lab->id . '" onclick="return confirm(\'Are you sure you want to delete this item?\');"><i class="fa fa-trash-o"></i>' . lang('') . '</a>';
            } else {
                $options3 = '';
            }

            $doctor_info = $this->doctor_model->getDoctorById($lab->doctor);
            if (!empty($doctor_info)) {
                $doctor = $doctor_info->name;
            } else {
                if (!empty($lab->doctor_name)) {
                    $doctor = $lab->doctor_name;
                } else {
                    $doctor = ' ';
                }
            }


            $patient_info = $this->patient_model->getPatientById($lab->patient);
            if (!empty($patient_info)) {
                $patient_details = $patient_info->name . '</br>' . $patient_info->address . '</br>' . $patient_info->phone . '</br>';
            } else {
                $patient_details = ' ';
            }
            $info[] = array(
                $lab->id,
                $patient_details,
                $date,
                $options1 . ' ' . $options2 . ' ' . $options3,
                    // $options2 . ' ' . $options3
            );
        }


        if (!empty($data['labs'])) {
            $output = array(
                "draw" => intval($requestData['draw']),
                "recordsTotal" => $this->db->get('lab')->num_rows(),
                "recordsFiltered" => $this->db->get('lab')->num_rows(),
                "data" => $info
            );
        } else {
            $output = array(
                // "draw" => 1,
                "recordsTotal" => 0,
                "recordsFiltered" => 0,
                "data" => []
            );
        }




        echo json_encode($output);
    }

    function add_new_test()
    {
        //echo "aaa";die;
        $data = $this->input->post();
        
            $result = $this->lab_model->add_test($data);

            if ($result == True) 
            {
                echo "1";
                $this->session->set_flashdata('feedback', 'Inserted');

            }
            else
            {
                echo "0";
            }
    }

    function generate_report_submit_pdf()
    {


        ini_set('memory_limit','32M');
        $data = array();
        $invoice_id = trim($this->input->post('invoice_id'));
        $doc_id = $this->input->post('doc_id');
        $doc = $doc_id[0];
        //echo $doc;die;
        //print_r($doc_id);
       // echo $doc_id[0];die;
        //print_r($f_doc_id);die;
        // foreach ($f_doc_id as $doc) {
        //     print_r($doc) ;
        // }

        //print_r($doc_id);die;
        $category_id = $this->input->post('hid_category_id');


        //echo $category_id;die;
        $data['settings'] = $this->settings_model->getSettings();
       
        //$data['test_parameters'] = $this->lab_model->gettest_parametersId($invoice_id);
        $data['patients_payments'] = $this->lab_model->getpatients_paymentsId($invoice_id);
        
        $test_parameter = $this->lab_model->test_parameter_group_by($invoice_id);
        $groups = array();
        $i = 0;
        $j = 0;
        $group_name = '';
        $test_heading = '';
        foreach($test_parameter as $test_param)
        {
            if($group_name != $test_param->group_name)
            {
                $i++;
            }
            $groups[$i]['group_name'] = $test_param->group_name;
            // $groups[$i]['comments'] = $test_param->comments;
            // $groups[$i]['note'] = $test_param->note;
            // $groups[$i]['interpretation'] = $test_param->interpretation;
            if($test_heading != $test_param->test_heading)
            {
                $groups[$i]['test_heading'][$j] = $test_param->test_heading;
                $groups[$i]['comments'][$j] = $test_param->comments;
                $groups[$i]['note'][$j] = $test_param->note;
                $groups[$i]['interpretation'][$j] = $test_param->interpretation;
            }
            $groups[$i]['test_parameter'][$j] = $test_param;
            $j++;
            $test_heading = $test_param->test_heading;
            $group_name = $test_param->group_name;
        }

        $data['test_parameters'] = $groups;
        $data['pathologys'] = $this->lab_model->getpathologysId($doc,$category_id,$invoice_id);
        //echo "<pre>";
        //print_r($data['pathologys']);die;
        //print_r($groups);die;
        // $data['get_Note'] = $this->lab_model->getNote($invoice_id);
        // $data['getGroup'] = $this->lab_model->getGroup($invoice_id);

        

         //echo "<pre>";
         


        // print_r($data['test_parameters']);die;



        //$data['discount_type'] = $this->finance_model->getDiscountType();
        //$data['payment'] = $this->finance_model->getPaymentById($id);
        //$this->load->view('home/dashboard'); // just the header file
        //$this->load->view('home/footer');

        //load the view and saved it into $html variable
        $stylesheet = file_get_contents(site_url() . 'common/css/pdf.css'); // Get css content

        $html=$this->load->view('pdf_invoice_report_generate', $data, true);
        $html_header=$this->load->view('pdf_header_test', $data, true);
        $html_footer=$this->load->view('pdf_footer', $data, true);

        //load mPDF library
        $this->load->library('pdf');
        $pdf = $this->pdf->load('utf-8', 'A4-L');
        $pdf->setAutoTopMargin = 'stretch'; // Set pdf top margin to stretch to avoid content overlapping
        $pdf->setAutoBottomMargin = 'stretch'; // Set pdf bottom margin to stretch to avoid content overlapping

        /*$pdf->WriteHTML($stylesheet, 1); // Writing style to pdf
        
        $header_html = '<htmlpageheader name="MyCustomHeader"><div class="pdf-header" style"height:auto;"><div class="header">'.$html_header.'</div></div></htmlpageheader>';
        $pdf->SetHTMLHeader($header_html);
        
        //$footer = '<htmlpagefooter name="MyCustomFooter"><div class="pdf-footer"><div class="footer">'.$html_footer.'</div></div></htmlpagefooter>';
        //$pdf->SetFooter($footer);
        //echo $stylesheet; echo $header_html; echo $html; echo $footer; exit();
        $pdf->SetHTMLFooter('<div class="pdf-footer" style"height:auto;"><div class="footer">'.$html_footer.'</div></div>');
        // $pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822)); // Add a footer for good measure ;)
       
        $pdf->WriteHTML($html); // write the HTML into the PDF
        */
        //echo $html_header; echo $html; echo $html_footer; exit();
        //echo $html;die;
        //$pdf->WriteHTML($stylesheet, 'HTML');

        $pdf->SetHTMLHeader('<htmlpageheader name="MyCustomHeader"><div class="pdf-header" style"height:auto;"><div class="header">'.$html_header.'</div></div></htmlpageheader>');
        $pdf->SetHTMLFooter('<htmlpagefooter name="MyCustomFooter"><div class="pdf-footer"><div class="footer">'.$html_footer.'</div></div></htmlpagefooter>');
        $pdf->WriteHTML($stylesheet, 1);
        $pdf->WriteHTML($html);

        //this the the PDF filename that user will get to download
        $pdfFilePath = time() . "_output_pdf_test.pdf";

        $pdf->Output($pdfFilePath, 'D'); // D for downloading file
        
    }

    function generate_report_pdf()
    {


        ini_set('memory_limit','32M');
        $data = array();
        $invoice_id = trim($this->input->get('id'));
        //echo $invoice_id;exit;
        $data['doc_cat'] = $this->lab_model->getDocCat($invoice_id);

        //print_r($data);die;
        $category_id = $data['doc_cat']->category_id;

        $doc_id = $data['doc_cat']->doc_id;

        //echo $doc_id;die;



        //echo $category_id;die;
        $data['settings'] = $this->settings_model->getSettings();
       
        //$data['test_parameters'] = $this->lab_model->gettest_parametersId($invoice_id);
        $data['patients_payments'] = $this->lab_model->getpatients_paymentsId($invoice_id);
        //print_r($data['patients_payments']);die;
        
        $test_parameter = $this->lab_model->test_parameter_group_by($invoice_id);
        $groups = array();
        $i = 0;
        $j = 0;
        $group_name = '';
        $test_heading = '';
        foreach($test_parameter as $test_param)
        {
            if($group_name != $test_param->group_name)
            {
                $i++;
            }
            $groups[$i]['group_name'] = $test_param->group_name;
            // $groups[$i]['comments'] = $test_param->comments;
            // $groups[$i]['note'] = $test_param->note;
            // $groups[$i]['interpretation'] = $test_param->interpretation;
            if($test_heading != $test_param->test_heading)
            {
                $groups[$i]['test_heading'][$j] = $test_param->test_heading;
                $groups[$i]['comments'][$j] = $test_param->comments;
                $groups[$i]['note'][$j] = $test_param->note;
                $groups[$i]['interpretation'][$j] = $test_param->interpretation;
            }
            $groups[$i]['test_parameter'][$j] = $test_param;
            $j++;
            $test_heading = $test_param->test_heading;
            $group_name = $test_param->group_name;
        }

        $data['test_parameters'] = $groups;
        $data['pathologys'] = $this->lab_model->getpathologysReportId($doc_id,$category_id,$invoice_id);

        //echo "<pre>";
        if($data == Sorry)
        {
        ?>
            <script type="text/javascript">
                alert("Name Available");
            </script>
        <?php
        }
        // print_r($groups);die;
        // $data['get_Note'] = $this->lab_model->getNote($invoice_id);
        // $data['getGroup'] = $this->lab_model->getGroup($invoice_id);

        

         //echo "<pre>";
         


        // print_r($data['test_parameters']);die;



        //$data['discount_type'] = $this->finance_model->getDiscountType();
        //$data['payment'] = $this->finance_model->getPaymentById($id);
        //$this->load->view('home/dashboard'); // just the header file
        //$this->load->view('home/footer');

        //load the view and saved it into $html variable
        $stylesheet = file_get_contents(site_url() . 'common/css/pdf.css'); // Get css content

        $html=$this->load->view('pdf_invoice_report_generate', $data, true);
        $html_header=$this->load->view('pdf_header_test', $data, true);
        $html_footer=$this->load->view('pdf_footer', $data, true);

        //load mPDF library
        $this->load->library('pdf');
        $pdf = $this->pdf->load('utf-8', 'A4-L');
        $pdf->setAutoTopMargin = 'stretch'; // Set pdf top margin to stretch to avoid content overlapping
        $pdf->setAutoBottomMargin = 'stretch'; // Set pdf bottom margin to stretch to avoid content overlapping

        /*$pdf->WriteHTML($stylesheet, 1); // Writing style to pdf
        
        $header_html = '<htmlpageheader name="MyCustomHeader"><div class="pdf-header" style"height:auto;"><div class="header">'.$html_header.'</div></div></htmlpageheader>';
        $pdf->SetHTMLHeader($header_html);
        
        //$footer = '<htmlpagefooter name="MyCustomFooter"><div class="pdf-footer"><div class="footer">'.$html_footer.'</div></div></htmlpagefooter>';
        //$pdf->SetFooter($footer);
        //echo $stylesheet; echo $header_html; echo $html; echo $footer; exit();
        $pdf->SetHTMLFooter('<div class="pdf-footer" style"height:auto;"><div class="footer">'.$html_footer.'</div></div>');
        // $pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822)); // Add a footer for good measure ;)
       
        $pdf->WriteHTML($html); // write the HTML into the PDF
        */
        //echo $html_header; echo $html; echo $html_footer; exit();
        //echo $html;die;
        //$pdf->WriteHTML($stylesheet, 'HTML');

        $pdf->SetHTMLHeader('<htmlpageheader name="MyCustomHeader"><div class="pdf-header" style"height:auto;"><div class="header">'.$html_header.'</div></div></htmlpageheader>');
        $pdf->SetHTMLFooter('<htmlpagefooter name="MyCustomFooter"><div class="pdf-footer"><div class="footer">'.$html_footer.'</div></div></htmlpagefooter>');
        $pdf->WriteHTML($stylesheet, 1);
        $pdf->WriteHTML($html);

        //this the the PDF filename that user will get to download
        $pdfFilePath = time() . "_output_pdf_test.pdf";

        $pdf->Output($pdfFilePath, 'D'); // D for downloading file
        
    }

}

/* End of file lab.php */
/* Location: ./application/modules/lab/controllers/lab.php */