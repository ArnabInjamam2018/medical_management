<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Lab_model extends CI_model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function insertLab($data) {
        $this->db->insert('lab', $data);
    }

    function getLab() {
        
        $this->db->order_by('id', 'desc');
        $query = $this->db->get('lab');
        return $query->result();
    }

    function getParameter()
    {
        $this->db->select('t.category_id,t.test_name,p.id, p.category');
        $this->db->from('test_parameter t');
        $this->db->join('payment_category p', 'p.id = t.category_id');
        $this->db->group_by('p.category');
        $this->db->order_by('id', 'desc');
        $query = $this->db->get();
        //zecho $query;die;
        return $query->result();

    }



    function getPaymentCategoryReport($id,$invoice_id)
    {
        //echo $id;die;
        // $sql_category = "select actual_value from test_parameter_actual_value where category_id='". $id ."'";
        // $result_cat = $this->db->query($sql_category);
        // if($result_cat->num_rows() > 0)
        // {
        //     echo "1";
        // }
        // else
        // {
            /*$query="SELECT   `t`.*, `p`.`id`, `p`.`category`, `p`.`test_heading`,tpav.id AS test_id,tpav.actual_value FROM (`test_parameter` t) 
                JOIN `payment_category` p ON `p`.`id` = `t`.`category_id` left JOIN `test_parameter_actual_value` tpav ON `tpav`.`id` = `t`.`id` WHERE `p`.`id` ='". $id ."';";*/
        /*$query ="SELECT `p`.`comments`,`p`.`interpretation`,`t`.*, `p`.`id`, `p`.`category`, `p`.`test_heading`,tpav.id AS test_id,ifnull(tpav.actual_value,'') actual_value FROM (`test_parameter` t) 
                JOIN `payment_category` p ON `p`.`id` = `t`.`category_id` left JOIN `test_parameter_actual_value` tpav ON `tpav`.`parameter_name` = `t`.`parameter_name` and tpav.invoice_id='". $invoice_id ."' WHERE `p`.`id` ='". $id ."';";*/
        $query = "SELECT distinct ifnull(`avh`.`comments`,`p`.`comments`) as comments,ifnull(`avh`.`interpretation`,`p`.`interpretation`) as interpretation,t.category_id, IFNULL(tpav.parameter_name, t.parameter_name) AS parameter_name, ifnull(tpav.test_unit,t.test_unit) as test_unit, ifnull(tpav.reference_range,t.reference_range) as reference_range, `p`.`id`, `p`.`category`,tpav.actual_value_bold, `p`.`test_heading`,tpav.id AS test_id,ifnull(tpav.actual_value,'') actual_value FROM (`test_parameter` t) JOIN `payment_category` p ON `p`.`id` = `t`.`category_id` left JOIN `test_parameter_actual_value` tpav ON `tpav`.`category_id` = `t`.`category_id` and tpav.invoice_id='". $invoice_id ."' left JOIN `actual_value_header` avh ON `avh`.`category_id` = `p`.`id` and avh.invoice_id='". $invoice_id ."' WHERE `p`.`id` ='". $id ."'";
            //echo $query;die;
            $results = $this->db->query($query)->result_array();


            
       
           return $results;
           //  echo json_encode($return_data);
        //}
        //die;


        //$return_data = array();

            

    }

    function getLabBySearch($search) 
    {
        $this->db->order_by('id', 'desc');
        $this->db->like('id', $search);
        $this->db->or_like('patient_name', $search);
        $this->db->or_like('patient_phone', $search);
        $this->db->or_like('patient_address', $search);
        $this->db->or_like('doctor_name', $search);
        $this->db->or_like('date_string', $search);
        $query = $this->db->get('lab');
        return $query->result();
    }

    function getLabByLimit($limit, $start) 
    {
        $this->db->order_by('id', 'desc');
        $this->db->limit($limit, $start);
        $query = $this->db->get('lab');
        return $query->result();
    }

    function getLabByLimitBySearch($limit, $start, $search) 
    {
        $this->db->order_by('id', 'desc');
        $this->db->like('id', $search);
        $this->db->or_like('patient_name', $search);
        $this->db->or_like('patient_phone', $search);
        $this->db->or_like('patient_address', $search);
        $this->db->or_like('doctor_name', $search);
        $this->db->or_like('date_string', $search);
        $this->db->limit($limit, $start);
        $query = $this->db->get('lab');
        return $query->result();
    }

    // function getLabById($id) {
    //     $this->db->where('id', $id);
    //     $query = $this->db->get('lab');
    //     return $query->row();
    //     //echo  $this->db->last_query();die;
    // }
// ------------------------------  featch email data from patient table   -------------------------------- //
    function getLabById($id) 
    {
                $this->db->select('l.*, p.email');
                $this->db->from('lab l');
                $this->db->join('patient p', 'p.id = l.patient');
                $this->db->where('l.id', $id);        
                $query = $this->db->get();
                return $query->row();
                 //echo $this->db->last_query();die;
    }


    
    function getReportByInvId($id) 
    
    {//echo "aaaa";die;
        
                $this->db->select('py.*, p.email,p.name');
                $this->db->from('payment py');
                $this->db->join('patient p', 'p.id = py.patient');
                $this->db->where('py.id', $id);        
                $query = $this->db->get();
               // echo $this->db->last_query();die;
                return $query->row();
                 
    }


    function getpatients_paymentsId($invoice_id) 
    {
        $qry_patient = "select p.*,py.* from patient p, payment py where py.patient=p.id and py.id='".$invoice_id."'";
        //echo $qry_patient;die;
        $results = $this->db->query($qry_patient)->row();
                // echo $this->db->last_query();die;
                return $results;
    }

    function test_parameter_group_by($invoice_id)
    {
        /*$qry_grp = "select t.*,pc.category,pc.test_heading,gm.group_id,gm.group_name,replace(pc.comments,'\n','\r\n') comments,pc.note,pc.interpretation from patient p, payment py,test_parameter_actual_value t,payment_category pc, group_master gm where py.patient=p.id and t.patient_id=p.id and t.category_id=pc.id and pc.test_group_id=gm.group_id and py.id=t.invoice_id and py.id='". $invoice_id."' order by pc.category";*/

        $qry_grp = "select t.*,pc.category,pc.test_heading,gm.group_id,gm.group_name,replace(act.comments,'\n','\r\n') comments,pc.note,replace(act.interpretation,'\n','\r\n') interpretation from patient p, payment py,test_parameter_actual_value t,payment_category pc, group_master gm ,actual_value_header act where py.patient=p.id and t.patient_id=p.id and t.category_id=pc.id and pc.test_group_id=gm.group_id and py.id=t.invoice_id and py.id='". $invoice_id ."' and act.category_id=t.category_id and act.invoice_id =py.id order by pc.category";
        //echo $qry_grp;exit;
        $results = $this->db->query($qry_grp)->result();
        return $results;  
     //echo $this->db->last_query();die;

    }

    function test_unit_count($invoice_id,$category_id)
    {
        /*$qry_grp = "select t.*,pc.category,pc.test_heading,gm.group_id,gm.group_name,replace(pc.comments,'\n','\r\n') comments,pc.note,pc.interpretation from patient p, payment py,test_parameter_actual_value t,payment_category pc, group_master gm where py.patient=p.id and t.patient_id=p.id and t.category_id=pc.id and pc.test_group_id=gm.group_id and py.id=t.invoice_id and py.id='". $invoice_id."' order by pc.category";*/

        $unit_count = "select count(test_unit) from test_parameter_actual_value where invoice_id='".$invoice_id."' and category_id='".$category_id."' and test_unit is null";
       // echo $unit_count;exit;
        $results = $this->db->query($unit_count)->result();
        return $results;  
     //echo $this->db->last_query();die;

    }



    // function gettest_parametersId($invoice_id) 
    // {
    //     $qry_parameter = "select t.* from patient p, payment py,test_parameter_actual_value t where py.patient=p.id and t.patient_id=p.id and py.id='".$invoice_id."'";
    //     $results = $this->db->query($qry_parameter)->result();
    //     return $results;  
    //             // echo $this->db->last_query();die;
    // }


    function getpathologysId($invoice_id) 
    {

   //echo $category_id;die;

        //$insert_path_doc = "update actual_value_header set doc_id='". $doc ."' where invoice_id='". $invoice_id ."'";

        //$result_path_doc = $this->db->query($insert_path_doc);

        $qry_pathology = "select distinct gm.group_name,pm.patho_name, pm.designation, pm.degree, pm.img_url,gm.doc_id,act.sig_required
 from group_master gm, payment_category pc,pathologist_master pm,actual_value_header act
where pc.test_group_id=gm.group_id and pm.id=gm.doc_id and act.category_id=pc.id and act.invoice_id='". $invoice_id ."'";
         //echo $qry_pathology;die;
        $results = $this->db->query($qry_pathology)->result_array();
         //print_r($results);die;

         //$return_data = array("status" => false, "patient" => $qry_results, "pathology" => $results);
        //return array_merge($results,$result);

         return $results;

         // if($result_path_doc == 1 && $results == 1)
         // {
         //    return true;
         // }
         // else
         // {
         //    return false;
         // }
        //return $results;


    }

    function getpathologysReportId($doc_id,$category_id,$invoice_id)
    {

        $exist_doc = "select count(id) from actual_value_header where invoice_id='". $invoice_id ."'";
        //echo $exist_doc;die;
        $results_doc = $this->db->query($exist_doc);
        $data_amt = mysqli_fetch_assoc($results_doc);

        if($data_amt)
        {
             $qry_pathology = "select * from pathologist_master where id in (".$doc_id.")";
        
             $results = $this->db->query($qry_pathology)->result_array();
            //echo $qry_pathology;die;
             return $results;

        }
        else
        {
            //echo "aaaaaaa";die;
             $qry = "Sorry";
             return false;
        }


        
         //print_r($results);die;

         //$return_data = array("status" => false, "patient" => $qry_results, "pathology" => $results);
        //return array_merge($results,$result);

         
    }

    // function getNote($invoice_id)
    // {
    //     $qry_payment_note = "select pc.note,pc.interpretation,pc.comments from payment_category pc,test_parameter_actual_value tpav,payment py where pc.id=tpav.category_id and tpav.patient_id=py.patient and py.id='". $invoice_id ."'";
    //     $results = $this->db->query($qry_payment_note)->row();
    //     return $results;
    // }

    // function getGroup($invoice_id)
    // {
    //     $qry_payment_cat = "select gm.group_name from group_master gm, payment_category pc,test_parameter_actual_value tpav,payment py where pc.id=tpav.category_id and tpav.patient_id=py.patient and pc.test_group_id=gm.group_id and py.id='". $invoice_id ."' group by gm.group_name;";
    //     $results = $this->db->query($qry_payment_cat)->result();
    //     return $results;

    // }




    function getLabByPatientId($id) {
        $this->db->order_by('id', 'desc');
        $this->db->where('patient', $id);
        $query = $this->db->get('lab');
        return $query->result();
    }

    function getLabByPatientIdByDate($id, $date_from, $date_to) {
        $this->db->order_by('id', 'desc');
        $this->db->where('patient', $id);
        $this->db->where('date >=', $date_from);
        $this->db->where('date <=', $date_to);
        $query = $this->db->get('lab');
        return $query->result();
    }

    function getLabByUserId($id) {
        $this->db->order_by('id', 'desc');
        $this->db->where('user', $id);
        $query = $this->db->get('lab');
        return $query->result();
    }

    function getOtLabByPatientId($id) {
        $this->db->order_by('id', 'desc');
        $this->db->where('patient', $id);
        $query = $this->db->get('ot_lab');
        return $query->result();
    }

    function getLabByPatientIdByStatus($id) {
        $this->db->where('patient', $id);
        $this->db->where('status', 'unpaid');
        $query = $this->db->get('lab');
        return $query->result();
    }

  
    function updateLab($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('lab', $data);
    }


    function insertLabCategory($data) {

        $this->db->insert('lab_category', $data);
    }

    function getLabCategory() {
        $query = $this->db->get('lab_category');
        return $query->result();
    }

    function getPaymentCategory($id) 
    {
      
        $this->db->select('p.id,p.category,p.test_heading,p.note,p.interpretation,p.comments');
        $this->db->from('payment_category p');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    function getTestParameter($id)
    {
        $this->db->select('t.id,t.parameter_name,t.category_id,t.test_unit,t.reference_range');
        $this->db->from('test_parameter t');
        $this->db->where('t.category_id', $id);
        $query = $this->db->get();
        return $query->result();

    }


    function getLabCategoryById($id) {
        $this->db->where('id', $id);
        $query = $this->db->get('lab_category');
        return $query->row();
    }


    function updateLabCategory($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('lab_category', $data);
    }

    function deleteLab($id) {
        $this->db->where('id', $id);
        $this->db->delete('lab');
    }

    function deleteLabCategory($id) {
        $this->db->where('id', $id);
        $this->db->delete('lab_category');
    }

    function getLabByDoctor($doctor) {
        $this->db->select('*');
        $this->db->from('lab');
        $this->db->where('doctor', $doctor);
        $query = $this->db->get();
        return $query->result();
    }

    function getLabByDate($date_from, $date_to) {
        $this->db->select('*');
        $this->db->from('lab');
        $this->db->where('date >=', $date_from);
        $this->db->where('date <=', $date_to);
        $query = $this->db->get();
        return $query->result();
    }

    function getLabByDoctorDate($doctor, $date_from, $date_to) {
        $this->db->select('*');
        $this->db->from('lab');
        $this->db->where('doctor', $doctor);
        $this->db->where('date >=', $date_from);
        $this->db->where('date <=', $date_to);
        $query = $this->db->get();
        return $query->result();
    }

 
    function getLabByUserIdByDate($user, $date_from, $date_to) {
        $this->db->order_by('id', 'desc');
        $this->db->select('*');
        $this->db->from('lab');
        $this->db->where('user', $user);
        $this->db->where('date >=', $date_from);
        $this->db->where('date <=', $date_to);
        $query = $this->db->get();
        return $query->result();
    }
    
    function insertTemplate($data) {
        $this->db->insert('template', $data);
    }

    function getTemplate() {
        $this->db->order_by('id', 'desc');
        $query = $this->db->get('template');
        return $query->result();
    }
    
    function updateTemplate($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('template', $data);
    }
    
    function getTemplateById($id) {
        $this->db->where('id', $id);
        $query = $this->db->get('template');
        return $query->row();
    }
    
    function deletetemplate($id) {
        $this->db->where('id', $id);
        $this->db->delete('template');
    }
	
	function getPatientDoctorByPayment($payment_id) {
        $this->db->where('id', $payment_id);
		$this->db->select('patient, doctor');
        $query = $this->db->get('payment');
        return $query->row();
    }
	
	function getPaymentWiseLabReport($payment_id) {
        $this->db->select('l.*');
        $this->db->from('lab l');
        //$this->db->join('payment p', 'p.patient = l.patient AND p.doctor = l.doctor', 'inner');
		$this->db->join('payment p', 'p.patient = l.patient', 'inner');
		$this->db->order_by('l.id', 'desc');
		$this->db->where('p.id', $payment_id);
        $query = $this->db->get();
        return $query->result();
    }

    function add_test($data)
    {
        //echo "aaa";die;
        $test_id= $data['parameter_id'];
        $test = $data['test'];
        $h_name = $data['h_name'];
        $parameter = $data['parameter'];
        $unit = $data['unit'];
        $ref_range = $data['ref_range'];
        $note = $data['note'];
        $interpretations = $data['interpretations'];
        $comments = $data['comments'];
        
        $count_parameter = count($parameter);
        $update_category = "UPDATE `payment_category` set test_heading='".$h_name."',note='". $this->db->escape_str($note) ."',interpretation='". $this->db->escape_str($interpretations) ."',comments = '". $this->db->escape_str($comments) ."' where id='".$test."'";
        $result_category = $this->db->query($update_category);
        for ($i=0; $i < $count_parameter; $i++) 
        { 
            if(!empty($test_id[$i]))
            {
                $sql ="update `test_parameter` set category_id='".$test."',parameter_name='".$parameter[$i]."',`test_unit`='".$unit[$i]."',reference_range ='".$ref_range[$i]."' WHERE id='".$test_id[$i]."'";
                $result = $this->db->query($sql);
            }
            else
            {
                $sql = 'INSERT INTO `test_parameter`(`category_id`, `parameter_name`, `test_unit`, `reference_range`) VALUES ("'.$test.'", "'.$parameter[$i].'", "'.$unit[$i].'", "'.$ref_range[$i].'")';
            //echo $sql;die;
            $result = $this->db->query($sql);
            }
            
        }



        if ($result == 1 && $result_category == 1) 
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    function getDocCat($invoice_id)
    {
        //echo $sql = "select t.category_id,a.doc_id from test_parameter_actual_value t,actual_value_header a,payment p where p.patient=t.patient_id and t.category_id=a.category_id and p.id='". $invoice_id."'";die;
        $sql = "select a.category_id,a.doc_id from actual_value_header a where a.invoice_id ='". $invoice_id ."'";

        $result = $this->db->query($sql);

        return $result->row();


    }

}
