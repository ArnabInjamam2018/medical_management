<!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- invoice start-->
        <section class="col-md-6">
                <page size="A4" layout="portrait">
            <div class="" id="lab">
                <!--<div class="panel-heading navyblue"> INVOICE</div>-->
                <div class="panel-body">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <thead><tr><td>
                        <div class="report_header text-center">
                                <img alt="" src="<?php echo $this->settings_model->getSettings()->logo; ?>" width="285" height="100">

                            <!--<h3>
                                <?php //echo $settings->title ?>
                            </h3>-->
                           
                            <h4 style="font-weight: bold; margin-top: 20px; text-transform: uppercase;margin:5px 0;">
                                <?php echo lang('lab_report') ?>
                                
                            </h4>
                        </div>
                          </td></tr>  </thead>
                        <tbody><tr><td>
                        <div class="report_body">
                             <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin:0;padding:0;" class="table">
                            <tr>
                                <td width="50%">
                                <p>
                                        <?php $patient_info = $this->db->get_where('patient', array('id' => $lab->patient))->row(); ?>
                                        <span><?php echo lang('patient'); ?> <?php echo lang('name'); ?> </span>
                                        <span style="text-transform: uppercase;"> : 
                                            <?php
                                            if (!empty($patient_info)) {
                                                echo $patient_info->name . ' <br>';
                                            }
                                            ?>
                                        </span>
                                    </p>
                                    <p>
                                        <span><?php echo lang('patient_id'); ?>  </span>
                                        <span style="text-transform: uppercase;"> : 
                                            <?php
                                            if (!empty($patient_info)) {
                                                echo $patient_info->id . ' <br>';
                                            }
                                            ?>
                                        </span>
                                    </p>
                                    <p>
                                        <span> <?php echo lang('address'); ?> </span>
                                        <span style="text-transform: uppercase;"> : 
                                            <?php
                                            if (!empty($patient_info)) {
                                                echo $patient_info->address . ' <br>';
                                            }
                                            ?>
                                        </span>
                                    </p>
                                     <p>
                                        <span><?php echo lang('phone'); ?>  </span>
                                        <span style="text-transform: uppercase;"> : 
                                            <?php
                                            if (!empty($patient_info)) {
                                                echo $patient_info->phone . ' <br>';
                                            }
                                            ?>
                                        </span>
                                    </p>
                                </td>
                                <td width="50%">
                                <p>
                                        <span> <?php echo lang('age'); ?>  </span>
                                        <span style="text-transform: uppercase;"> : 
                                            <?php
                                            if (!empty($patient_info->id)) {
                                                echo $patient_info->age;
                                            }
                                            ?>
                                        </span>
                                    </p>

                                <p>
                                        <span> <?php echo lang('lab'); ?> <?php echo lang('report'); ?> <?php echo lang('id'); ?>  </span>
                                        <span style="text-transform: uppercase;"> : 
                                            <?php
                                            if (!empty($lab->id)) {
                                                echo $lab->id;
                                            }
                                            ?>
                                        </span>
                                    </p>
                                    <p>
                                        <span><?php echo lang('date'); ?>  </span>
                                        <span style="text-transform: uppercase;"> : 
                                            <?php
                                            if (!empty($lab->date)) {
                                                echo date('d-m-Y', $lab->date) . ' <br>';
                                            }
                                            ?>
                                        </span>
                                    </p>
                                    <p>
                                        <span><?php echo lang('doctor'); ?>  </span>
                                        <span style="text-transform: uppercase;"> : 
                                            <?php
                                            if (!empty($lab->doctor)) {
                                                $doctor_details = $this->doctor_model->getDoctorById($lab->doctor);
                                                if (!empty($doctor_details)) {
                                                    echo $doctor_details->name. '<br>';
                                                }
                                            }
                                            ?>
                                        </span>
                                    </p>
                                </td>                                
                                </tr>
                            </table>
                           
                        
                    <div class="col-md-12 panel-body">
                        <?php
                        if (!empty($lab->report)) {
                            echo $lab->report;
                        }
                        ?>
                    </div>
                            
                            </div>
                          </td></tr>  </tbody>
                        <tfoot><tr><td>
                    <div class="report_footer text-center">
                                <p>
                                <?php echo $settings->address ?>
                            </p>
                            <p>
                                Tel: <?php echo $settings->phone ?>
                            </p>
                            <p>
                                Email: <?php echo $settings->email ?>   Web: <?php echo $settings->web ?>
                            </p>
                            </div>
                         </td></tr>   </tfoot>
                        </table>
                </div>
            </div>
                </page>



        </section>


        <section class="col-md-6">

            <div class="col-md-5 no-print" style="margin-top: 20px;">
                <div class="text-center col-md-12 row">
                    <a href="lab" class="btn btn-info btn-sm info pull-left"><i class="fa fa-arrow-circle-left"></i>  <?php echo lang('back_to_lab_module'); ?> </a>
                    <a class="btn btn-info btn-sm invoice_button pull-left" onclick="javascript:window.print();"><i class="fa fa-print"></i> <?php echo lang('print'); ?> </a>

                    <a class="btn btn-info btn-sm detailsbutton pull-left download" id="download"><i class="fa fa-download"></i> <?php echo lang('download'); ?> </a>

                    <?php if ($this->ion_auth->in_group(array('admin', 'Laboratorist'))) { ?>
                        <a href="lab?id=<?php echo $lab->id; ?>" class="btn btn-info btn-sm blue pull-left"><i class="fa fa-edit"></i> <?php echo lang('edit_report'); ?> </a>
                    <?php } ?>


                </div>

                <?php if ($this->ion_auth->in_group(array('admin', 'Laboratorist'))) { ?>
                    <div class="no-print">


                        <a href="lab" class="pull-left">
                            <div class="btn-group">
                                <button id="" class="btn green btn-sm">
                                    <i class="fa fa-plus-circle"></i> <?php echo lang('add_a_new_report'); ?>
                                </button>
                            </div>
                        </a>
                    </div>
                <?php } ?>

            </div>
     </section>



<style media="print">
           page {background: #ccc;display: block;height: 100%; margin: 0 auto;margin-bottom: 0.5cm;-webkit-box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);-moz-box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);-o-box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);}
           
            @media print {           
                p{margin: 0;font-size: 11px;font-family: 'Arial';}
                page {background: #333;margin: 0;box-shadow: 0;position: relative;}
                body {font-size: 13px;padding: 0;background: #333;background-color:#FFFFFF;margin: 0px;}
                .report_header {position: fixed;left: 0;width: 100%;width: 100%; text-align: center;top: 0;height: 150px;border-bottom:1px solid #ccc; padding: 10px;}
                .report_body {page-break-after: always;margin-top: 150px;margin-bottom: 100px;height: 100%;}
                .report_footer {position: fixed;bottom: 0;left: 0; height:80px;width: 100%;padding: 10px; text-align: center;border-top:1px solid #ccc; }
                thead {display: table-header-group;} 
                tfoot {display: table-footer-group;width: 100%;}
                .table>tbody>tr>td{border-top: none;}
            }
        </style>






   
        <!-- invoice end-->
    </section>
</section>
<!--main content end-->
<!--footer start-->
<style>
    p{margin: 0;}
    table td {padding:5px 10px}
    pre {display: none;}
</style>
<script src="common/js/codearistos.min.js"></script>

<script>
                        $(document).ready(function () {
                            $(".flashmessage").delay(3000).fadeOut(100);
                        });
</script>





<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.0.272/jspdf.debug.js"></script>

<script>


                        $('#download').click(function () {
                            var pdf = new jsPDF('p', 'pt', 'letter');
                            pdf.addHTML($('#lab'), function () {
                                pdf.save('lab_id_<?php echo $lab->id; ?>.pdf');
                            });
                        });

                        // This code is collected but useful, click below to jsfiddle link.
</script>
