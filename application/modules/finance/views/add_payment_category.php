<!--sidebar end-->
<!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <section class="col-md-7">
            <header class="panel-heading">
                <?php
                if (!empty($category->id))
                    echo lang('edit_payment_category');
                else
                    echo lang('create_payment_procedure');
                ?>
            </header>
            <div class="panel-body">
                <div class="adv-table editable-table ">
                    <div class="clearfix">
                        <?php echo validation_errors(); ?>
                        <form role="form" action="finance/addPaymentCategory" class="clearfix" method="post" enctype="multipart/form-data">
                            <div class="form-group"> 
                                <label for="exampleInputEmail1"><?php echo lang('category'); ?> <?php echo lang('name'); ?></label>
                                <input type="text" class="form-control" name="category" id="exampleInputEmail1" value='<?php
                                if (!empty($setval)) {
                                    echo set_value('category');
                                }
                                if (!empty($category->category)) {
                                    echo $category->category;
                                }
                                ?>' placeholder="">    
                            </div> 

                            <div class="form-group">
                                <label for="exampleInputEmail1"><?php echo lang('category'); ?> Group</label>
                                <select class="form-control m-bot15 js-example-basic-single pos_select" name="testgroup"  id="testgroup">
                                <option value="" style="color: #41cac0 !important;"><?php ?>Select group.....</option>
                                <?php foreach ($t_groups as $group) { ?>
                                    <option value="<?php echo $group->group_id; ?>" <?php
                                    if (!empty($category->group_id)) {
                                        if ($category->group_id == $group->group_id) {
                                            echo 'selected';
                                        }
                                    }
                                    // if (!empty($payment->patient_bloodgroup)) {
                                    //     if ($group->group == $payment->patient_bloodgroup) {
                                    //         echo 'selected';
                                    //     }
                                    // }
                                    
                                    ?> > <?php echo $group->group_name; ?> </option>
                                        <?php } ?> 
                            </select>
                            </div> 



                            <div class="form-group"> 
                                <label for="exampleInputEmail1"><?php echo lang('test_code'); ?> <?php echo lang('code'); ?></label>
                                <input type="text" class="form-control" name="test_code" id="exampleInputEmail1" value='<?php
                                if (!empty($setval)) {
                                    echo set_value('test_code');
                                }
                                if (!empty($category->test_code)) {
                                    echo $category->test_code;
                                }
                                ?>' placeholder="">    
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1"><?php echo lang('description'); ?></label>
                                <input type="text" class="form-control" name="description" id="exampleInputEmail1" value='<?php
                                if (!empty($setval)) {
                                    echo set_value('description');
                                }
                                if (!empty($category->description)) {
                                    echo $category->description;
                                }
                                ?>' placeholder="">
                            </div>

                             <div class="form-group">
                                <label for="exampleInputEmail1"><?php echo lang('specimen'); ?></label>
                                <input type="text" class="form-control" name="specimen" id="exampleInputEmail1" value='<?php
                                if (!empty($setval)) {
                                    echo set_value('specimen');
                                }
                                if (!empty($category->specimen)) {
                                    echo $category->specimen;
                                }
                                ?>' placeholder="">
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1"><?php echo lang('category'); ?> <?php echo lang('price'); ?></label>
                                <input type="text" class="form-control" name="c_price" id="exampleInputEmail1" value='<?php
                                if (!empty($setval)) {
                                    echo set_value('c_price');
                                }
                                if (!empty($category->c_price)) {
                                    echo $category->c_price;
                                }
                                ?>' placeholder="" required>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1"><?php echo lang('doctors_commission'); ?> <?php echo lang('rate'); ?> (%)</label>
                                <input type="text" class="form-control" name="d_commission" id="exampleInputEmail1" value='<?php
                                if (!empty($setval)) {
                                    echo set_value('d_commission');
                                }
                                if (!empty($category->d_commission)) {
                                    echo $category->d_commission;
                                }
                                ?>' placeholder="">
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1"><?php echo lang('type'); ?></label>
                                <select class="form-control m-bot15" name="type" value=''>    
                                    <option value="diagnostic" <?php
                                    if (!empty($setval)) {
                                        if (set_value('type') == 'diagnostic') {
                                            echo 'selected';
                                        }
                                    }
                                    if (!empty($category->type)) {
                                        if ($category->type == 'diagnostic') {
                                            echo 'selected';
                                        }
                                    }
                                    ?> > <?php echo lang('diagnostic_test'); ?> </option>  
                                    <option value="others" <?php
                                    if (!empty($setval)) {
                                        if (set_value('type') == 'others') {
                                            echo 'selected';
                                        }
                                    }
                                    if (!empty($category->type)) {
                                        if ($category->type == 'others') {
                                            echo 'selected';
                                        }
                                    }
                                    ?> > <?php echo lang('others'); ?> </option>  
                                     <option value="opd" <?php
                                    if (!empty($setval)) {
                                        if (set_value('type') == 'opd') {
                                            echo 'selected';
                                        }
                                    }
                                    if (!empty($category->type)) {
                                        if ($category->type == 'opd') {
                                            echo 'selected';
                                        }
                                    }
                                    ?> > <?php echo lang('opd'); ?> </option>  
                                </select>
                            </div>

                            <input type="hidden" name="id" value='<?php
                            if (!empty($category->id)) {
                                echo $category->id;
                            }
                            ?>'>

                            <div class="form-group col-md-12">
                                <button type="submit" name="submit" class="btn btn-info pull-right"><?php echo lang('submit'); ?></button>
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
<!--footer start-->
