<!--sidebar end-->
<!--main content start-->
<?php 

$pathology = $path_doctor;

?>
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <section class="panel">
            <header class="panel-heading">
            Pathologist <?php echo lang('doctors'); ?>    
                <div class="col-md-4 no-print pull-right"> 
                    <a data-toggle="modal" href="#myModal">
                        <div class="btn-group pull-right">
                            <button id="" class="btn green btn-xs">
                                <i class="fa fa-plus-circle"></i> <?php echo lang('add_new'); ?>
                            </button>
                        </div>
                    </a>
                </div>
            </header>
            <div class="panel-body">
                <div class="adv-table editable-table ">
                    <div class="space15"></div>
                    <table class="table table-striped table-hover table-bordered" id="editable-sample">
                        <thead>
                            <tr>
                                <th>Group <?php echo lang('name'); ?></th>
                               
                                <th class="no-print"><?php echo lang('options'); ?></th>
                            </tr>
                        </thead>
                        <tbody>

                        <style>

                            .img_url{
                                height:20px;
                                width:20px;
                                background-size: contain; 
                                max-height:20px;
                                border-radius: 100px;
                            }

                        </style>



                        </tbody>
                    </table>
                </div>
            </div>
        </section>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
<!--footer start-->






<!-- Add Accountant Modal-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">  <?php echo lang('add_new_group'); ?></h4>
            </div>
            <div class="modal-body row">
                <form role="form" action="finance/addNewGroup" class="clearfix" method="post" enctype="multipart/form-data">
                    <div class="form-group col-md-6">
                        <label for="name"><?php echo lang('name'); ?></label>
                        <input type="text" class="form-control" name="name" id="name" value='' placeholder="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="name"> Select <?php echo lang('Pathologist'); ?> Doctor</label>
                            <select class="form-control m-bot15 js-example-basic-single add_doctor" id="add_doctor" name="doctor" value='' title="Select Pathology Doctor..">  
                                <?php foreach ($pathology as $pathology) { ?>
                                <option value="<?php echo $pathology->id; ?>"> <?php echo $pathology->patho_name; ?> </option>
                                <?php } ?> 
                                            
                            </select>
                    </div>

                    <div class="col-md-6 col-sm-2 col-xs-12">
                        <div class="item form-group">
                            <label class="control-label ">Signature Required : <span class="required">*</span></label>
                                <p>                                 
                                    <input required="" type="radio" id="signature_y" name="signature" value="Y" checked>
                                    <label for="signature_y"> Yes</label>                                                                        
                                    <input required="" type="radio" id="signature_n" name="signature" value="N">
                                    <label for="signature_n"> No</label>                                 
                                </p>
                        </div>
                     </div>
               
                    <div class="form-group col-md-12">
                        <button type="submit" name="submit" class="btn btn-info pull-right"><?php echo lang('submit'); ?></button>
                    </div>

                </form>

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!-- Add Accountant Modal-->







<!-- Edit Event Modal-->
<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title"> <?php echo lang('edit_group'); ?></h4>
            </div>
            <div class="modal-body">
                <form role="form" id="editDoctorForm" class="clearfix" action="finance/addNewGroup" method="post" enctype="multipart/form-data">
                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1"><?php echo lang('name'); ?></label>
                        <input type="text" class="form-control" name="name" id="exampleInputEmail1" value='' placeholder="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="name"> Select <?php echo lang('Pathologist'); ?> Doctor</label>
                            <select class="form-control m-bot15 js-example-basic-single add_doctor" id="edit_doctor" name="doctor" title="Select Pathology Doctor..">  
                                <?php foreach ($path_doctor as $path_doctor_row) { ?>
                                <option value="<?php echo $path_doctor_row->id; ?>"> <?php echo $path_doctor_row->patho_name; ?> </option> 
                                 <?php } ?>        
                            </select>
                    </div>

                    <div class="col-md-6 col-sm-2 col-xs-12">
                        <div class="item form-group">
                            <label class="control-label ">Signature Required : <span class="required">*</span></label>
                                <p>                                 
                                    <input required="" type="radio" id="sig_y" name="signature" value="Y">
                                    <label for="signature_y"> Yes</label>                                                                        
                                    <input required="" type="radio" id="sig_n" name="signature" value="N">
                                    <label for="signature_n"> No</label>                                 
                                </p>
                        </div>
                     </div>


                    <input type="hidden" name="id" value=''>
                    <div class="form-group col-md-12">
                        <button type="submit" name="submit" class="btn btn-info pull-right"><?php echo lang('submit'); ?></button>
                    </div>
                </form>

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!-- Edit Event Modal-->


<script src="common/js/codearistos.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {

    	var table = $('#editable-sample').DataTable({
            responsive: true,

            "processing": true,
            "serverSide": true,
            "searchable": true,
            "ajax": {
                url: "finance/getGroup_Master",
                type: 'POST',
                //dataType: 'json',
            },
            scroller: {
                loadingIndicator: true
            },

            dom: "<'row'<'col-sm-3'l><'col-sm-5 text-center'B><'col-sm-4'f>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5',
                {
                    extend: 'print',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5],
                    }
                },
            ],

            aLengthMenu: [
                [10, 25, 50, 100, -1],
                [10, 25, 50, 100, "All"]
            ],
            iDisplayLength: 100,
            "order": [[0, "desc"]],

            "language": {
                "lengthMenu": "_MENU_",
                search: "_INPUT_",
                searchPlaceholder: " Search..."
            }
        });
        table.buttons().container().appendTo('.custom_buttons');

        $(".flashmessage").delay(3000).fadeOut(100);


        $(".table").on("click", ".editbutton", function () {
            // Get the record's ID via attribute  
            var iid = $(this).attr('data-id');
            //alert(iid);
           // $("#img").attr("src", "uploads/cardiology-patient-icon-vector-6244713.jpg");
            $('#editDoctorForm').trigger("reset");
            $.ajax({
                url: 'finance/editGroupMasterByJason?id=' + iid,
                method: 'GET',
                data: '',
                dataType: 'json',
            }).success(function (response) {
                // Populate the form fields with the data returned from server
                $('#editDoctorForm').find('[name="id"]').val(response.group.group_id).end()
                $('#editDoctorForm').find('[name="name"]').val(response.group.group_name).end();
                //$('#editDoctorForm').find('[name="doctor"]').val(response.group.doc_id).end();
                //$("#edit_doctor").selectpicker('refresh');
                //alert(response.group.doc_id);
                //$("#edit_doctor").select2("val", response.group.doc_id);
                $("#edit_doctor").val(response.group.doc_id).trigger('change');
                if(response.group.sig_required == 'Y')
                {
                    //alert('A');
                    $("#sig_y").attr('checked', 'checked');
                }
                if(response.group.sig_required == 'N')
                {
                    $("#sig_n").attr('checked', 'checked');
                }
                

                //alert(response.group.doc_id);
                //generate_doctor(response.group.doc_id);

                //$('#editDoctorForm').find('[name="doctor"]').val(response.group.doc_id).end() 
               // $('.js-example-basic-single.department').val(response.doctor.department).trigger('change');

                $('#myModal2').modal('show');

            });
        });
    });

   //  function generate_doctor(selected)
   //  {
   //      //alert(selected);
   //      $.ajax({
   //          type:'GET',
   //          url:"finance/pathologist_doctor",
   //          //data:{action_type:'generate_report'},
   //          dataType: "json",
   //          encode: true
   //      })
   //      .done(function(data)
   //      {
   //      	//var rslt='<option value="">Select Pathology</option>';
        	
   //          //var rslt='<option value="">Select Pathology Doctor....</option>';

   //          $.each(data.path_doctor_list,function(key,value){
   //              var selected_text = '';
               
   //             	if (selected == value.id) {
   //             		selected_text = 'selected';
   //             	}

   //              //alert(value.id); 
   //              // $("#doc_hide").show();
   //              rslt +='<option value="'+value.id+'"'+ selected_text +'>'+value.patho_name+'</option>';
   //          })

   //          $("#add_doctor").html(rslt);
                      
			// $("#add_doctor").selectpicker('refresh');

   //      });
   //  }

</script>