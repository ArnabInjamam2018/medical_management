main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <header class="panel-heading"> 
            <?php echo lang('financial_report'); ?>  
            <div class="col-md-1 pull-right">
                <button class="btn btn-info green no-print pull-right" onclick="javascript:window.print();"><?php echo lang('print'); ?></button>
            </div>
        </header>
        <div class="col-md-12">
            <div class="col-md-12 row">
                <section>
                    <form role="form" class="f_report" action="finance/pathologywiseReport" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <!--     <label class="control-label col-md-3">Date Range</label> -->
                            <div class="col-md-6">
                                <div class="input-group input-large" data-date="13/07/2013" data-date-format="mm/dd/yyyy">
                                    <input type="text" class="form-control dpd1" name="date_from" value="<?php
                                    if (!empty($from)) {
                                        echo $from;
                                    }
                                    ?>" placeholder="<?php echo lang('date_from'); ?>" readonly="">
                                    <span class="input-group-addon"><?php echo lang('to'); ?></span>
                                    <input type="text" class="form-control dpd2" name="date_to" value="<?php
                                    if (!empty($to)) {
                                        echo $to;
                                    }
                                    ?>" placeholder="<?php echo lang('date_to'); ?>" readonly="">
                                </div>
                                <div class="row"></div>
                                <span class="help-block"></span> 
                            </div>
                            <div class="col-md-3">
                                <!-- <label for="exampleInputEmail1"> <?php echo lang('refd_by_doctor'); ?></label> -->
                                <select class="form-control m-bot15 js-example-basic-single add_doctor" id="add_doctor" name="doctor" value=''>  
                                    <option value=""><?php echo lang('select') . ' ' . lang('doctor'); ?></option>
                                    <?php foreach ($doctors as $doctor) { ?>
                                        <option value="<?php echo $doctor->id; ?>"<?php
                                        if (!empty($payment->doctor)) {
                                            if ($payment->doctor == $doctor->id) {
                                                echo 'selected';
                                            }
                                        }
                                        ?>><?php echo $doctor->name; ?> </option>
                                            <?php } ?>
                                </select>
                            </div>
                            <div class="col-md-3 no-print">
                                <button type="submit" name="submit" class="btn btn-info range_submit"><?php echo lang('submit'); ?></button>
                            </div>
                        </div>
                    </form>
                </section>
            </div>
        </div>


                                             

        <?php
        if (!empty($payments)) {
            $paid_number = 0;
            foreach ($payments as $payment) {
                $paid_number = $paid_number + 1;
            }
        }
        ?>
        <div class="row">
            <div class="col-md-12">
                <section class="panel">
                    <header class="panel-heading">
                        <?php echo lang('income'); ?> 
                    </header>
                    <table class="table table-striped table-advance table-hover" id="editable-sample">
                        <thead>
                            <tr>
                            <th> <?php echo lang('doctor'); ?></th>
                            <th> <?php echo lang('patient'); ?> Name</th>
                            <th> <?php echo lang('category'); ?> Name</th>
                            <th> <?php echo lang('date'); ?></th>
                            <th class="hidden-phone"> <?php echo lang('amount'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                         
                        $amount_per_category[] ='';
                        $opd_amount_per_category[] ='';
                            foreach($payments as $row){
                                foreach ($payment_categories as $cat_name) {
                                    $categories_in_payment = explode(',', $row->category_name);
                                    foreach ($categories_in_payment as $key => $category_in_payment) {
                                        $category_id = explode('*', $category_in_payment);
                                        if ($category_id[0] == $cat_name->id && $cat_name->type!='opd') {
                                            $amount_per_category[] = ($category_id[1] * $category_id[3]);
                                            ?>
                                            <tr>
                                                <td><?php echo $row->doctor_name; ?></td>
                                                <td><?php echo $row->patient_name; ?></td>
                                                <td><?php echo $cat_name->category; ?></td>
                                              
                                                <?php
                                                // if($cat_name->type=='opd'){
                                                //     //echo $cat_name->type;
                                                //     $opd_amount_per_category[]=$category_id[1] * $category_id[3];
                                                //     //print_r($opd_amount_per_category);die;
                                                    
                                                // }
                                                
                                                ?>
                                                <td><?php echo date('d-m-Y', $row->date); ?></td>
                                                <td align="right" style="text-align:right;"><?php echo $settings->currency; ?><?php echo ($category_id[1] * $category_id[3]); ?></td>
                                            </tr>
                                            <?php
                                        }
                                                        // if ($category_id[2] =='opd') {
                                                        // 	$opd_amount_per_category[] = ($category_id[1] * $category_id[3]);
                                                            
                                                        // }

                                                    }
                                                }
                                            }

                                            //print_r($opd_amount_per_category);die;                 	
                                            //print_r($amount_per_category);
                                            

                                            if (!empty($opd_amount_per_category)) {
                                            $total_opd_amt= array_sum($opd_amount_per_category);
                                            //echo $total_opd_amt;die;
                                            
                                                }


                                                                        // <!-- check opd test or pathology -->

                                            ?>





                           
                       </tbody>
                        
                        <tfoot>
                            
                                <tr>
                                <td>
                                    <div style="width:100%;float:left;display:table-row;"><?php echo lang('total'); ?></div>
                                  
                                    <div style="width:100%;float:left;display:table-row;"><?php echo lang('total'); ?> <?php echo lang('discount'); ?></div>
                                    <div style="width:100%;float:left;display:table-row;"><i class="fa fa-money"></i> <?php echo lang('gross_income'); ?></div>
                                    <div style="width:100%;float:left;display:table-row;">Total OPD Amount</div>
                                    <div style="width:100%;float:left;display:table-row;"><i class="fa fa-money"></i> <?php echo lang('commission_%'); ?>
                                    
                                    </div>
                                    <div style="width:100%;float:left;display:table-row;">Total OPD and Commission Amount</div>

                                    
                                    </td>
                                <td style="vertical-align: bottom !important;">
                                    <div style="width:100%;float:left;display:table-row;">-</div>
                                    <div style="width:100%;float:left;display:table-row;">-</div>
                                    <div style="width:100%;float:left;display:table-row;">-</div>
                               
                                    
                                    <div style="width:100%;float:left;display:table-row;">-</div>
                                    
                                    <div style="width:100%;float:left;display:table-row;">
                                    <?php

                                       foreach ($payments as $payment) {
                                        $doctor_commision[] = $payment->doctor_commision;
                                       

                                    }
                                    if(!empty($payment->doctor_commision)){
                                        echo $doctor_commision1 = $doctor_commision[0]; 
                                    }
                                 
                                    ?> </div>
                                    <div style="width:100%;float:left;display:table-row;">-</div>
                                      </td>
                                <td></td>
                                <td></td>
                                <td align="right" style="text-align:right;">
                                    <div style="width:100%;float:left;display:table-row;">
                                    <?php echo $settings->currency; ?> <?php
                                   if (!empty($amount_per_category)) {
                                    echo array_sum($amount_per_category);
                                    $total_payment_by_category[] = array_sum($amount_per_category);
                                    
                                } else {
                                    echo '0';
                                }
                                    ?> 
                                        </div>
                                     
                                    
                                    <div style="width:100%;float:left;display:table-row;">  
                                          <?php echo $settings->currency; ?> <?php
                                    if (!empty($payments)) {
                                        foreach ($payments as $payment) {
                                            $discount[] = $payment->flat_discount;
                                        }
                                        if ($paid_number > 0) {
                                            echo array_sum($discount);
                                        } else {
                                            echo '0';
                                        }
                                    } else {
                                        echo '0';
                                    }
                                    ?>
                                    </div>
                                    <div style="width:100%;float:left;display:table-row;">  
                                          <?php echo $settings->currency; ?> <?php
                                    if (!empty($payments)) {
                                        if ($paid_number > 0) {
                                            $gross = array_sum($total_payment_by_category) - array_sum($discount) ;
                                            echo $gross;
                                      } else {
                                           echo '0';
                                       }
                                  } else {
                                      echo '0';
                                   }
                                    ?>
                                    </div>
                                     <!--   total opd amt -->
                                     <div style="width:100%;float:left;display:table-row;">  
                                          <?php echo $settings->currency; ?> <?php
                                    if (!empty($total_opd_amt)) {

                                        echo $total_opd_amt;
                                         $tot_opd_sub_add=(array_sum($total_payment_by_category)-$total_opd_amt);
                                         
                                        //$tot_opd_sub_add = array_sum($total_payment_by_category) - array_sum($total_opd_amt) ;
                                        //$tot_opd_sub_add=($total_payment_by_category - $total_opd_amt);
                                      
                                       
                                    } else {
                                        echo '0';
                                        $tot_opd_sub_add=0;
                                    }
                                    ?>
                                    </div>
                                     <!--   total opd amt -->
















                                    <div style="width:100%;float:left;display:table-row;">  
                                       
                                        <i class="fa fa-money"></i> <?php echo lang('commission_amount'); ?> <?php echo $settings->currency; ?> <?php
                                    if (!empty($payments)) {
                                        if ($paid_number > 0) {
                                            $gross_commission= $tot_opd_sub_add * $doctor_commision[0] / 100;
                                            //$gross_commission = array_sum($total_payment_by_category) * $doctor_commision[0] / 100 ;
                                           

                                            
                                            echo $gross_commission;
                                      } else {
                                           echo '0';
                                       }
                                  } else {
                                      echo '0';
                                   }
                                    ?>
                                    </div>



                                    <!-- opd and commission amount -->

<div style="width:100%;float:left;display:table-row;">  
                                       
                                       <i class="fa fa-money"></i> <?php echo $settings->currency; ?> <?php
                                   if (!empty($payments)) {
                                       if ($paid_number > 0) {
                                           $gross_commission_tot= $gross_commission + $total_opd_amt;
                                           //$gross_commission = array_sum($total_payment_by_category) * $doctor_commision[0] / 100 ;
                                          

                                           
                                           echo $gross_commission_tot;
                                     } else {
                                          echo '0';
                                      }
                                 } else {
                                     echo '0';
                                  }
                                   ?>
                                   </div>
                                    <!-- opd and commission amount -->








                                   
                                </td>
                            
                            </tr>
                               
                              
                              
                        </tfoot>
                      </table>
                </section>


            </div>


            <style>
                .billl{
                    background: #39B24F !important;
                }

                .due{
                    background: #39B1D1 !important;
                }


            </style>

        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
<!--footer start-->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>


<script>


$(document).ready(function () {
    var table = $('#editable-sample').DataTable({
        destroy: true,
           ordering: true,
        responsive: true,
        //dom: 'lfrBtip',
        "paging": true,
            "bAutoWidth": false,
        //"processing": true,
        //"serverSide": true,
        footer: true,
        "searchable": true,
        //scroller: {
        //    loadingIndicator: true
        //},
        dom: "<'row'<'col-md-3'l><'col-sm-5 text-center'B><'col-sm-4'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-5'i><'col-sm-7'p>>",
        buttons: [
            /*'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5',*/
            /*{
                extend: 'print',
                exportOptions: {
                    columns: [1, 2, 3, 4, 5],
                }
            },*/
          { extend: 'copyHtml5', footer: true,},
          { extend: 'excelHtml5', footer: true,},
          { extend: 'csvHtml5', footer: true,},
          { extend: 'pdfHtml5', footer: true,
           customize: function(doc) {
             // doc.defaultStyle.alignment = 'right';
             // doc.styles.tableHeader.alignment = 'right';
               var rowCount = doc.content[1].table.body.length;
               //console.log('rowCount: ' + rowCount);
                for (i = 1; i < rowCount; i++) {
                    //console.log(doc.content[1].table.body[i][4]);
                    doc.content[1].table.body[i][4].alignment = 'right';
                };
               doc.content[1].table.widths = [100, 100, 100, 60, 150, '*']
               doc.styles.tableBodyEven.alignment = 'left';
               doc.styles.tableBodyOdd.alignment = 'left';
            }
          },
        ],
        
        aLengthMenu: [
            [10, 25, 50, 100, -1],
            [10, 25, 50, 100, "All"]
        ],
        iDisplayLength: 100,

        "order": [[0, "desc"]],

        "language": {
            "lengthMenu": "_MENU_",
             search: "_INPUT_",
            searchPlaceholder: " Search..."
        }
    });
    table.buttons().container().appendTo('.custom_buttons');     
    

});
</script>

<!-- <script type="text/javascript">
    $(document).ready(function () {
        var e = document.getElementById("add_doctor");
        var value = e.options[e.selectedIndex].text;alert(value);
        if(value != "" ) {
            $('#Others').show();
        } else {
            $('#Others').hide();
        }
    });
</script> -->
<!-- <script>
function row_hide_show(){

    var e = document.getElementById("add_doctor");
        var value = e.options[e.selectedIndex].text; alert(value);
        if(value != "") {
            $("#Others").show();
        } else {
            $("#Others").hide();
        }


}
</script> 