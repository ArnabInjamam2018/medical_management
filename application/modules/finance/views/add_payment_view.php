<!--sidebar end-->
<!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <section class="">
            <header class="panel-heading">
                <?php
                if (!empty($payment->id))
                    echo lang('edit_payment');
                else
                    echo lang('add_new_payment');
                ?>
            </header>
            <div class="">
                <div class="adv-table editable-table ">
                    <div class="clearfix">
                        <!--  <div class="col-lg-12"> -->
                        <div class="">
                           <!--   <section class="panel"> -->
                            <section class="">
                                <!--   <div class="panel-body"> -->
                                <div class="">
                                    <style> 
                        .payment { padding-top: 10px; padding-bottom: 0px; border: none; color: #31708f; background-color: #d9edf7; border-color: #bce8f1; }
                        .pad_bot { padding-bottom: 5px; }
                        .payment label { color: #31708f; }
                        .pos_client {padding: 0 15px;}
                        form { background: #f1f1f1; padding: 0 15px; }
                        .modal-body form { background: #fff; padding: 21px; }
                        .remove { width: 20%; float: right; margin-bottom:0px; padding:4px;text-align: center; border-bottom: 1px solid #f1f1f1;  background: #fff; color: #333;}
                        .remove1 { width: 80%; float: left; margin-bottom:0px; padding:4px; border-bottom: 1px solid #f1f1f1; }
                        form input { border: none; }
                        .pos_box_title { border: none; }
                        .qfloww { color: #3c763d; background-color: #dff0d8; border-color: #d6e9c6; height:180px;padding: 10px; }
                        
                        .ms-container { min-height: 235px; }
                        .ms-container .ms-selectable, .ms-container .ms-selection {width: 48%;}
                    </style>

                    <form role="form" id="editPaymentForm" class="clearfix" action="finance/addPayment" method="post"  autocomplete="off" enctype="multipart/form-data" style="background:#fff;">
                    <div class="row pad_bot">
                        <div class="col-md-6">
                            <!--
                            <div class="pull-right">
                                <a data-toggle="modal" href="#myModal">
                                    <div class="btn-group">
                                        <button id="" class="btn green">
                                            <i class="fa fa-plus-circle"></i> <?php echo lang('register_new_patient'); ?>
                                        </button>
                                    </div>
                                </a>
                            </div>
                            -->

                            <div class="col-md-12 payment pad_bot">
                                <label for="exampleInputEmail1"><?php echo lang('patient'); ?></label>
                                <select class="form-control m-bot15 js-example-basic-single pos_select" id="pos_select" name="patient" value=''> 
                                    <option value="add_new" style="color: #41cac0 !important;"><?php echo lang('add_new'); ?></option>
                                    <?php foreach ($patients as $patient) { ?>
                                        <option value="<?php echo $patient->id; ?>" <?php
                                        if (!empty($payment->patient)) {
                                            if ($payment->patient == $patient->id) {
                                                echo 'selected';
                                            }
                                        }
                                        ?> ><?php echo $patient->name .' - '. $patient->phone; ?> </option>
                                            <?php } ?>
                                </select>
                            </div> 
                           
                            <div class="pos_client">
                            <div class="row">
                                <div class="col-md-6 payment pad_bot">
                                    <label for="exampleInputEmail1"> <?php echo lang('patient'); ?> <?php echo lang('name'); ?></label>
                                    <span style="color:red;">*</span>
                                    <input type="text" class="form-control pay_in" name="p_name"  id="p_name"  value='<?php
                                    if (!empty($payment->patient_name)) {
                                        echo $payment->patient_name;
                                    }
                                
                                    ?>' placeholder="" required >
                                </div>
                                <div class="col-md-6 payment pad_bot">
                                    <label for="exampleInputEmail1"> <?php echo lang('patient'); ?> <?php echo lang('email'); ?></label> 
                                    <input type="text" class="form-control pay_in" name="p_email" id="p_email" value='<?php
                                    if (!empty($payment->patient_email)) {
                                        echo $payment->patient_email;
                                    }
                                    ?>' placeholder="">
                                </div>
                                <div class="col-md-6 payment pad_bot">
                                    <label for="exampleInputEmail1"> <?php echo lang('patient'); ?> <?php echo lang('address'); ?></label>
                                    <input type="text" class="form-control pay_in" name="address" id="address" value='<?php
                                    if (!empty($payment->patient_address)) {
                                        echo $payment->patient_address;
                                    }
                                    ?>' placeholder="" >
                                </div> 
                                <div class="col-md-6 payment pad_bot">
                                    <label for="exampleInputEmail1"> <?php echo lang('patient'); ?> <?php echo lang('phone'); ?></label>
                                    <span style="color:red;">*</span>
                                    <input type="text" class="form-control pay_in" name="p_phone" id="p_phone" value='<?php
                                    if (!empty($payment->patient_phone)) {
                                        echo $payment->patient_phone;
                                    }
                                    ?>' placeholder="">
                                </div>
                                <div class="col-md-6 payment pad_bot">
                                    <label for="exampleInputEmail1"> <?php echo lang('patient'); ?> <?php echo lang('age'); ?></label>
                                    <input type="text" class="form-control pay_in" name="p_age" id="p_age" value='<?php
                                    if (!empty($payment->patient_age)) {
                                        echo $payment->patient_age;
                                    }
                                    ?>' placeholder="">
                                </div> 
                                
                                <div class="col-md-6 payment pad_bot">
                                    <label for="exampleInputEmail1"> <?php echo lang('patient'); ?> <?php echo lang('birth_date'); ?></label>
                                    <input type="text" class="form-control pay_in default-date-picker" name="birthdate"  id="birthdate" value='<?php
                                    if (!empty($payment->patient_birthdate)) {
                                        echo $payment->patient_birthdate;
                                    }
                                    ?>' placeholder="" readonly="">
                                </div>
                                <div class="col-md-6 payment pad_bot">
                                    <label for="exampleInputEmail1"> <?php echo lang('patient'); ?> <?php echo lang('gender'); ?></label>
                                    <select class="form-control m-bot15" name="p_gender" id="p_gender">

                                        <option value="Male" <?php
                                        if (!empty($payment->patient_sex)) {
                                            if ($payment->patient_sex == 'Male') {
                                                echo 'selected';
                                            }
                                        }
                                        ?> > Male </option>   
                                        <option value="Female" <?php
                                        if (!empty($payment->patient_sex)) {
                                            if ($payment->patient_sex == 'Female') {
                                                echo 'selected';
                                            }
                                        }
                                        ?> > Female </option>
                                        <option value="Others" <?php
                                        if (!empty($payment->patient_sex)) {
                                            if ($payment->patient_sex == 'Others') {
                                                echo 'selected';
                                            }
                                        }
                                        ?> > Others </option>
                                    </select>
                                </div>
                                <div class="col-md-6 payment pad_bot">
                            <label for="exampleInputEmail1"><?php echo lang('blood_group'); ?></label>
                            <select class="form-control m-bot15" name="bloodgroup"  id="bloodgroup" >
                                <?php foreach ($groups as $group) { ?>
                                    <option value="<?php echo $group->group; ?>" <?php
                                    if (!empty($setval)) {
                                        if ($group->group == set_value('bloodgroup')) 
                                        {
                                            echo 'selected';
                                        }
                                    }
                                    if (!empty($payment->patient_bloodgroup)) {
                                        if ($group->group == $payment->patient_bloodgroup) {
                                            echo 'selected';
                                        }
                                    }
                                    
                                    ?> > <?php echo $group->group; ?> </option>
                                        <?php } ?> 
                            </select>
                        </div>
                            </div>
                            </div>	
                            <?php /*?>										
                                      <div class="col-md-6 payment pad_bot" id="reg_charge_div">
                                            <label for="exampleInputEmail1">Registration Charge</label>
                                                <input type="radio" name="reg_value" id="id_charge1" value="1"> Yes
                                                <input type="radio" name="reg_value" id="id_charge2" value="0" checked> No
                                                <input type="hidden" name="hidden_amt" id="hidden_amt" >
                                                <input type="hidden" name="hidden_charge" id="hidden_charge" >
                                        </div>
                                        <div class="col-md-6 payment pad_bot" id="reg_div" style="display:none;">
                                            <label for="exampleInputEmail1">Registration Charge</label>
                                            <input type="text" class="form-control" name="reg_charge" id="reg_charge" placeholder="" value=''>
                                        </div>
                                        <?php*/?>
                            
                        </div>

                        
                        <div class="col-md-6">                                            
                            <div class="col-md-6 payment pad_bot">
                                
                                
                                    <label for="exampleInputEmail1"><?php echo lang('date'); ?> </label>
                                
                                
                                    <input type="text" class="form-control  default-date-picker" name="date" id="" value='<?php
                            if (!empty($payment->date)) {
                                echo date('d-m-Y');
                            } else {
                                echo date('d-m-Y');
                            }
                            ?>' placeholder=" ">
                                
                                
                            </div>
                            <div class="col-md-6 payment pad_bot">
                                                 
                                               
                                            <label for="exampleInputEmail1"><?php echo lang('time'); ?> </label>
                                        
                                            <input type="text" class="form-control  timepicker-default" name="time" id="" value='<?php
                                    if (!empty($payment->time)) {
                                        echo time('h-i-s');
                                    } else {
                                        echo time('h-i-s');
                                    }
                                    ?>' placeholder=" ">
                                        
                                        
                                    </div>                                           
                                        <div class="col-md-12 payment pad_bot">
                                        <label for="exampleInputEmail1"> <?php echo lang('refd_by_doctor'); ?></label>
                                        <select class="form-control m-bot15 js-example-basic-single add_doctor" id="add_doctor" name="doctor" value=''> 
                                        <!-- <option value="" style="color: #41cac0 !important;">Select Doctor</option>  -->
                                            <option value="add_new" style="color: #41cac0 !important;"><?php echo lang('add_new'); ?></option>
                                            <?php foreach ($doctors as $doctor) { ?>
                                                <option value="<?php echo $doctor->id; ?>"<?php
                                                if (!empty($payment->doctor)) {
                                                    if ($payment->doctor == $doctor->id) {
                                                        echo 'selected';
                                                    }
                                                }
                                                ?>><?php echo $doctor->name; ?> </option>
                                                    <?php } ?>
                                        </select>
                                    </div>
                                    <input type="hidden" id="hidden_opd" name="opd_id" value='<?php
                                        if (!empty($payment->opd_flag) && $payment->opd_flag == 1) {
                                            echo $payment->opd_flag;
                                        }
                                        ?>'>
                                    
                                    <div class="pos_doctor">
                                        <div class="col-md-6 payment pad_bot">
                                            <label for="exampleInputEmail1"> <?php echo lang('doctor'); ?> <?php echo lang('name'); ?></label>
                                            <span style="color:red;">*</span>
                                            <input type="text" class="form-control pay_in" name="d_name" id="d_name" value='<?php
                                            if (!empty($payment->doctor_name)) {
                                                echo $payment->doctor_name;
                                            }
                                            ?>' placeholder=""  required>
                                        </div>
                                        <input type="hidden" name="static_val" id="static_val" value="1">
                                        <input type="hidden" name="opd_fees_paid" id="opd_fees_paid" value="N">

                                        
                                        <div class="col-md-6 payment pad_bot">
                                            <label for="exampleInputEmail1"> <?php echo lang('doctor'); ?> <?php echo lang('email'); ?></label>
                                            <!-- <span style="color:red;">*</span> -->
                                            <input type="text" class="form-control pay_in" name="d_email"  id="d_email" value='<?php
                                            if (!empty($payment->doctor_email)) {
                                                echo $payment->doctor_email;
                                            }
                                            ?>' placeholder="">
                                        </div>
                                        <div class="col-md-6 payment pad_bot"> 
                                            <label for="exampleInputEmail1"> <?php echo lang('doctor'); ?> <?php echo lang('phone'); ?></label>
                                            <span style="color:red;">*</span>
                                            <input type="text" class="form-control pay_in" name="d_phone" id="d_phone" value='<?php
                                            if (!empty($payment->doctor_phone)) {
                                                echo $payment->doctor_phone;
                                            }
                                            ?>' placeholder="">
                                        </div>
                                        <div class="col-md-6 payment pad_bot"> 
                                        <label for="exampleInputEmail1"><?php echo lang('department'); ?></label>
                                            <select class="form-control m-bot15 js-example-basic-single" name="department" id="department" value=''>
                                            <option value="">Select Department</option>
                                                <?php foreach ($departments as $department) { ?>
                                                    <option value="<?php echo $department->name; ?>"> <?php echo $department->name; ?> </option>
                                                <?php } ?> 
                                            </select>
                                        </div>
                                        <div class="col-md-6 payment pad_bot"> 
                                            <label for="exampleInputEmail1"> <?php echo lang('doctor'); ?> <?php echo lang('doctors_commission_%'); ?></label>
                                            <input type="text" class="form-control pay_in" name="d_commi" id="d_commi" 
                                            value="<?=!empty($payment->doctor_commision)?$payment->doctor_commision:'0.00'?>"
                                            placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-md-6 payment pad_bot">
                                        <label for="pos_collector"><?php echo lang('collector'); ?></label>
                                        <select class="form-control m-bot15 js-example-basic-single pos_select" id="pos_collector" name="collector"> 
                                            <option value=""><?php echo lang('select'); ?></option>
                                            <?php foreach ($collectors as $collector) { ?>
                                                <option value="<?php echo $collector->collection_id; ?>" <?php
                                                if (!empty($payment->collector)) {
                                                    if ($payment->collector == $collector->collection_id) {
                                                        echo 'selected';
                                                    }
                                                }
                                                ?> ><?php echo $collector->collector_name; ?> </option>
                                                    <?php } ?>
                                        </select>
                                    </div>
                                      <div class="form-group col-md-6" id="opd_flag_div" style="display:none;">
                                            <label for="exampleInputEmail1">OPD</label>
                                                <input type="radio" name="opd_value" id="id_radio1" value="1"> Yes
                                                <input type="radio" name="opd_value" id="id_radio2" value="0" checked> No
                                        </div>
                                        <div class="form-group col-md-6" id="opd_div" style="display:none;">
                                            <label for="exampleInputEmail1">OPD Time Slot</label>
                                            <input type="text" class="form-control" name="opd_time" id="opd_time" placeholder="" value='<?php
                                            if (!empty($payment->opd_time_slot)) {
                                                echo $payment->opd_time_slot;
                                            }
                                            ?>'>
                                        </div>
                                        <!-- <div class="form-group col-md-6" id="opd_amt_div" style="display:none;">
                                            <label for="exampleInputEmail1">OPD Amount</label>
                                            <input type="text" class="form-control" name="opd_amt" id="opd_amt" value='<?php
                                            // if (!empty($payment->opd_amt)) {
                                            //     echo $payment->opd_amt;
                                            // }else{
                                            //     echo '0.00';
                                            // }
                                            ?>' placeholder="">
                                        </div> -->
                                                        <div class="col-md-12 payment pad_bot">
                                        <div class="payment_label"> 
                                            <label for="exampleInputEmail1"><?php echo lang('patient_history'); ?> </label>
                                        </div>
                                        <div class=""> 
                                            <input type="text" class="form-control pay_in" name="history" id="history" value='<?php
                                            if (!empty($payment->history)) {

                                                echo $payment->history;
                                            }
                                            ?>' placeholder=" " maxlength="255">
                                        </div>

                                    </div> 
                                
                                </div>
                                
                            </div>
            <div class="row">
            <div class="col-md-7">
                <div class="col-md-12 payment">
                    <div class="form-group last"> 
                        <label for="exampleInputEmail1"> <?php echo lang('select'); ?></label>
                        <select name="category_name[]" class="multi-select" multiple="" id="my_multi_select3" required>
                            <?php foreach ($categories as $category) { ?>
                                <option class="ooppttiioonn" data-id="<?php echo $category->c_price; ?>" data-idd="<?php echo $category->id; ?>" data-cat_name="<?php echo $category->category; ?>" value="<?php echo $category->category; ?>" 

                                        <?php
                                        if (!empty($payment->category_name)) {
                                            $category_name = $payment->category_name;
                                            $category_name1 = explode(',', $category_name);
                                            foreach ($category_name1 as $category_name2) {
                                                $category_name3 = explode('*', $category_name2);
                                                if ($category_name3[0] == $category->id) {
                                                    echo 'data-qtity=' . $category_name3[3];
                                                }
                                            }
                                        }
                                        ?>

                                        <?php
                                        if (!empty($payment->category_name)) {
                                            $category_name = $payment->category_name;
                                            $category_name1 = explode(',', $category_name);
                                            foreach ($category_name1 as $category_name2) {
                                                $category_name3 = explode('*', $category_name2);
                                                if ($category_name3[0] == $category->id) {
                                                    echo 'selected';
                                                }
                                            }
                                        }
                                        ?>><?php echo $category->category; ?></option>
                                    <?php } ?>
                        </select>
                    </div>

                                    </div>

                                    <div class="col-md-12 qfloww">

                                        <label class=" col-md-10 pull-left remove1"><?php echo lang('items') ?></label><label class="pull-right col-md-2 remove"><?php echo lang('qty') ?></label>


                                    </div>

                                </div>



                                <div class="col-md-5">
                                    <div class="">
                                    <div class="col-md-6 payment">
                                        <div class="payment_label"> 
                                            <label for="exampleInputEmail1"><?php echo lang('sub_total'); ?> </label>
                                        </div>
                                        <div class=""> 
                                            <input type="text" class="form-control pay_in" name="subtotal" id="subtotal" value='<?php
                                            if (!empty($payment->amount)) {

                                                echo $payment->amount;
                                            }
                                            
                                            ?>' placeholder=" " disabled >
                                        </div>

                                    </div>
                                    <div class="col-md-6 payment">
                                        <div class="payment_label"> 
                                            <label for="exampleInputEmail1"><?php echo lang('discount'); ?>  <?php
                                                if ($discount_type == 'percentage') {
                                                    echo ' (%)';
                                                }
                                                ?> </label>
                                        </div>
                                        <div class=""> 
                                            <input type="text" class="form-control pay_in" name="discount" id="dis_id" value='<?php
                                            if (!empty($payment->discount)) {
                                                $discount = explode('*', $payment->discount);
                                                echo $discount[0];
                                            }
                                            ?>' placeholder="">
                                        </div>

                                    </div></div>
                                        <div class="col-md-12 payment">
                                        <div class="payment_label"> 
                                            <label for="exampleInputEmail1"><?php echo lang('discount_amount'); ?> </label>
                                        </div>
                                        <div class=""> 
                                            <input type="text" class="form-control pay_in" name="discount_amount" id="discount_amount" value='<?php
                                            if (!empty($payment->discount_amount)) {

                                                echo $payment->discount_amount;
                                            }
                                            ?>' placeholder=" " disabled>
                                        </div>

                                    </div>
                                    
                                    <div class="col-md-12 payment">
                                        <div class="payment_label"> 
                                            <label for="exampleInputEmail1">Registration Amount</label>
                                        </div>
                                        <div class=""> 
                                       
                                            <input type="text" class="form-control pay_in" name="reg_amount" id="reg_amount" value='0.00' placeholder=" ">
                                        </div>

                                    </div>


                                    <div class="col-md-12 payment">
                                        <div class="payment_label"> 
                                            <label for="exampleInputEmail1"><?php echo lang('gross_total'); ?> </label>
                                        </div>
                                        <div class=""> 
                                            <input type="text" class="form-control pay_in" name="grsss" id="gross" value='<?php
                                            if (!empty($payment->gross_total)) {

                                                echo $payment->gross_total;
                                            }
                                            ?>' placeholder=" " disabled>
                                        </div>

                                    </div>


                                    <div class="col-md-6 payment">
                                        <div class="payment_label"> 
                                            <label for="exampleInputEmail1"><?php echo lang('note'); ?> </label>
                                        </div>
                                        <div class=""> 
                                            <input type="text" class="form-control pay_in" name="remarks" id="" value='<?php
                                            if (!empty($payment->remarks)) {

                                                echo $payment->remarks;
                                            }
                                            ?>' placeholder=" ">
                                        </div>

                                    </div>  

                                    <div class="col-md-6 payment">

                                        <div class="payment_label"> 
                                            <label for="exampleInputEmail1"><?php
                                                if (empty($payment)) {
                                                    echo lang('deposited_amount');
                                                } else {
                                                    echo lang('deposit') . ' 1 <br>';
                                                    echo date('d/m/Y', $payment->date);
                                                };
                                                ?> </label>
                                        </div>

                                        <div class=""> 
                                            <input type="text" class="form-control pay_in" name="amount_received" id="amount_received" value='<?php
                                            if (!empty($payment->amount_received)) {

                                                echo $payment->amount_received;
                                            }
                                            else{ echo '0'; }
                                            ?>' placeholder=" " <?php
                                                    if (!empty($payment->deposit_type)) {
                                                        if ($payment->deposit_type == 'Card') {
                                                            echo 'readonly';
                                                        }
                                                    }
                                                    ?>>
                                        </div>

                                    </div>





                    <?php if (empty($payment->id)) { ?>
                        <div class="col-md-12 payment">
                            <div class="payment_label"> 
                                <label for="exampleInputEmail1"><?php echo lang('deposit_type'); ?></label>
                            </div>
                            <div class=""> 
                                <select class="form-control m-bot15 js-example-basic-single selecttype" id="selecttype" name="deposit_type" value=''> 
                                    <?php if ($this->ion_auth->in_group(array('admin', 'Accountant', 'Receptionist'))) { ?>
                                        <option value="Cash"> <?php echo lang('cash'); ?> </option>
                                        <option value="Card"> <?php echo lang('card'); ?> </option>
                                    <?php } ?>

                                </select>
                            </div>

                            <?php
                            $payment_gateway = $settings->payment_gateway;
                            ?>

                            <?php
                            if ($payment_gateway == 'PayPal') {
                                ?>

                                <div class = "card">

                                    
                                    <div class="col-md-12 payment pad_bot">
                                        <label for="exampleInputEmail1"> <?php echo lang('accepted'); ?> <?php echo lang('cards'); ?></label>
                                        <div class="payment pad_bot">
                                            <img src="uploads/card.png" width="200px">
                                        </div> 
                                    </div>


                                    <div class="col-md-12 payment pad_bot">
                                        <label for="exampleInputEmail1"> <?php echo lang('card'); ?> <?php echo lang('type'); ?></label>
                                        <select class="form-control m-bot15" name="card_type" value=''>

                                            <option value="Mastercard"> <?php echo lang('mastercard'); ?> </option>   
                                            <option value="Visa"> <?php echo lang('visa'); ?> </option>
                                            <option value="American Express" > <?php echo lang('american_express'); ?> </option>
                                        </select>
                                    </div>

                                    <div class="col-md-12 payment pad_bot">
                                        <label for="exampleInputEmail1"> <?php echo lang('card'); ?> <?php echo lang('number'); ?></label>
                                        <input type="text" class="form-control pay_in" name="card_number" value='<?php
                                        if (!empty($payment->p_email)) {
                                            echo $payment->p_email;
                                        }
                                        ?>' placeholder="">
                                    </div>



                                    <!-- <div class="col-md-8 payment pad_bot">
                                        <label for="exampleInputEmail1"> <?php //echo lang('expire'); ?> <?php //echo lang('date'); ?></label>
                                        <input type="text" class="form-control pay_in" data-date="" data-date-format="MM YY" placeholder="Expiry (MM/YY)" name="expire_date" maxlength="7" aria-describedby="basic-addon1" value='<?php
                                        //if (!empty($payment->p_phone)) {
                                            //echo $payment->p_phone;
                                        // }
                                        ?>' placeholder="">
                                    </div>
                                    <div class="col-md-4 payment pad_bot">
                                        <label for="exampleInputEmail1"> <?php //echo lang('cvv'); ?> </label>
                                        <input type="text" class="form-control pay_in" maxlength="3" name="cvv_number" value='<?php
                                        //if (!empty($payment->p_age)) {
                                            //echo $payment->p_age;
                                        //}
                                        ?>' placeholder="">
                                    </div>  -->

                                </div>

                                <?php
                            }
                            ?>

                        </div> 
                    <?php } ?>

                                    <?php
                    if (!empty($payment)) {
                        $deposits = $this->finance_model->getDepositByPaymentId($payment->id);
                        $i = 1;
                        foreach ($deposits as $deposit) {

                            if (empty($deposit->amount_received_id)) {
                                $i = $i + 1;
                                ?>
                                <div class="col-md-12 payment">
                                    <div class="payment_label"> 
                                        <label for="exampleInputEmail1"><?php echo lang('deposit'); ?> <?php
                                            echo $i . '<br>';
                                            echo date('d/m/Y', $deposit->date);
                                            ?> 
                                        </label>
                                    </div>
                                    <div class=""> 
                                        <input type="text" class="form-control pay_in" name="deposit_edit_amount[]" id="amount_received" value='<?php echo ($deposit->deposited_amount) ? $deposit->deposited_amount : '0'; ?>' <?php
                                        if ($deposit->deposit_type == 'Card') {
                                            echo 'readonly';
                                        }
                                        ?>>
                                        <input type="hidden" class="form-control pay_in" name="deposit_edit_id[]" id="amount_received" value='<?php echo $deposit->id; ?>' placeholder=" ">
                                    </div>

                                </div>
                                <?php
                            }
                        }
                    }
                    ?>

 <input type="hidden" id="static_val_hidden" name="static_val_hidden" value='<?php
                 if(!empty($static_val_hidden)){
                        echo $static_val_hidden;
                 }
                 else{
                    echo 0;
                 }
                 
                    ?>'>

                    <div class="col-md-12 payment">
                        <div class="col-md-12 form-group">
                            <button type="submit" name="submit" id="submit" onclick="finance_submit();" class="btn btn-info pull-right row"><?php echo lang('submit'); ?></button>
                        </div>
                    </div>

                </div>








                                <!--
                    <div class="col-md-12 payment">
                        <div class="col-md-3 payment_label"> 
                            <label for="exampleInputEmail1">Vat (%)</label>
                        </div>
                        <div class="col-md-9"> 
                            <input type="text" class="form-control pay_in" name="vat" id="exampleInputEmail1" value='<?php
                    if (!empty($payment->vat)) {
                        echo $payment->vat;
                    }
                    ?>' placeholder="%">
                        </div>
                    </div>
                    -->

                    <input type="hidden" id="hidden_id" name="id" value='<?php
                    if (!empty($payment->id)) {
                        echo $payment->id;
                    }
                    ?>'>
                     <input type="hidden" id="hidden_id" name="id" value='<?php
                    if (!empty($payment->id)) {
                        echo $payment->id;
                    }
                    ?>'>
                  
                    <div class="row">
                    </div>
                    <div class="form-group">
                    </div>

                        </div>
                        </form>





                </div>
                </section>
            </div>
        </div>
    </div>
    </div>
</section>

</section>
</section>
<!--main content end-->
<!--footer start-->

<script src="common/js/codearistos.min.js"></script>


            <!--

            <script>
            $(document).ready(function () {
            $('.multi-select').change(function () {
                $(".qfloww").html("");
                var tot = 0;
                $.each($('select.multi-select option:selected'), function () {
                    var curr_val = $(this).data('id');
                    var idd = $(this).data('idd');
                    tot = tot + curr_val;
                    var cat_name = $(this).data('cat_name');
                    $("#editPaymentForm .qfloww").append('<div class="remove1" id="id-div' + idd + '">  ' + $(this).data("cat_name") + '- <?php echo $settings->currency; ?> ' + $(this).data('id') + '</div><br>')
                });
                var discount = $('#dis_id').val();
            <?php
            if ($discount_type == 'flat') {
            ?>
                                                            var gross = tot - discount;
            <?php } else { ?>
                                                                                                                                                var gross = tot - tot * discount / 100;
            <?php } ?>
                $('#editPaymentForm').find('[name="subtotal"]').val(tot).end()
                $('#editPaymentForm').find('[name="grsss"]').val(gross)
            }

            );


            });

            $(document).ready(function () {
            $('#dis_id').keyup(function () {
                var val_dis = 0;
                var amount = 0;
                var ggggg = 0;
                amount = $('#subtotal').val();
                val_dis = this.value;
            <?php
            if ($discount_type == 'flat') {
            ?>
                                                                                                                                                ggggg = amount - val_dis;
            <?php } else { ?>
                                                                                                                                                ggggg = amount - amount * val_dis / 100;
            <?php } ?>
                $('#editPaymentForm').find('[name="grsss"]').val(ggggg)
            });
            });

            </script> 

            <script>
            $(document).ready(function () {

            $(".qfloww").html("");
            var tot = 0;
            $.each($('select.multi-select option:selected'), function () {
                var curr_val = $(this).data('id');
                var idd = $(this).data('idd');
                tot = tot + curr_val;
                var cat_name = $(this).data('cat_name');
                $("#editPaymentForm .qfloww").append('<div class="remove1" id="id-div' + idd + '">  ' + $(this).data("cat_name") + '- <?php echo $settings->currency; ?> ' + $(this).data('id') + '</div><br>')
            });
            var discount = $('#dis_id').val();
            <?php
            if ($discount_type == 'flat') {
            ?>
                                                                                                                                            var gross = tot - discount;
            <?php } else { ?>
                                                                                                                                            var gross = tot - tot * discount / 100;
            <?php } ?>
            $('#editPaymentForm').find('[name="subtotal"]').val(tot).end()
            $('#editPaymentForm').find('[name="grsss"]').val(gross)

            });

            $(document).ready(function () {
            $('#dis_id').keyup(function () {
                var val_dis = 0;
                var amount = 0;
                var ggggg = 0;
                amount = $('#subtotal').val();
                val_dis = this.value;
            <?php
            if ($discount_type == 'flat') {
            ?>
                                                                                                                                                ggggg = amount - val_dis;
            <?php } else { ?>
                                                                                                                                                ggggg = amount - amount * val_dis / 100;
            <?php } ?>
                $('#editPaymentForm').find('[name="grsss"]').val(ggggg)
            });
            });

            </script> 

            -->






<script>
$(document).ready(function () {

var tot = 0;
//  $(".qfloww").html("");
$(".ms-selected").click(function () {
    var idd = $(this).data('idd');
    $('#id-div' + idd).remove();
    $('#idinput-' + idd).remove();
    $('#categoryinput-' + idd).remove();

});
$.each($('select.multi-select option:selected'), function () {
    var curr_val = $(this).data('id');
    var idd = $(this).data('idd');
    var qtity = $(this).data('qtity');
    //  tot = tot + curr_val;
    var cat_name = $(this).data('cat_name');
    if ($('#idinput-' + idd).length)
    {

    } else {
        if ($('#id-div' + idd).length)
        {

        } else {
            $("#editPaymentForm .qfloww").append('<div class="remove1" id="id-div' + idd + '">  ' + $(this).data("cat_name") + '- <?php echo $settings->currency; ?> ' + $(this).data('id') + '</div>')
        }


        var input2 = $('<input>').attr({
            type: 'text',
            class: "remove",
            id: 'idinput-' + idd,
            name: 'quantity[]',
            value: qtity,
        }).appendTo('#editPaymentForm .qfloww');

        $('<input>').attr({
            type: 'hidden',
            class: "remove",
            id: 'categoryinput-' + idd,
            name: 'category_id[]',
            value: idd,
        }).appendTo('#editPaymentForm .qfloww');
    }


    $(document).ready(function () {
        $('#idinput-' + idd).keyup(function () {
            var qty = 0;
            var total = 0;
            $.each($('select.multi-select option:selected'), function () {
                var id1 = $(this).data('idd');
                qty = $('#idinput-' + id1).val();
                var ekokk = $(this).data('id');
                total = total + qty * ekokk;
            });

            tot = total;

            var discount = $('#dis_id').val();
            var discamt=tot * discount / 100;
            var gross = tot - discount;
            $('#editPaymentForm').find('[name="subtotal"]').val(tot).end()
            $('#editPaymentForm').find('[name="grsss"]').val(gross)
            $('#editPaymentForm').find('[name="discount_amount"]').val(discamt)
            $('#editPaymentForm').find('[name="d_name"]').val(doctor)


                    var amount_received = $('#amount_received').val();
                    var change = amount_received - gross;
                    $('#editPaymentForm').find('[name="change"]').val(change).end()


                });
            });
            var sub_total = $(this).data('id') * $('#idinput-' + idd).val();
            tot = tot + sub_total;


        });

        var discount = $('#dis_id').val();

<?php
if ($discount_type == 'flat') {
    ?>

            var gross = tot - discount;

<?php } else { ?>
    var discamt=tot * discount / 100;
            var gross = tot - tot * discount / 100;

<?php } ?>

        $('#editPaymentForm').find('[name="subtotal"]').val(tot).end()

        $('#editPaymentForm').find('[name="grsss"]').val(gross)
        $('#editPaymentForm').find('[name="discount_amount"]').val(discamt)

        var amount_received = $('#amount_received').val();
        var change = amount_received - gross;
        $('#editPaymentForm').find('[name="change"]').val(change).end()

    }

    );




    $(document).ready(function () {
        $('#dis_id').keyup(function () {
            var val_dis = 0;
            var amount = 0;
            var ggggg = 0;
            amount = $('#subtotal').val();
            val_dis = this.value;
            <?php
            if ($discount_type == 'flat') {
                ?>
                            ggggg = amount - val_dis;
            <?php } else { ?>
                            ggggg = amount - amount * val_dis / 100;
            <?php } ?>
            $('#editPaymentForm').find('[name="grsss"]').val(ggggg)


            var amount_received = $('#amount_received').val();
            var change = amount_received - ggggg;
            $('#editPaymentForm').find('[name="change"]').val(change).end()

        });
    });



</script> 

<script>
    $(document).ready(function () {

        $('.multi-select').change(function () {
            var tot = 0;
            //  $(".qfloww").html("");
            $(".ms-selected").click(function () {
                var idd = $(this).data('idd');
                $('#id-div' + idd).remove();
                $('#idinput-' + idd).remove();
                $('#categoryinput-' + idd).remove();

            });
            $.each($('select.multi-select option:selected'), function () {
                var curr_val = $(this).data('id');
                var idd = $(this).data('idd');
                //  tot = tot + curr_val;
                var cat_name = $(this).data('cat_name');
                if ($('#idinput-' + idd).length)
                {

                } else {
                    if ($('#id-div' + idd).length)
                    {

                    } else {
                        $("#editPaymentForm").find(".qfloww").append('<div class="remove1" id="id-div' + idd + '">  ' + $(this).data("cat_name") + '- <?php echo $settings->currency; ?> ' + $(this).data('id') + '</div>')
                    }


                    var input2 = $('<input>').attr({
                        type: 'text',
                        class: "remove",
                        id: 'idinput-' + idd,
                        name: 'quantity[]',
                        value: '1',
                    }).appendTo('#editPaymentForm .qfloww');

                    $('<input>').attr({
                        type: 'hidden',
                        class: "remove",
                        id: 'categoryinput-' + idd,
                        name: 'category_id[]',
                        value: idd,
                    }).appendTo('#editPaymentForm .qfloww');
                }


                $(document).ready(function () {
                    $('#idinput-' + idd).keyup(function () {
                        var qty = 0;
                        var total = 0;
                        $.each($('select.multi-select option:selected'), function () {
                            var id1 = $(this).data('idd');
                            qty = $('#idinput-' + id1).val();
                            var ekokk = $(this).data('id');
                            total = total + qty * ekokk;
                        });

                        tot = total;

                        var discount = $('#dis_id').val(); 

                        var gross = tot - discount;
                        $('#editPaymentForm').find('[name="subtotal"]').val(tot).end()
                        $('#editPaymentForm').find('[name="grsss"]').val(gross)

                        var amount_received = $('#amount_received').val();
                        var change = amount_received - gross;
                        $('#editPaymentForm').find('[name="change"]').val(change).end()


                    });
                });
                var sub_total = $(this).data('id') * $('#idinput-' + idd).val();
                tot = tot + sub_total;


            });

            var discount = $('#dis_id').val();

            <?php
            if ($discount_type == 'flat') {
                ?>

                            var gross = tot - discount;

            <?php } else { ?>
                var discamt=tot * discount / 100; 
                            var gross = tot - tot * discount / 100;

            <?php } ?>

            var reg_charge=$("#reg_amount").val();
            gross =gross + reg_charge * 1;
            $("#hidden_amt").val(gross);

            $('#editPaymentForm').find('[name="subtotal"]').val(tot).end()

            $('#editPaymentForm').find('[name="grsss"]').val(gross)
            $('#editPaymentForm').find('[name="discount_amount"]').val(discamt)

            var amount_received = $('#amount_received').val();
            var change = amount_received - gross;
            $('#editPaymentForm').find('[name="change"]').val(change).end()


        }

        );
    });

    $(document).ready(function () {
        $('#dis_id').keyup(function () {
            var val_dis = 0;
            var amount = 0;
            var ggggg = 0;
            amount = $('#subtotal').val();
            val_dis = this.value;
            <?php
            if ($discount_type == 'flat') {
                ?>
                            ggggg = amount - val_dis;
            <?php } else { ?>
                            ggggg = amount - amount * val_dis / 100;
                            
                            var discamt=amount * val_dis / 100; 
            <?php } ?>

            var reg_charge=$("#reg_amount").val();
               ggggg= ggggg + reg_charge *1;
              $("#hidden_amt").val(ggggg);

            $('#editPaymentForm').find('[name="grsss"]').val(ggggg);
            $('#editPaymentForm').find('[name="discount_amount"]').val(discamt)


            var amount_received = $('#amount_received').val();
            var change = amount_received - ggggg;
            $('#editPaymentForm').find('[name="change"]').val(change).end()





        });
    });

</script> 






<!-- Add Patient Modal-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title"><i class="fa fa-plus-circle"></i> Patient Registration</h4>
            </div>
            <div class="modal-body">
                <form role="form" action="patient/addNew?redirect=payment" method="post" enctype="multipart/form-data">

                    <div class="form-group">
                        <label for="exampleInputEmail1">Name</label>
                        <input type="text" class="form-control" name="name" id="exampleInputEmail1" value='' placeholder="">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Address</label>
                        <input type="text" class="form-control" name="address" id="exampleInputEmail1" value='' placeholder="">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Phone</label>
                        <input type="text" class="form-control" name="phone" id="exampleInputEmail1" value='' placeholder="">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Image</label>
                        <input type="file" name="img_url">
                    </div>

                    <input type="hidden" name="id" value=''>

                    <section class="">
                        <button type="submit" name="submit" class="btn btn-info">Submit</button>
                    </section>
                </form>

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!-- Add Patient Modal-->
<!--              ....................................mrityunjoy.........................................-->


<!--  -->

<!--        ....................................mrityunjoy end...........................................................-->
<script>
    $(document).ready(function () {

         $("#id_radio1").click(function() {
                $("#opd_amt_div").show();
                $("#opd_div").show();
                });
                // hides the table on clicking the noted link
                $("#id_radio2").click(function() {
                    $("#opd_amt_div").hide();
                    $("#opd_div").hide();
                    $("#opd_time").val('');
                    $("#opd_amt").val('');
             });

              $("#id_charge1").click(function() {
                $("#reg_div").show();

                });
                // hides the table on clicking the noted link
                $("#id_charge2").click(function() {
                    $("#reg_div").hide();
                    $("#reg_charge").val('');
                    $("#reg_amount").val(0.00);
                    var grsss_amt= $("#hidden_amt").val();
                    var grsss_charge= $("#hidden_charge").val(); 

                 var grsss_new= grsss_amt - grsss_charge *1;
                   
                   $("#gross").val(grsss_new);
                  
               


             });

        var hidden_id= $("#hidden_id").val();
        if(hidden_id){
            if($("#hidden_opd").val()==1 ){
                $("#id_radio1").prop("checked", true);
                $('#opd_flag_div').show();
                $("#opd_amt_div").show();
                    $("#opd_div").show();
            }
        }
             
        $('.pos_client').show();
        $(document.body).on('change', '#pos_select', function () {

            var v = $("select.pos_select option:selected").val()
            var v_id=$("#pos_select").val()
            if (v == 'add_new') {
               // $('#reg_charge_div').show();
               $("#static_val").val("1");
                $("#p_name").val("");
                $("#p_name").attr("readonly", false);

                $("#p_email").val("");
                $("#p_email").attr("readonly", false);

                $("#address").val("");
                $("#address").attr("readonly", false);
                
                $("#p_phone").val("");
                $("#p_phone").attr("readonly", false);

                $("#p_age").val("");
                $("#p_age").attr("readonly", false);

                $("#birthdate").val("");
                $("#birthdate").attr("readonly", false);

                $("#p_gender").val("");
                $("#p_gender").attr("readonly", false);

                $("#bloodgroup").val("");
                $("#bloodgroup").attr("readonly", false);


            } else {
                //$('#reg_charge_div').hide();
                //$('.pos_client').hide();

                $("#static_val").val("0");
                $.ajax({
                    type:'POST',
                    url:"finance/getPatient",
                    data: {v_id:v_id},
                    dataType: "json",
                    encode: true
                })
                .done(function(data){
                    console.log(data);
                    //alert(data.name);
                    $("#p_name").val(data.name);
                    $("#p_name").attr("readonly", true);

                    $("#p_email").val(data.email);
                    $("#p_email").attr("readonly", true);
                    if((data.registration_validity==null) && (data.opd_fees_paid=='Y')) {
                    $("#reg_amount").val(100);
                    }
                    else{
                    $("#reg_amount").val(0.00);
                        }
                    $("#reg_amount").attr("readonly", false);
                    $("#opd_fees_paid").val(data.opd_fees_paid);

                    




                    

                    $("#address").val(data.address);
                    $("#address").attr("readonly", true);
                   
                    $("#p_phone").val(data.phone);
                    $("#p_phone").attr("readonly", true);

                    $("#p_age").val(data.age);
                    $("#p_age").attr("readonly", true);

                    $("#birthdate").val(data.birthdate);
                    $("#birthdate").attr("readonly", true);

                    $("#p_gender").val(data.sex);
                    $("#p_gender").attr("readonly", true);

                    $("#bloodgroup").val(data.bloodgroup);
                    $("#bloodgroup").attr("readonly", true);


                });

            }
        });

    });


</script>

<script>
    $(document).ready(function () {
        $('.pos_doctor').show();
        $(document.body).on('change', '#add_doctor', function () {

            var v = $("select.add_doctor option:selected").val()
            var v_d_id=$("#add_doctor").val()
            if (v == 'add_new') {
                $('#opd_flag_div').show();
                $("#d_name").val("");
                $("#d_name").attr("readonly", false);

                $("#d_email").val("");
                $("#d_email").attr("readonly", false);

                $("#d_phone").val("");
                $("#d_phone").attr("readonly", false);

                $("#d_phone").val("");
                $("#d_phone").attr("readonly", false);

                $("#d_phone").val("");
                $("#d_phone").attr("readonly", false);
                
                // $("#history").val("");
                // $("#history").attr("readonly", false);

                
            } else {
                $('#opd_flag_div').hide();

                $.ajax({
                    type:'POST',
                    url:"finance/getDoctor",
                    data: {v_id:v_d_id},
                    dataType: "json",
                    encode: true
                })
                .done(function(data){
                    console.log(data);
                  
                    $("#d_name").val(data.name);
                    $("#d_name").attr("readonly", true);

                    $("#d_email").val(data.email);
                    $("#d_email").attr("readonly", true);

                    $("#d_phone").val(data.phone);
                    $("#d_phone").attr("readonly", true);

                    $("#department").val(data.department);
                    //$("#department").attr("readonly", true);
                   // $("#department").selectpicker('refresh');

                    $("#d_commi").val((data.doctor_commision)?data.doctor_commision:'0.00');
                    $("#d_commi").attr("readonly", true);
                   
                    // $("#history").val(data.history);
                    // $("#history").attr("readonly", true);

                  
                });

            }
        });
        $("#submit").attr('disabled', false);
    });


</script>

<script>
    $(document).ready(function () {
        $('.card').hide();
        $(document.body).on('change', '#selecttype', function () {

            var v = $("select.selecttype option:selected").val()
            if (v == 'Card') {
                $('.card').show();
            } else {
                $('.card').hide();
            }
        });

    });


</script>
<script>
     $("#reg_charge").keyup(function(){

         var reg_charge=$("#reg_charge").val();
         $("#reg_amount").val(reg_charge);
         $("#hidden_charge").val(reg_charge);
        //  var gross_amt=$("#gross").val();alert(gross_amt);
        //  gross_amt_new=(gross_amt+ reg_charge) * 1 ;alert(gross_amt_new);
        //  $("#gross").val(gross_amt_new);
               


     })

     function finance_submit(){
    $(this).attr("disabled", 'disabled');
}

</script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">



