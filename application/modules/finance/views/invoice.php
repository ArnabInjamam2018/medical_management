<!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- invoice start-->
        <section class="col-md-7">
            <div class="panel panel-primary" id="invoice">
                <!--<div class="panel-heading navyblue"> INVOICE</div>-->
                <page size="A4" layout="portrait"> 
                     <div class="panel-body">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <thead><tr><td>
                        <div class="report_header text-center">
                                <img alt="" src="<?php echo $this->settings_model->getSettings()->logo; ?>" width="285" height="100">

                            <!--<h3>
                                <?php //echo $settings->title ?>
                            </h3>-->
                           
                            <h4 style="font-weight: bold; margin-top: 20px; text-transform: uppercase;margin:5px 0;">
                                <?php echo lang('payment') ?>
                                <?php echo lang('invoice') ?>
                                
                            </h4>
                        </div>
                          </td></tr>  </thead>
                         <tbody><tr><td>
                        <div class="report_body">
                             <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin:0;padding:0;">
                           
                            <tr>
                                <td>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin:0;padding:0;" class="table">
                                        <tr>
                                             <td width="60%">
                                               
                                                    <p>
                                                        <span style="text-align:right;font-family: Arial;font-size:12px;" width="50%">
                                                            <?php $patient_info = $this->db->get_where('patient', array('id' => $payment->patient))->row(); ?> <span class="control-label"><?php echo lang('patient'); ?> <?php echo lang('name'); ?> </span> </span>
                                                        <span style="text-align:left;font-family: Arial;font-size:12px;" width="50%"> <span style="text-transform: uppercase;"> : 
                                            <?php 
                                            if (!empty($patient_info)) {
                                                echo $patient_info->name . ' <br>';
                                            }
                                            ?>
                                        </span></span>
                                                    </p>
                                                    <p>
                                                        <span style="text-align:right;font-family: Arial;font-size:12px;" width="50%"> <span class="control-label"><?php echo lang('patient_id'); ?>  </span></span>
                                                        <span style="text-align:left;font-family: Arial;font-size:12px;" width="50%"><span style="text-transform: uppercase;"> : 
                                            <?php
                                            if (!empty($patient_info)) {
                                                echo $patient_info->id . ' <br>';
                                            }
                                            ?>
                                        </span></span>
                                                    </p>
                                                    <p>
                                                        <span style="text-align:right;font-family: Arial;font-size:12px;" width="50%"><span class="control-label"> <?php echo lang('address'); ?> </span></span>
                                                        <span style="text-align:left;font-family: Arial;font-size:12px;" width="50%"><span style="text-transform: uppercase;"> : 
                                            <?php
                                            if (!empty($patient_info)) {
                                                echo $patient_info->address . ' <br>';
                                            }
                                            ?>
                                        </span></span>
                                                    </p>
                                                    <p>
                                                        <span style="text-align:right;font-family: Arial;font-size:12px;" width="50%"><span class="control-label"><?php echo lang('phone'); ?>  </span></span>
                                                        <span style="text-align:left;font-family: Arial;font-size:12px;" width="50%"> <span style="text-transform: uppercase;"> : 
                                            <?php
                                            if (!empty($patient_info)) {
                                                echo $patient_info->phone . ' <br>';
                                            }
                                            ?>
                                        </span></span>
                                                    </p>
                                            </td>
                                            <td width="40%">

                                                  <p>
                                                        <span style="text-align:right;font-family: Arial;font-size:12px;" width="50%"> <span class="control-label"><?php echo lang('age'); ?>  </span></span>
                                                        <span style="text-align:left;font-family: Arial;font-size:12px;" width="50%"><span style="text-transform: uppercase;"> : 
                                            <?php
                                            if (!empty($patient_info->id)) {
                                                echo $patient_info->age;
                                            }
                                            ?>
                                        </span></span>
                                                    </p>
                                               
                                                    <p>
                                                        <span style="text-align:right;font-family: Arial;font-size:12px;" width="50%"> <span class="control-label"><?php echo lang('invoice'); ?>  </span></span>
                                                        <span style="text-align:left;font-family: Arial;font-size:12px;" width="50%"><span style="text-transform: uppercase;"> : 
                                            <?php
                                            if (!empty($payment->id)) {
                                                echo $payment->id;
                                            }
                                            ?>
                                        </span></span>
                                                    </p>
                                                    <p>
                                                        <span style="text-align:right;font-family: Arial;font-size:12px;" width="50%"><span class="control-label"><?php echo lang('date'); ?>  </span></span>
                                                        <span style="text-align:left;font-family: Arial;font-size:12px;" width="50%"><span style="text-transform: uppercase;"> : 
                                            <?php
                                            if (!empty($payment->date)) {
                                                echo date('d-m-Y', $payment->date) . ' ';
                                            }
                                            ?>
                                            <?php
                                            if (!empty($payment->entry_time)) {
                                                echo ($payment->entry_time) . '<br> ';
                                            }
                                            ?>
                                        </span></span>
                                                    </p>
                                                    <p>
                                                        <span style="text-align:right;font-family: Arial;font-size:12px;" width="50%"><span class="control-label"><?php echo lang('doctor'); ?>  </span></span>
                                                        <span style="text-align:left;font-family: Arial;font-size:12px;" width="50%"> <span style="text-transform: uppercase;"> : 
                                            <?php
                                            if (!empty($payment->doctor)) {
                                                $doc_details = $this->doctor_model->getDoctorById($payment->doctor);
                                                if (!empty($doc_details)) {
                                                    echo $doc_details->name . ' <br>';
                                                } else {
                                                    echo $payment->doctor_name . ' <br>';
                                                }
                                            }
                                            ?>
                                        </span></span>
                                                    </p>
                                               
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                   
                   <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin:0;padding:10px;" class="table table-striped table-hover">
                        <thead class="theadd">
                            <tr>
                                <th>#</th>
                                <th>
                                    <?php echo lang('description'); ?>
                                </th>
                                <th style="width:100px;">
                                    <?php echo lang('unit_price'); ?>
                                </th>
                                <th>
                                    <?php echo lang('qty'); ?>
                                </th>
                                <th>
                                    <?php echo lang('amount'); ?>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if (!empty($payment->category_name)) {
                                $category_name = $payment->category_name;
                                $category_name1 = explode(',', $category_name);
                                $i = 0;
                                foreach ($category_name1 as $category_name2) {
                                    $i = $i + 1;
                                    $category_name3 = explode('*', $category_name2);
                                    if ($category_name3[3] > 0) {
                                        ?>
                                <tr>
                                    <td>
                                        <?php echo $i; ?>
                                    </td>
                                    <td style="width: 245px;">
                                        <?php echo $this->finance_model->getPaymentcategoryById($category_name3[0])->category; ?>
                                    </td>
                                    <td class="">
                                        <?php echo $settings->currency; ?>
                                            <?php echo $category_name3[1]; ?>
                                    </td>
                                    <td class="">
                                        <?php echo $category_name3[3]; ?>
                                    </td>
                                    <td style="text-align:right">
                                        <?php echo $settings->currency; ?>
                                            <?php echo $category_name3[1] * $category_name3[3]; ?>
                                    </td>
                                </tr>
                                <?php
                                    }
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                    <div class="">
                        <div class="invoice-block text-right" style="padding:10px;margin-top:20px;">
                            <div class="unstyled amounts">
                                <p><?php echo lang('sub_total'); ?> :
                                   <strong> <?php echo $settings->currency; ?>
                                        <?php echo $payment->amount; ?> </strong>
                                </p>
                                <?php if (!empty($payment->discount)) { ?>
                                    <p><?php echo lang('discount'); ?>:
                                       <strong>    
                                         <?php
                                        $discount = explode('*', $payment->discount);
                                        if (!empty($discount[1])) {
                                            echo $discount[0] . ' %  =  ' . $settings->currency . ' ' . $discount[1];
                                        } else {
                                            echo $discount[0];
                                        }
                                        ?>
                                        <?php
                                        if ($discount_type == 'percentage') {
                                            echo '(%) ';
                                        } else {
                                            echo  $settings->discount;
                                        }
                                        ?></strong>
                                    </p>
                                    <?php } ?>
                                        <?php if (!empty($payment->vat)) { ?>
                                            <p>VAT :
                                               <strong> <?php
                                        if (!empty($payment->vat)) {
                                            echo $payment->vat;
                                        } else {
                                            echo '0';
                                        }
                                        ?> % =
                                                    <?php echo $settings->currency . ' ' . $payment->flat_vat; ?></strong>
                                            </p>
                                            <?php } ?>
                                            <!-- <p>Registration Fees :
                                                   <strong> <?php //echo $settings->currency; ?>
                                                        <?php //echo $payment->registration_fees; ?> </strong>
                                                </p> -->

                                                <p><?php echo lang('grand_total'); ?> :
                                                   <strong> <?php echo $settings->currency; ?>
                                                        <?php echo $g = $payment->gross_total; ?> </strong>
                                                </p>
                                                <p><?php echo lang('amount_received'); ?> :
                                                  <strong>  <?php echo $settings->currency; ?>
                                                        <?php echo $r = $this->finance_model->getDepositAmountByPaymentId($payment->id); ?> </strong>
                                                </p>
                                                <p><?php echo lang('amount_to_be_paid'); ?> : 
                                                   <strong> <?php echo $settings->currency; ?>
                                                        <?php echo $g - $r; ?> </strong>
                                                </p>
                            </div>
                        </div>
                    </div>
                             </div>
                             </td>
                             </tr>
                        </tbody>
                        <tfoot><tr><td>
                    <div class="report_footer text-center">
                        <!-- <h4>
                                    <?php //echo $settings->title ?>
                                </h4> -->
                                     <p style="font-size:12px;font-family: Arial; margin:0;">
                                        <?php echo $settings->address ?> 
                                    </p>
                                    <p style="font-size:12px; margin:0;"> Tel:
                                        <?php echo $settings->phone ?>
                                    </p>
                                    <p style="font-size:12px; margin:0;"> Email:
                                        <?php echo $settings->email ?>   Web: <?php echo $settings->web ?>
                                    </p>
                            </div>
                            </td>
                            </tr>
                        </tfoot>
                         </table>
                    </div>
                        
                               
                    </page>
            </div>
        </section>
        <section class="col-md-5">
            <div class="col-md-5 no-print" style="margin-top: 20px;"> <a href="finance/payment" class="btn btn-info btn-sm info pull-left"><i class="fa fa-arrow-circle-left"></i>  <?php echo lang('back_to_payment_modules'); ?> </a> 
                <div class="text-center col-md-12 row"> <a class="btn btn-info btn-sm invoice_button pull-left" onclick="javascript:window.print();"><i class="fa fa-print"></i> <?php echo lang('print'); ?> </a> 
                    <?php if ($this->ion_auth->in_group(array('admin', 'Accountant'))) { ?> <a href="finance/editPayment?id=<?php echo $payment->id; ?>" class="btn btn-info btn-sm editbutton pull-left"><i class="fa fa-edit"></i> <?php echo lang('edit'); ?> <?php echo lang('invoice'); ?> </a> 
                        <?php } ?> <a class="btn btn-info btn-sm detailsbutton pull-left download" id="download"><i class="fa fa-download"></i> <?php echo lang('download'); ?> </a> </div>
                <div class="no-print">
                    <a href="finance/addPaymentView" class="pull-left">
                        <div class="btn-group">
                            <button id="" class="btn btn-info green btn-sm"> <i class="fa fa-plus-circle"></i>
                                <?php echo lang('add_another_payment'); ?>
                            </button>
                        </div>
                    </a>
                </div>
            </div>
        </section>
      <style media="print">
           /* page {background: #ccc;display: block;height: 100%; margin: 0 auto;margin-bottom: 0.5cm;-webkit-box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);-moz-box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);-o-box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);} */
           
            @media print {           
                p{margin: 0;font-size: 11px;font-family: 'Arial';}
                page {background: #333;margin: 0;box-shadow: 0;position: relative;}
                body {font-size: 13px;padding: 0;background: #333;background-color:#FFFFFF;margin: 0px;}
                .report_header {position: fixed;left: 0;width: 100%;width: 100%; text-align: center;top: 0;height: 150px;border-bottom:1px solid #ccc; padding: 10px;}
                .report_body {page-break-after: always;margin-top: 150px;margin-bottom: 100px;height: 100%;}
                .report_footer {position: fixed;bottom: 0;left: 0; height:80px;width: 100%;padding: 10px; text-align: center;border-top:1px solid #ccc; }
                thead {display: table-header-group;} 
                tfoot {display: table-footer-group;width: 100%;}
                .table>tbody>tr>td{border-top: none;}
                .invoice-block.text-right p {margin-bottom: 6px;}
    .invoice-block.text-right p strong {min-width: 80px;float: right;}
            }
        </style>
        <style>
    p{margin: 0;}
    table td {padding:5px 10px}
    pre {display: none;}
    .invoice-block.text-right p {margin-bottom: 6px;}
    .invoice-block.text-right p strong {min-width: 80px;float: right;}
</style>
        <script src="common/js/codearistos.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.0.272/jspdf.debug.js"></script>
        <script>
            $('#download').click(function () {
                var pdf = new jsPDF('p', 'pt', 'letter');
                pdf.addHTML($('#invoice'), function () {
                    pdf.save('invoice_id_<?php echo $payment->id; ?>.pdf');
                });
            });
            // This code is collected but useful, click below to jsfiddle link.
        </script>
    </section>
    <!-- invoice end-->
</section>
<!--main content end-->
<!--footer start-->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script>
    $(document).ready(function () {
        $(".flashmessage").delay(3000).fadeOut(100);
    });
    var printTask = printEvent.request.createPrintTask("Print Sample", function (args) {
    var src = MSApp.getHtmlPrintDocumentSource(document);
    src.rightMargin = 0;
    src.leftMargin = 0;
    src.topMargin = 0;
    src.bottomMargin = 0;
    src.shrinkToFit = false;
    args.setSource(src);
}
</script>