<!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- invoice start-->
        <section class="col-md-6">
            <div class="panel panel-primary" id="invoice">
                <!--<div class="panel-heading navyblue"> INVOICE</div>-->
                <page size="A5" layout="landscape">
                
                   
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin:0;padding:0;">
                            <tr>
                                <td align="center" valign="top"> <img alt="" src="<?php echo $this->settings_model->getSettings()->logo; ?>"> </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top">
                                    <!-- <h4>
                                    <?php //echo $settings->title ?>
                                </h4> -->
                                    <p style="font-size:12px;font-family: Arial; margin:0;">
                                        <?php echo $settings->address ?>
                                    </p>
                                    <p style="font-size:12px; margin:0;"> Tel:
                                        <?php echo $settings->phone ?>
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <p class="text-center" style="font-weight: bold;font-family: Arial;text-transform: uppercase;">
                                        <?php echo lang('test') ?>
                                            <?php echo lang('request') ?>
                                            <?php echo lang('form') ?>
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin:0;padding:0;" class="table">
                                        <tr>
                                             <td width="60%">
                                               
                                                    <p>
                                                        <span style="text-align:right;font-family: Arial;font-size:12px;" width="50%">
                                                            <?php $patient_info = $this->db->get_where('patient', array('id' => $payment->patient))->row(); ?> <span class="control-label"><?php echo lang('patient'); ?> <?php echo lang('name'); ?> </span> </span>
                                                        <span style="text-align:left;font-family: Arial;font-size:12px;" width="50%"> <span style="text-transform: uppercase;"> : 
                                            <?php
                                            if (!empty($patient_info)) {
                                                echo $patient_info->name . ' <br>';
                                            }
                                            ?>
                                        </span></span>
                                                    </p>
                                                    <p>
                                                        <span style="text-align:right;font-family: Arial;font-size:12px;" width="50%"> <span class="control-label"><?php echo lang('patient_id'); ?>  </span></span>
                                                        <span style="text-align:left;font-family: Arial;font-size:12px;" width="50%"><span style="text-transform: uppercase;"> : 
                                            <?php
                                            if (!empty($patient_info)) {
                                                echo $patient_info->patient_id . ' <br>';
                                            }
                                            ?>
                                        </span></span>
                                                    </p>
                                                    <p>
                                                        <span style="text-align:right;font-family: Arial;font-size:12px;" width="50%"><span class="control-label"> <?php echo lang('address'); ?> </span></span>
                                                        <span style="text-align:left;font-family: Arial;font-size:12px;" width="50%"><span style="text-transform: uppercase;"> : 
                                            <?php
                                            if (!empty($patient_info)) {
                                                echo $patient_info->address . ' <br>';
                                            }
                                            ?>
                                        </span></span>
                                                    </p>
                                                    <p>
                                                        <span style="text-align:right;font-family: Arial;font-size:12px;" width="50%"><span class="control-label"><?php echo lang('phone'); ?>  </span></span>
                                                        <span style="text-align:left;font-family: Arial;font-size:12px;" width="50%"> <span style="text-transform: uppercase;"> : 
                                            <?php
                                            if (!empty($patient_info)) {
                                                echo $patient_info->phone . ' <br>';
                                            }
                                            ?>
                                        </span></span>
                                                    </p>
													<p>
                                                        <span style="text-align:right;font-family: Arial;font-size:12px;" width="50%"><span class="control-label"><?php echo lang('email'); ?>  </span></span>
                                                        <span style="text-align:left;font-family: Arial;font-size:12px;" width="50%"> <span style="text-transform: uppercase;"> : 
                                            <?php
                                            if (!empty($patient_info)) {
                                                echo $patient_info->email . ' <br>';
                                            }
                                            ?>
                                        </span></span>
                                                    </p>
                                                    <p>
                                                        <span style="text-align:right;font-family: Arial;font-size:12px;" width="50%"><span class="control-label"><?php echo lang('age'); ?>  </span></span>
                                                        <span style="text-align:left;font-family: Arial;font-size:12px;" width="50%"> <span style="text-transform: uppercase;"> : 
                                            <?php
                                            if (!empty($patient_info)) {
                                                echo $patient_info->age . ' <br>';
                                            }
                                            ?>
                                        </span></span>
                                                    </p>

													<p>
                                                        <span style="text-align:right;font-family: Arial;font-size:12px;" width="50%"><span class="control-label"><?php echo lang('sex'); ?>  </span></span>
                                                        <span style="text-align:left;font-family: Arial;font-size:12px;" width="50%"> <span style="text-transform: uppercase;"> : 
                                            <?php
                                            if (!empty($patient_info)) {
                                                echo $patient_info->sex . ' <br>';
                                            }
                                            ?>
                                        </span></span>
                                                    </p>
													
													<!-- <p>
                                                        <span style="text-align:right;font-family: Arial;font-size:12px;" width="50%"><span class="control-label"><?php //echo lang('dob'); ?>  </span></span>
                                                        <span style="text-align:left;font-family: Arial;font-size:12px;" width="50%"> <span style="text-transform: uppercase;"> : 
                                            <?php
                                           // if (!empty($patient_info)) {
                                               // echo $patient_info->birthdate . ' <br>';
                                           // }
                                            ?>
                                        </span></span>
                                                    </p> -->
													
													<p>
                                                        <span style="text-align:right;font-family: Arial;font-size:12px;" width="50%"><span class="control-label"><?php echo lang('blood'). ' '. lang('group'); ?>  </span></span>
                                                        <span style="text-align:left;font-family: Arial;font-size:12px;" width="50%"> <span style="text-transform: uppercase;"> : 
                                            <?php
                                            if (!empty($patient_info)) {
                                                echo $patient_info->bloodgroup . ' <br>';
                                            }
                                            ?>
                                        </span></span>
                                                    </p>
													
													<p>
                                                        <span style="text-align:right;font-family: Arial;font-size:12px;" width="50%"><span class="control-label"><?php echo lang('patient_history'); ?>  </span></span>
                                                        <span style="text-align:left;font-family: Arial;font-size:12px;" width="50%"> <span style="text-transform: uppercase;"> : 
                                            <?php
                                            if (!empty($payment)) {
                                                echo $payment->history . ' <br>';
                                            }
                                            ?>
                                        </span></span>
                                                    </p>
													
													
                                            </td>
                                            <td width="40%">
                                               
                                                    <p>
                                                        <span style="text-align:right;font-family: Arial;font-size:12px;" width="50%"> <span class="control-label"><?php echo lang('invoice'); ?>  </span></span>
                                                        <span style="text-align:left;font-family: Arial;font-size:12px;" width="50%"><span style="text-transform: uppercase;"> : 
                                            <?php
                                            if (!empty($payment->id)) {
                                                echo $payment->id;
                                            }
                                            ?>
                                        </span></span>
                                                    </p>
                                                    <p>
                                                        <span style="text-align:right;font-family: Arial;font-size:12px;" width="50%"><span class="control-label"><?php echo lang('date'); ?>  </span></span>
                                                        <span style="text-align:left;font-family: Arial;font-size:12px;" width="50%"><span style="text-transform: uppercase;"> : 
                                                            <?php
                                            if (!empty($payment->date)) {
                                                echo date('d-m-Y', $payment->date) . ' ';
                                            }
                                            ?>
                                               <?php
                                            if (!empty($payment->entry_time)) {
                                                echo ($payment->entry_time) . ' <br>';
                                            }
                                            ?>
                                        </span></span>
                                                    </p>
                                                    <p>
                                                        <span style="text-align:right;font-family: Arial;font-size:12px;" width="50%"><span class="control-label"><?php echo lang('doctor'); ?>  </span></span>
                                                        <span style="text-align:left;font-family: Arial;font-size:12px;" width="50%"> <span style="text-transform: uppercase;"> : 
                                            <?php
                                            if (!empty($payment->doctor)) {
                                                $doc_details = $this->doctor_model->getDoctorById($payment->doctor);
                                                if (!empty($doc_details)) {
                                                    echo $doc_details->name . ' <br>';
                                                } else {
                                                    echo $payment->doctor_name . ' <br>';
                                                }
                                            }
                                            ?>
                                        </span></span>
                                                    </p>
                                               
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                   
                   <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin:0;padding:10px;" class="table table-striped table-hover">
                        <thead class="theadd">
                            <tr>
                                <th>#</th>
                                <th>
                                    <?php echo lang('product_code'); ?>
                                </th>
                                <th>
									<?php echo lang('product_details'); ?>
                                </th>
                                
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if (!empty($payment->category_name)) {
                                $category_name = $payment->category_name;
                                $category_name1 = explode(',', $category_name);
                                $i = 0;
                                foreach ($category_name1 as $category_name2) {
                                    $i = $i + 1;
                                    $category_name3 = explode('*', $category_name2);
                                    if ($category_name3[3] > 0) {
                                        ?>
                                <tr>
                                    <td>
                                        <?php echo $i; ?>
                                    </td>
                                    <td>
                                        <?php echo $this->finance_model->getPaymentcategoryById($category_name3[0])->test_code; ?>
                                    </td>
                                    <td class="">
                                        <?php echo $this->finance_model->getPaymentcategoryById($category_name3[0])->category; ?>
                                    </td>
                                    <td class="">
                                        <?php //echo $category_name3[3]; ?>
                                    </td>
                                    <td style="text-align:right">
                                        <?php //echo $settings->currency; ?>
                                            <?php //echo $category_name3[1] * $category_name3[3]; ?>
                                    </td>
                                </tr>
                                <?php
                                    }
                                }
                            }
                            ?>
                        </tbody>
							
						<tbody>
						<thead class="theadd">
                            <tr>
                                <th>#</th>
                                <th>
                                    <?php echo lang('specimen_detail'); ?>
                                </th>
                                                                
                            </tr>
                        </thead>
						
						<?php
                            if (!empty($payment->category_name)) {
                                $category_name = $payment->category_name;
                                $category_name1 = explode(',', $category_name);
                                $i = 0;
                                foreach ($category_name1 as $category_name2) {
                                    $i = $i + 1;
                                    $category_name3 = explode('*', $category_name2);
                                    if ($category_name3[3] > 0) {
                                        ?>
                                <tr>
                                    <td>
                                        <?php echo $i; ?>
                                    </td>
                                    <td>
                                        <?php echo $this->finance_model->getPaymentcategoryById($category_name3[0])->specimen; ?>
                                    </td>
                                    
                                    <td class="">
                                        <?php //echo $category_name3[3]; ?>
                                    </td>
                                    <td style="text-align:right">
                                        <?php //echo $settings->currency; ?>
                                            <?php //echo $category_name3[1] * $category_name3[3]; ?>
                                    </td>
                                </tr>
                                <?php
                                    }
                                }
                            }
                            ?>
							
							
						</tbody>
                    </table>
                    <div class="">
                        <div class="invoice-block text-right" style="padding:10px;">
                            
                        </div>
                    </div>
                   
                    </page>
            </div>
        </section>
        <section class="col-md-6">
            <div class="col-md-5 no-print" style="margin-top: 20px;"> <a href="finance/payment" class="btn btn-info btn-sm info pull-left"><i class="fa fa-arrow-circle-left"></i>  <?php echo lang('back_to_payment_modules'); ?> </a> 
                <div class="text-center col-md-12 row"> <a class="btn btn-info btn-sm invoice_button pull-left" onclick="javascript:window.print();"><i class="fa fa-print"></i> <?php echo lang('print'); ?> </a> 
                    <?php if ($this->ion_auth->in_group(array('admin', 'Accountant'))) { ?> 
                        <?php } ?> <a class="btn btn-info btn-sm detailsbutton pull-left download" id="download"><i class="fa fa-download"></i> <?php echo lang('download'); ?> </a> </div>
                <div class="no-print">
                    
                </div>
            </div>
        </section>
        
        <style media="print">
           page {background: #ccc;display: block;margin: 0 auto;margin-bottom: 0.5cm;-webkit-box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);-moz-box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);-o-box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);}
           
            @media print {
           
                p{margin: 0;font-size: 11px;font-family: 'Arial';}
              page {background: #333;
                margin: 0;
                box-shadow: 0;
              }
                body {font-size: 13px;padding: 0;background: #333;
        background-color:#FFFFFF; 
        
        margin: 0px;  /* this affects the margin on the content before sending to printer */
   }
            }
           
        </style>
        <script src="common/js/codearistos.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.0.272/jspdf.debug.js"></script>
        <script>
            $('#download').click(function () {
                var pdf = new jsPDF('p', 'pt', 'letter');
                pdf.addHTML($('#invoice'), function () {
                    pdf.save('invoice_id_<?php echo $payment->id; ?>.pdf');
                });
            });
            // This code is collected but useful, click below to jsfiddle link.
        </script>
    </section>
    <!-- invoice end-->
</section>
<!--main content end-->
<!--footer start-->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script>
    $(document).ready(function () {
        $(".flashmessage").delay(3000).fadeOut(100);
    });
    var printTask = printEvent.request.createPrintTask("Print Sample", function (args) {
    var src = MSApp.getHtmlPrintDocumentSource(document);
    src.rightMargin = 0;
    src.leftMargin = 0;
    src.topMargin = 0;
    src.bottomMargin = 0;
    src.shrinkToFit = false;
    args.setSource(src);
}
</script>