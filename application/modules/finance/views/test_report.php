<!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- invoice start-->
        <section class="col-md-4">
            <div class="panel panel-primary" id="invoice">
                <!--<div class="panel-heading navyblue"> INVOICE</div>-->
                <page size="A4" layout="portrait"> 
                     <div class="panel-body">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <thead>
                                <tr>
                                    <td>
                                        <div class="report_header text-center">
                                                <img alt="" src="<?php echo $this->settings_model->getSettings()->logo; ?>" width="285" height="100">

                                                <h4 style="font-weight: bold; margin-top: 20px; text-transform: uppercase;margin:5px 0;">
                                                <?php echo lang('test_report') ?>
                                                
                                                
                                            </h4>
                                        </div>
                          </td></tr>  </thead>
                         <tbody><tr><td>
                        <div class="report_body">
                             <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin:0;padding:0;">
                           
                            <tr>
                                <td>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin:0;padding:0;" class="table">
                                        <tr>
                                             <td width="60%">
                                               
                                                    <p>
                                                        <span style="text-align:right;font-family: Arial;font-size:12px;" width="50%">
                                                         <?php $patient_info = $this->db->get_where('patient', array('id' => $payment->patient))->row(); ?> <span class="control-label"><?php echo lang('patient'); ?> <?php echo lang('name'); ?> </span> </span>
                                                        <span style="text-align:left;font-family: Arial;font-size:12px;" width="50%"> <span style="text-transform: uppercase;"> : 
                                            <?php
                                            if (!empty($patient_info)) {
                                                echo $patient_info->name . ' <br>';
                                            }
                                            ?>
                                        </span></span>
                                                    </p>
                                                    <p>
                                                        <span style="text-align:right;font-family: Arial;font-size:12px;" width="50%"> <span class="control-label"><?php echo lang('patient_id'); ?>  </span></span>
                                                        <span style="text-align:left;font-family: Arial;font-size:12px;" width="50%"><span style="text-transform: uppercase;" > : 
                                            <?php
                                            if (!empty($patient_info)) {
                                                echo $patient_info->id;
                                            }
                                            ?>
                                        </span></span>
                                                    </p>
                                                    <p>
                                                        <span style="text-align:right;font-family: Arial;font-size:12px;" width="50%"><span class="control-label"> <?php echo lang('address'); ?> </span></span>
                                                        <span style="text-align:left;font-family: Arial;font-size:12px;" width="50%"><span style="text-transform: uppercase;"> : 
                                            <?php
                                            if (!empty($patient_info)) {
                                                echo $patient_info->address . ' <br>';
                                            }
                                            ?>
                                        </span></span>
                                                    </p>
                                                    <p>
                                                        <span style="text-align:right;font-family: Arial;font-size:12px;" width="50%"><span class="control-label"><?php echo lang('phone'); ?>  </span></span>
                                                        <span style="text-align:left;font-family: Arial;font-size:12px;" width="50%"> <span style="text-transform: uppercase;"> : 
                                            <?php
                                            if (!empty($patient_info)) {
                                                echo $patient_info->phone . ' <br>';
                                            }
                                            ?>
                                        </span></span>
                                                    </p>
                                            </td>
                                            <td width="40%">

                                                  <p>
                                                        <span style="text-align:right;font-family: Arial;font-size:12px;" width="50%"> <span class="control-label"><?php echo lang('age'); ?>  </span></span>
                                                        <span style="text-align:left;font-family: Arial;font-size:12px;" width="50%"><span style="text-transform: uppercase;"> : 
                                            <?php
                                            if (!empty($patient_info->id)) {
                                                echo $patient_info->age;
                                            }
                                            ?>
                                        </span></span>
                                                    </p>
                                               
                                                    <p>
                                                        <span id="invoice_id" style="text-align:right;font-family: Arial;font-size:12px;" width="50%"> <span class="control-label"><?php echo lang('invoice'); ?>  </span></span>
                                                        <span style="text-align:left;font-family: Arial;font-size:12px;" width="50%"><span style="text-transform: uppercase;" id="payment_id">  
                                            <?php
                                            if (!empty($payment->id)) {
                                                echo $payment->id;
                                            }
                                            ?>
                                        </span></span>
                                                    </p>
                                                    <p>
                                                        <span style="text-align:right;font-family: Arial;font-size:12px;" width="50%"><span class="control-label"><?php echo lang('date'); ?>  </span></span>
                                                        <span style="text-align:left;font-family: Arial;font-size:12px;" width="50%"><span style="text-transform: uppercase;"> : 
                                            <?php
                                            if (!empty($payment->date)) {
                                                echo date('d-m-Y', $payment->date) . ' ';
                                            }
                                            ?>
                                            <?php
                                            if (!empty($payment->entry_time)) {
                                                echo ($payment->entry_time) . '<br> ';
                                            }
                                            ?>
                                        </span></span>
                                                    </p>
                                                    <p>
                                                        <span style="text-align:right;font-family: Arial;font-size:12px;" width="50%"><span class="control-label"><?php echo lang('doctor'); ?>  </span></span>
                                                        <span style="text-align:left;font-family: Arial;font-size:12px;" width="50%"> <span style="text-transform: uppercase;"> : 
                                            <?php
                                            if (!empty($payment->doctor)) {
                                                $doc_details = $this->doctor_model->getDoctorById($payment->doctor);
                                                if (!empty($doc_details)) {
                                                    echo $doc_details->name . ' <br>';
                                                } else {
                                                    echo $payment->doctor_name . ' <br>';
                                                }
                                            }
                                            ?>
                                        </span></span>
                                                    </p>
                                               
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
  
                   <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin:0;padding:10px;" class="table table-striped table-hover">
                        <thead class="theadd">
                            <tr>
                                <th>#</th>
                                <th>
                                    <?php echo lang('description'); ?>
                                </th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if (!empty($payment->category_name)) {

                                $category_name = $payment->category_name;
                                
                                $category_name1 = explode(',', $category_name);
                                $i = 0;
                                foreach ($category_name1 as $category_name2) {
                                    $i = $i + 1;
                                    $category_name3 = explode('*', $category_name2);
                                    if ($category_name3[3] > 0) {
                                        ?>
                                <tr>
                                    <td>
                                        <?php echo $i; ?>
                                    </td>
                                    <td>
                                        <?php echo $this->finance_model->getPaymentcategoryById($category_name3[0])->category; 
                                        ?>
                                    </td>
                                    <td>
                               
                                        <button type="button"  class="btn btn-info pull-right" onclick="test_report_function(<?php echo $category_name3[0]?>,<?php echo $payment->id; ?>)"  ><?php echo lang('add_result'); ?></button>
                            
                                    </td>
                                  
                                </tr>
                                <?php
                                    }
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                   
                                    </div>
                                    </td>
                                    </tr>
                                </tbody>
                
                                </table>
                                <div class="col-md-1 lab pad_bot" style="padding-top: 25px; margin-left: 1px; margin-top: -25px;">
                                    <button type="button" id="generate_report" class="btn color-style "> Generate Report </button>
                                </div> 
                            </div>
                            
                                    
                            </page>
                          
                    </div>
                </section>
                <section class="col-md-8">
                <button type="button" id="updateButton" class="btn btn-primary" onclick="window.location.href='home'" style="float:right;margin:0;"> Back To Home</button>

            <form id="report_dnld_form" action="lab/generate_report_submit_pdf" method="post" class="row">
                    <!-- <form role="form" id="editTest" class="clearfix" action="" method="post" enctype="multipart/form-data"> -->
                    <div id=cat_hide >
                    
                    <input type="hidden" id="hid_invoice_id" name="invoice_id">
                    <input type="hidden" id="hid_doc_id" name="doc_id[]">
                    <input type="hidden" name="category_id" id="category_id">
                    <input type="hidden" name="patient_id" id="patient_id" value="<?php if (!empty($patient_info)) {
                                                        echo $patient_info->id;
                                                    } ?>">


                            <div class="col-md-10 lab pad_bot" style="margin-left: 16px;" >
                            <label for="parameter">Test Name</label>
                            <input type="text" id="catagory_name" readonly="" name="catagory_name[]" class="form-control pay_in" value="">
                            <label for="comments">Comments</label>
                            <textarea class="form-control pay_in" name="comments" rows="5" cols="5"  id="comments" placeholder="" style="height:70px!important;"></textarea>
                            <label for="interpretation">Interpretation</label>
                            <textarea class="form-control pay_in" name="interpretation" rows="5" cols="5"  id="interpretation" placeholder="" style="height:70px!important;"></textarea>
                            <!-- <label for="interpretation1">Interpretation1</label>
                            <textarea class="form-control pay_in" name="interpretation1" rows="5" cols="5"  id="interpretation1" placeholder="" style="height:70px!important;"></textarea> -->
                            </div>

                            <div id="param_head">
                            	<div class="col-md-3 lab pad_bot" style="margin-left: 16px;">
                            		<label for="parameter">Parameter Name</label>
                            	</div>
                            	<div class="col-md-2 lab pad_bot">
                            		<label for="unit"> Unit </label>
                            	</div>
                            	<div class="col-md-2 lab pad_bot">
                            		<label for="ref_range">Ref. Range</label>
                            	</div>
                            	<div class="col-md-2 lab pad_bot">
                            		<label for="accurate_value">Result</label>
                            	</div>
                                <div class="col-md-2 lab pad_bot">
                            		<label for="accurate_value">Bold</label>
                            	</div>
                            </div>
                                                        
                        
                            <div  id="chk_html" style="margin-left: 16px; margin-top: 90px;">
                            	
                            </div>

                          
                    
                      </div>  
                        
                      <div class="col-md-12 lab pad_bot" id="doc_hide">
                        <br><br><br><br><br>
                            <label for="exampleInputEmail1">Select Pathologist <?php echo lang('doctor'); ?></label>
                                <select class="form-control m-bot15 js-example-basic-single add_doctor" id="add_doctor" name="doctor[]" value='' multiple="multiple" title="Select Pathology Doctor..">  
                                            <!-- <option value="add_new" style="color: #41cac0 !important;"><?php //echo lang('add_new'); ?></option>
                                            <?php //foreach ($doctors as $doctor) { ?>
                                                <option value="<?php //echo $doctor->id; ?>"<?php
                                                //if (!empty($payment->doctor)) {
                                                    //if ($payment->doctor == $doctor->id) {
                                                        //echo 'selected';
                                                    //}
                                               // }
                                                ?>><?php //echo $doctor->name; ?> </option>
                                                    <?php //} ?> -->
                                </select>
                                <!-- <input type="text" id="hid_category_id" name="hid_category_id"> -->
                                <input type="hidden" id="hid_array_doc_id" name="hid_array_doc_id">

                     </div>
                 
                    
                     <div class="row" id="add_hide" class="rowclass" style="margin-left: 550px;">
                        <div class="col-md-1 lab pad_bot" style="padding-top: 25px;">
                            <button type="submit" id="updateButton" class="btn btn-info"> Save </button>
                        </div>
                     </div>

                    <div id="invoice_add">
                        <div class="col-md-1 lab pad_bot" style="padding-top: 25px;">
                            <button id="invoiceButton" type="button" class="btn btn-info"> Save & Proceed </button>
                        </div>
                    </div>
                            

            </form>
                 
                            <!-- <form id="report_dnld_form" action="lab/generate_report_submit_pdf" method="post">
                            <input type="hidden" id="hid_invoice_id" name="invoice_id">
                            <input type="hidden" id="hid_doc_id" name="doc_id">
                              </form> -->
                            
        </section>

      <style media="print">
           page {background: #ccc;display: block;height: 100%; margin: 0 auto;margin-bottom: 0.5cm;-webkit-box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);-moz-box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);-o-box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);}
           
            @media print {           
                p{margin: 0;font-size: 11px;font-family: 'Arial';}
                page {background: #333;margin: 0;box-shadow: 0;position: relative;}
                body {font-size: 13px;padding: 0;background: #333;background-color:#FFFFFF;margin: 0px;}
                .report_header {position: fixed;left: 0;width: 100%;width: 100%; text-align: center;top: 0;height: 150px;border-bottom:1px solid #ccc; padding: 10px;}
                .report_body {page-break-after: always;margin-top: 150px;margin-bottom: 100px;height: 100%;}
                .report_footer {position: fixed;bottom: 0;left: 0; height:80px;width: 100%;padding: 10px; text-align: center;border-top:1px solid #ccc; }
                thead {display: table-header-group;} 
                tfoot {display: table-footer-group;width: 100%;}
                .table>tbody>tr>td{border-top: none;}
                .invoice-block.text-right p {margin-bottom: 6px;}
    .invoice-block.text-right p strong {min-width: 80px;float: right;}
            }
    </style>
        <style>
    p{margin: 0;}
    table td {padding:5px 10px}
    pre {display: none;}
    .invoice-block.text-right p {margin-bottom: 6px;}
    .invoice-block.text-right p strong {min-width: 80px;float: right;}
    .active{margin-left:300px;}
    .color-style{background-color:orange;}
</style>
        <script src="common/js/codearistos.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.0.272/jspdf.debug.js"></script>
        <script>
            $('#download').click(function () {
                var pdf = new jsPDF('p', 'pt', 'letter');
                pdf.addHTML($('#invoice'), function () {
                    pdf.save('invoice_id_<?php echo $payment->id; ?>.pdf');
                });
            });
         



            // This code is collected but useful, click below to jsfiddle link.
        </script>
    </section>
    <!-- invoice end-->
</section>
<!--main content end-->
<!--footer start-->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        
    });
</script>


<script>
    $(document).ready(function () {
        $(".flashmessage").delay(3000).fadeOut(100);
        $("#add_hide").hide();
        $("#cat_hide").hide();
        $("#doc_hide").hide();
        //$("#add_hide").addClass("active");
        $("#invoice_add").hide();
        
        //$("#param_head").hide();


    });

    // function generate_report_function(cat_id)
    // {
    //     var cat_id=cat_id;
    //     alert(cat_id);

       
    //     $("#add_hide").hide();
    //     $("#cat_hide").hide();
    //     $("#doc_hide").show();
    //     $("#invoice_add").show();
    //     $("#hid_category_id").val(cat_id);


    //     $.ajax({
    //                 type:'POST',
    //                 url:"lab/test_report",
    //                 data:{action_type:'generate_report',cat_id:cat_id},
    //                 dataType: "json",
    //                 encode: true
    //             })
    //             .done(function(data)
    //             {
                   
    //                           //var rslt='<option value="">Select Pathology Doctor....</option>';
    //                           var rslt='';

    //                             $.each(data.path_doctor_list,function(key,value){
                                     
    //                                 // $("#doc_hide").show();
    //                                  rslt +='<option value="'+value.id+'">'+value.patho_name+'</option>';
    //                                      })
    //                               $("#add_doctor").html(rslt);
                                  


    //                      $("#add_doctor").selectpicker('refresh');

    //             });



    // }

   function test_report_function(aaaaaa,bbbbb) 
   {
    //alert(aaaaaa);
    var v_d_id=aaaaaa;
    var inv_d_id=bbbbb;
        //alert(inv_d_id);
    
 

        $.ajax({
                    type:'POST',
                    url:"lab/test_report",
                    data: {v_id:v_d_id,inv_d_id:inv_d_id},
                    dataType: "json",
                    encode: true
                })
                .done(function(data){
                    var result='';
                    var count = 0;
                    

                    
                    //alert(data.category_report_list);
                    console.log(data.category_report_list);
                      if(data.status){//alert(data.category_report_list.category);
			             $.each(data.category_report_list,function(key,value){//alert(value.test_id);
                            // var category = value.category;
                            // alert(category);
                             count++;
                            
                             //result += '<div >'+count+'</div>';
                             result += '<div id="" class="row">';
                             //result +='<input type="text" name="inv_id[]" value="'+value.id+'">';
                             result +='<input type="hidden" name="test_id[]" value="'+value.test_id+'">';
                             result +='<div class="col-md-3 lab pad_bot" style="margin-left: 16px;">';
                             //result +='<label for="parameter">Parameter Name</label>';
                             //result +='<input type="text" id="parameter" name="parameter[]" class="form-control pay_in" value="'+value.parameter_name+'">';
                             result +='<textarea name="parameter[]" id="parameter_'+count+'" class="ckeditor-basic form-control pay_in " rows="10">'+value.parameter_name+'</textarea>'

                             result +='</div>';
                             result +='<div class="col-md-2 lab pad_bot">';
                             //result +='<label for="unit"> Unit </label>';
                            // result +='<input type="text" value="'+value.test_unit+'" id="unit" name="unit[]" class="form-control pay_in">';
                             result +='<textarea name="unit[]" id="unit_'+count+'" class="ckeditor-basic form-control pay_in " rows="10">'+value.test_unit+'</textarea>'
                             result +='</div>';
                             result +='<div class="col-md-2 lab pad_bot">';
                             //result +='<label for="ref_range">Ref. Range</label>';
                             //result +='<input type="text" value="'+value.reference_range+'" name="ref_range[]" id="ref_range" class="form-control pay_in">';
                             result +='<textarea name="ref_range[]" id="ref_range_'+count+'" class="ckeditor-basic form-control pay_in " rows="10">'+value.reference_range+'</textarea>'
                             result +='</div>';
                             result +='<div class="col-md-2 lab pad_bot">';
                             //result +='<label for="accurate_value">Accurate&nbsp;&nbsp;Value</label>';
                            // result +='<input type="text" id="accurate_value" name="accurate_value[]" value="'+value.actual_value+'" class="form-control pay_in">';
                             result +='<textarea name="accurate_value[]" id="accurate_value_'+count+'" class="ckeditor-basic form-control pay_in " rows="10" >'+value.actual_value+'</textarea>'
                             result +='</div>';
                             result +='<div class="col-md-2 lab pad_bot">';
                             if(value.actual_value_bold == 1){
                                result +='<label for="checkbox_'+count+'"> <input type="checkbox" id="checkbox_'+count+'" name="checkbox[]" checked value="1" onclick="checkbox_fun('+count+');"class="">';

                             }else{
                             result +='<label for="checkbox_'+count+'"> <input type="checkbox" id="checkbox_'+count+'" name="checkbox[]" value="1" onclick="checkbox_fun('+count+');"class="">';
                             }
                             result +='</div>';
                             result +='<input type="hidden" id="checkbox_bold_'+count+'" name="checkbox_bold[]" value="0">';
                             result +='</div>';
                             // result +='<div class="row">';
                             // result +='<div class="col-md-1 lab pad_bot" style="padding-top: 25px;">';
                             // result +='<button type="button" id="updateButton" class="addrow"> Add </button>';
                             // result +='</div>';
                             // result +='</div>';
                             $("#catagory_name").val(value.category);
                             $("#comments").val(value.comments);
                             $("#interpretation").val(value.interpretation);
                            //  $("#interpretation1").val(value.interpretation1);



                                    })
                                    //alert(v_d_id);
                                 $("#category_id").val(v_d_id);
                                 $("#hid_invoice_id").val(inv_d_id);
                                
                                }

                                $("#chk_html").html(result);

                                $('.ckeditor-basic').each(function(e){
                                    CKEDITOR.replace(this.id, {
                                        width: 120,
                                        height: 50,
                                        toolbar: [
                                            { name: 'document', items: [ 'Source', '-', 'NewPage', 'Preview', '-', 'Templates' ] },	// Defines toolbar group with name (used to create voice label) and items in 3 subgroups.
                                            [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ],			// Defines toolbar group without name.
                                            '/',																					// Line break - next group will be placed in new line.
                                            { name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', 'Superscript', 'Subscript' ] },
                                            { name: 'Featured Formula', type: 'widget', widget: 'mathjax', attributes: { 'class': 'math-featured' } }
                                        ]
                                    });
                                });

                                //$("#param_head").show();
                                $("#add_hide").show();
                              $("#cat_hide").show();
                              $("#doc_hide").hide();
                              $("#invoice_add").hide();
                             
                          

                            });


   }


            var printTask = printEvent.request.createPrintTask("Print Sample", function (args) {
            var src = MSApp.getHtmlPrintDocumentSource(document);
            src.rightMargin = 0;
            src.leftMargin = 0;
            src.topMargin = 0;
            src.bottomMargin = 0;
            src.shrinkToFit = false;
            args.setSource(src);
        });
</script>

<script>
                check_box_val='';
            function checkbox_fun(count){

              if($("#checkbox_"+count).prop("checked") == true){
             var check_box_val=$("#checkbox_"+count).val();
                 $("#checkbox_bold_"+count).val(check_box_val);
                  
                    }
                    else{

                    //selected_fabric = removeArrayElement(selected_fabric,$("#fabric_id_"+fabric_id).val());
                 $("#checkbox_bold_"+count).val("0");
                    
                    }



            }

            $('#report_dnld_form').submit(function(e){
                e.preventDefault();

                    for ( instance in CKEDITOR.instances )
                    CKEDITOR.instances[instance].updateElement();

    
                var formData = $('#report_dnld_form').serializeArray();
                //alert(formData);


                console.log(formData);

                $.ajax({
                    type: 'POST',
                    url: "finance/test_report_accurate_value",
                    data: formData,
                    dataType: "json",
                    encode: true
                })
                .done(function(response)
                {
                    if (response == 1) {

                        $("#show_message").html('Successfully Inserted !');
                        setTimeout(function () {
                            window.location.reload();
                        }, 2000);
                        //alert('Success');
                    }
                    else{
                        alert('Sorry');
                    }
                })
                .fail(function(response){
                    console.log(response);
                });

            });




            $("#generate_report").click(function()
            {
                 var invoice_id = $("#payment_id").text();
                 var doc_id = $("#add_doctor").val();
                 //alert(invoice_id);
                 var category_id = $("#hid_category_id").val();
                 //alert(category_id);
                 $("#hid_invoice_id").val(invoice_id);
                 $("#hid_doc_id").val(doc_id);
                 //$("#hid_category_id").val(category_id);
                 $("#report_dnld_form").submit();
            })
            


</script>



