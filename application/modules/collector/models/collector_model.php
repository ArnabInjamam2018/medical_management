<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Collector_model extends CI_model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    // function insertDoctor($data) {
    //     $this->db->insert('doctor', $data);
    // }

    function getCollector() {
       
        $query = $this->db->get('collection_center_master');
        return $query->result();
    }

     function getCollectorBysearch($search) {
        $this->db->like('collection_id', $search);
         $this->db->or_like('collector_name', $search);
        $this->db->or_like('collector_ph', $search);
         $this->db->or_like('collector_address', $search);
         $this->db->or_like('contact_person', $search);
         $this->db->or_like('commission_percentage', $search);
         $query = $this->db->get('collection_center_master');
        return $query->result();
     }

     function getCollectorByLimit($limit, $start) {
        $this->db->order_by('collection_id', 'desc');
        $this->db->limit($limit, $start);
       $query = $this->db->get('collection_center_master');
       return $query->result();
     }

    function getCollectorByLimitBySearch($limit, $start, $search) {

        $this->db->like('collection_id', $search);

        $this->db->order_by('collection_id', 'desc');

        $this->db->or_like('collector_name', $search);
        $this->db->or_like('collector_ph', $search);
        $this->db->or_like('collector_address', $search);
        $this->db->or_like('contact_person', $search);
        $this->db->or_like('commission_percentage', $search);

        $this->db->limit($limit, $start);
        $query = $this->db->get('collection_center_master');
        return $query->result();
    }

    // // function getDoctorById($id) {
    // //     $this->db->where('id', $id);
    // //     $query = $this->db->get('doctor');
    // //     return $query->row();
    // // }
    function getcollectorById($id) {
        $this->db->where('collection_id', $id);
        $query = $this->db->get('collection_center_master');
        return $query->row();
    }

    function insertCollector($data) {
        $this->db->insert('collection_center_master', $data);
    }

    
    function updateCollector($id, $data) {
        $this->db->where('collection_id', $id);
        $this->db->update('collection_center_master', $data);
    }
    
    // function getDoctorByIonUserId($id) {
    //     $this->db->where('ion_user_id', $id);
    //     $query = $this->db->get('doctor');
    //     return $query->row();
    // }

    // function updateDoctor($id, $data) {
    //     $this->db->where('id', $id);
    //     $this->db->update('doctor', $data);
    // }

    function delete($id) {
        $this->db->where('collection_id', $id);
        $this->db->delete('collection_center_master');
    }

    // function updateIonUser($username, $email, $password, $ion_user_id) {
    //     $uptade_ion_user = array(
    //         'username' => $username,
    //         'email' => $email,
    //         'password' => $password
    //     );
    //     $this->db->where('id', $ion_user_id);
    //     $this->db->update('users', $uptade_ion_user);
    // }

}
