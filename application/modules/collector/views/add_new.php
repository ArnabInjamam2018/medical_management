<!--sidebar end-->
<!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <section class="col-md-7 row">
            <header class="panel-heading">
                <?php
                if (!empty($doctor->id))
                    echo lang('edit_collector');
                else
                    echo lang('add_collector');
                ?>
            </header> 
            <div class="panel-body">
                <div class="adv-table editable-table ">
                    <div class="clearfix">
                        <div class="col-lg-12">
                            <div class="col-lg-3"></div>
                            <div class="col-lg-6">
                                <?php echo validation_errors(); ?>
                                <?php echo $this->session->flashdata('feedback'); ?>
                            </div>
                            <div class="col-lg-3"></div>
                        </div>
                        <form role="form" action="collector/addNew" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="name"><?php echo lang('collector_name'); ?></label>
                                <input type="text" class="form-control" name="name" id="name" value='<?php
                                if (!empty($setval)) {
                                    echo set_value('name');
                                }
                                if (!empty($collector->collector_name)) {
                                    echo $collector->collector_name;
                                }
                                ?>' placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="phone"><?php echo lang('collector_phone'); ?></label>
                                <input type="text" class="form-control" name="phone" id="phone" value='<?php
                                if (!empty($setval)) {
                                    echo set_value('phone');
                                }
                                if (!empty($collector->collector_ph)) {
                                    echo $collector->collector_ph;
                                }
                                ?>' placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="collector_address"><?php echo lang('collector_address'); ?></label>
                                <input type="text" class="form-control" name="collector_address" id="collector_address" value='<?php
                                if (!empty($setval)) {
                                    echo set_value('collector_address');
                                }
                                if (!empty($collector->collector_address)) {
                                    echo $collector->collector_address;
                                }
                                ?>' placeholder="">
                            </div>

                            <div class="form-group">
                                <label for="contact_person"><?php echo lang('contact_person'); ?></label>
                                <input type="text" class="form-control" name="contact_person" id="contact_person" value='<?php
                                if (!empty($setval)) {
                                    echo set_value('contact_person');
                                }
                                if (!empty($collector->contact_person)) {
                                    echo $collector->contact_person;
                                }
                                ?>' placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="commission_percentage"><?php echo lang('commission_percentage'); ?></label>
                                <input type="text" class="form-control" name="commission_percentage" id="commission_percentage" value='<?php
                                if (!empty($setval)) {
                                    echo set_value('commission_percentage');
                                }
                                if (!empty($collector->commission_percentage)) {
                                    echo $collector->commission_percentage;
                                }
                                ?>' placeholder="">
                            </div>
                            <input type="hidden" name="id" value='<?php
                            if (!empty($collector->collection_id)) {
                                echo $collector->collection_id;
                            }
                            ?>'>
                            <button type="submit" name="submit" class="btn btn-info"><?php echo lang('submit'); ?></button>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
<!--footer start-->
