<!--sidebar end-->
<!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <section class="panel">
            <header class="panel-heading">
                <?php echo lang('collector'); ?>    
                <div class="col-md-4 no-print pull-right"> 
                    <a data-toggle="modal" href="#myModal">
                        <div class="btn-group pull-right">
                            <button id="" class="btn green btn-xs">
                                <i class="fa fa-plus-circle"></i> <?php echo lang('add_new'); ?>
                            </button>
                        </div>
                    </a>
                </div>
            </header>
            <div class="panel-body">
                <div class="adv-table editable-table ">
                    <div class="space15"></div>
                    <table class="table table-striped table-hover table-bordered" id="editable-sample">
                        <thead>
                            <tr>
                                <th><?php echo lang('id'); ?></th>
                                <th><?php echo lang('collector_name'); ?></th>
                                <th><?php echo lang('collector_phone'); ?></th>
                                <th><?php echo lang('collector_address'); ?></th>
                                <th><?php echo lang('contact_person'); ?></th>
                                <th><?php echo lang('commission_percentage'); ?></th>
                             <th class="no-print"><?php echo lang('options'); ?></th> 
                            </tr>
                        </thead>
                        <tbody>

                        <style>

                            .img_url{
                                height:20px;
                                width:20px;
                                background-size: contain; 
                                max-height:20px;
                                border-radius: 100px;
                            }

                        </style>



                        </tbody>
                    </table>
                </div>
            </div>
        </section>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
<!--footer start-->






<!-- Add Accountant Modal-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">  <?php echo lang('add_new_collector'); ?></h4>
            </div>
            <div class="modal-body row">
                <form role="form" action="collector/addNew" class="clearfix" method="post" enctype="multipart/form-data">
                    <div class="form-group col-md-6">
                        <label for="name"><?php echo lang('collector_name'); ?></label>
                        <input type="text" class="form-control" name="name" id="name" value='' placeholder="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="phone"><?php echo lang('collector_phone'); ?></label>
                        <input type="text" class="form-control" name="phone" id="phone" value='' placeholder="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="collector_address"><?php echo lang('collector_address'); ?></label>
                        <input type="text" class="form-control" name="collector_address" id="collector_address" placeholder="">
                    </div>

                    <div class="form-group col-md-6">
                        <label for="contact_person"><?php echo lang('contact_person'); ?></label>
                        <input type="text" class="form-control" name="contact_person" id="contact_person" value='' placeholder="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="commission_percentage"><?php echo lang('commission_percentage'); ?></label>
                        <input type="text" class="form-control" name="commission_percentage" id="commission_percentage" value='' placeholder="">
                    </div>
                    
                    <div class="form-group col-md-12">
                        <button type="submit" name="submit" class="btn btn-info pull-right"><?php echo lang('submit'); ?></button>
                    </div>

                </form>

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!-- Add Accountant Modal-->







<!-- Edit Event Modal-->
<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title"> <?php echo lang('edit_collector'); ?></h4>
            </div>
            <div class="modal-body">
                <form role="form" id="editCollectorForm" class="clearfix" action="collector/addNew" method="post" enctype="multipart/form-data">
                    <div class="form-group col-md-6">
                        <label for="name"><?php echo lang('collector_name'); ?></label>
                        <input type="text" class="form-control" name="name" id="name" value='' placeholder="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="phone"><?php echo lang('collector_phone'); ?></label>
                        <input type="text" class="form-control" name="phone" id="phone" value='' placeholder="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1"><?php echo lang('collector_address'); ?></label>
                        <input type="text" class="form-control" name="collector_address" id="collector_address" placeholder="">
                    </div>

                    <div class="form-group col-md-6">
                        <label for="contact_person"><?php echo lang('contact_person'); ?></label>
                        <input type="text" class="form-control" name="contact_person" id="contact_person" value='' placeholder="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="commission_percentage"><?php echo lang('commission_percentage'); ?></label>
                        <input type="text" class="form-control" name="commission_percentage" id="commission_percentage" value='' placeholder="">
                    </div>
   

                    <input type="hidden" name="id" value=''>
                    <div class="form-group col-md-12">
                        <button type="submit" name="submit" class="btn btn-info pull-right"><?php echo lang('submit'); ?></button>
                    </div>
                </form>

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!-- Edit Event Modal-->



<!-- <div class="modal fade" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title"> <?php //echo lang('doctor'); ?> <?php //echo lang('info'); ?></h4>
            </div>
            <div class="modal-body">
                <form role="form" id="editDoctorForm" class="clearfix" action="doctor/addNew" method="post" enctype="multipart/form-data">

                    <div class="form-group last col-md-6">
                        <div class="">
                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                    <img src="//www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" id="img1" alt="" />
                                </div>
                                <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                            </div>

                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1"><?php //echo lang('name'); ?></label>
                        <div class="nameClass"></div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1"><?php //echo lang('email'); ?></label>
                        <div class="emailClass"></div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1"><?php //echo lang('address'); ?></label>
                        <div class="addressClass"></div>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1"><?php //echo lang('phone'); ?></label>
                        <div class="phoneClass"></div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1"><?php //echo lang('department'); ?></label>
                        <div class="departmentClass"></div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1"><?php //echo lang('profile'); ?></label>
                        <div class="profileClass"></div>
                    </div>


                </form>

            </div>
        </div> /.modal-content 
    </div>/.modal-dialog 
</div> -->






<script src="common/js/codearistos.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $(".table").on("click", ".editbutton", function () {
           
            // Get the record's ID via attribute  
            var iid = $(this).attr('data-id');//alert(iid);
          
           // $("#img").attr("src", "uploads/cardiology-patient-icon-vector-6244713.jpg");
            $('#editCollectorForm').trigger("reset");
            $.ajax({
                url: 'collector/editCollectorByJason?id=' + iid,
                method: 'GET',
                data: '',
                dataType: 'json',
            }).success(function (response) {
                // Populate the form fields with the data returned from server
                $('#editCollectorForm').find('[name="id"]').val(response.collector.collection_id).end()
                $('#editCollectorForm').find('[name="name"]').val(response.collector.collector_name).end()
                $('#editCollectorForm').find('[name="phone"]').val(response.collector.collector_ph).end()
                $('#editCollectorForm').find('[name="collector_address"]').val(response.collector.collector_address).end()
                $('#editCollectorForm').find('[name="contact_person"]').val(response.collector.contact_person).end()
                $('#editCollectorForm').find('[name="commission_percentage"]').val(response.collector.commission_percentage).end()
        
                $('#myModal2').modal('show');

            });
        });
    });
</script>







 <script>
    $(document).ready(function () {
        var table = $('#editable-sample').DataTable({
            responsive: true,

            "processing": true,
            "serverSide": true,
            "searchable": true,
            "ajax": {
                url: "collector/getCollector",
                type: 'POST',
            },
            scroller: {
                loadingIndicator: true
            },

            dom: "<'row'<'col-sm-3'l><'col-sm-5 text-center'B><'col-sm-4'f>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5',
                {
                    extend: 'print',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5, 6],
                    }
                },
            ],

            aLengthMenu: [
                [10, 25, 50, 100, -1],
                [10, 25, 50, 100, "All"]
            ],
            iDisplayLength: 100,
            "order": [[0, "desc"]],

            "language": {
                "lengthMenu": "_MENU_",
                search: "_INPUT_",
                searchPlaceholder: " Search..."
            }
        });
        table.buttons().container().appendTo('.custom_buttons');
    });
</script>




<!-- <script>
    $(document).ready(function () {
        $(".flashmessage").delay(3000).fadeOut(100);
    });
</script> -->

