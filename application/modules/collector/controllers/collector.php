<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Collector extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('Ion_auth');
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->model('collector_model');
        $this->load->library('upload');
        $language = $this->db->get('settings')->row()->language;
        $this->lang->load('system_syntax', $language);
        $this->load->model('ion_auth_model');
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        }
        if (!$this->ion_auth->in_group(array('admin', 'Accountant', 'Doctor'))) {
            redirect('home/permission');
        }
    }

    public function index() {
        

        $data['collector'] = $this->collector_model->getCollector();
        //$data['departments'] = $this->department_model->getDepartment();
        $this->load->view('home/dashboard'); // just the header file
        $this->load->view('collector', $data);
        $this->load->view('home/footer'); // just the header file
    }

    public function addNew() {

        $id = $this->input->post('id');
        $name = $this->input->post('name');
        $phone = $this->input->post('phone');
        $collector_address = $this->input->post('collector_address');
        $contact_person = $this->input->post('contact_person');
        $commission_percentage = $this->input->post('commission_percentage');

        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        // Validating Name Field
        $this->form_validation->set_rules('name', 'Name', 'trim|required|min_length[1]|max_length[100]|xss_clean');
        $this->form_validation->set_rules('phone', 'Phone', 'trim|required|min_length[5]|max_length[10]|xss_clean');
        // Validating Address Field   
        $this->form_validation->set_rules('collector_address', 'Address', 'trim|required|min_length[1]|max_length[500]|xss_clean');
        $this->form_validation->set_rules('contact_person', 'Contact Person', 'trim|min_length[1]|max_length[20]|xss_clean');
        // Validating Phone Field           
        $this->form_validation->set_rules('commission_percentage', 'Commission Percentage', 'trim|min_length[1]|max_length[50]|xss_clean');
        // Validating Department Field   
    


        if ($this->form_validation->run() == FALSE) {
            if (!empty($id)) {
                $data = array();
                $data['collector'] = $this->collector_model->getCollectorById($id);
                $this->load->view('home/dashboard'); // just the header file
                $this->load->view('add_new', $data);
                $this->load->view('home/footer'); // just the footer file
            } else {
                $data = array();
                $data['setval'] = 'setval';
                //$data['departments'] = $this->department_model->getDepartment();
                $this->load->view('home/dashboard'); // just the header file
                $this->load->view('add_new', $data);
                $this->load->view('home/footer'); // just the header file
            }
        } else {
            $data = array(
                'collector_name' => $name,
                'collector_ph' => $phone,
                'collector_address' => $collector_address,
                'contact_person' => $contact_person,
                'commission_percentage' => $commission_percentage
            );
            if (empty($id)) {     // Adding New
                $this->collector_model->insertCollector($data);

                
            } else { // Updating 
                $this->collector_model->updateCollector($id, $data);
                $this->session->set_flashdata('feedback', 'Updated');
            }
            // Loading View
            redirect('collector');
        }
    }

    function editCollector() {
        $data = array();
       // $data['departments'] = $this->department_model->getDepartment();
        $id = $this->input->get('collection_id');
        $data['collector'] = $this->collector_model->getcollectorById($id);
        $this->load->view('home/dashboard'); // just the header file
        $this->load->view('add_new', $data);
        $this->load->view('home/footer'); // just the footer file
    }

    function details() {

        $data = array();

        if ($this->ion_auth->in_group(array('Doctor'))) {
            $doctor_ion_id = $this->ion_auth->get_user_id();
            $id = $this->doctor_model->getDoctorByIonUserId($doctor_ion_id)->id;
        } else {
            redirect('home');
        }


        $data['doctor'] = $this->doctor_model->getDoctorById($id);
        $data['todays_appointments'] = $this->appointment_model->getAppointmentByDoctorByToday($id);
        $data['appointments'] = $this->appointment_model->getAppointmentByDoctor($id);
        $data['patients'] = $this->patient_model->getPatient();
        $data['appointment_patients'] = $this->patient->getPatientByAppointmentByDctorId($id);
        //$data['doctors'] = $this->doctor_model->getDoctor();
        $data['collector'] = $this->doctor_model->getCollector();
        $data['prescriptions'] = $this->prescription_model->getPrescriptionByDoctorId($id);
        $data['holidays'] = $this->schedule_model->getHolidaysByDoctor($id);
        $data['schedules'] = $this->schedule_model->getScheduleByDoctor($id);



        $this->load->view('home/dashboard'); // just the header file
        $this->load->view('details', $data);
        $this->load->view('home/footer'); // just the footer file
    }

    // function editDoctorByJason() {
    //     $id = $this->input->get('id');
    //     $data['doctor'] = $this->doctor_model->getDoctorById($id);
    //     echo json_encode($data);
    // }
    function editCollectorByJason() {
        $id = $this->input->get('id');
        $data['collector'] = $this->collector_model->getcollectorById($id);
        echo json_encode($data);
    }

    function delete() {
        $data = array();
        $id = $this->input->get('id');
        $user_data = $this->db->get_where('collection_center_master', array('id' => $id))->row();
        $path = $user_data->img_url;

        if (!empty($path)) {
            unlink($path);
        }
        $ion_user_id = $user_data->ion_user_id;
        $this->db->where('id', $ion_user_id);
        $this->db->delete('users');
        $this->doctor_model->delete($id);
        $this->session->set_flashdata('feedback', 'Deleted');
        redirect('doctor');
    }

    function getCollector() {
        $requestData = $_REQUEST;
        $start = $requestData['start'];
        $limit = $requestData['length'];
        $search = $this->input->post('search')['value'];

        if ($limit == -1) {
            if (!empty($search)) {
                $data['collector'] = $this->collector_model->getCollectorBysearch($search);
            } else {
                $data['collector'] = $this->collector_model->getCollector();
            }
        } else {
            if (!empty($search)) {
                $data['collector'] = $this->collector_model->getCollectorByLimitBySearch($limit, $start, $search);
            } else {
                $data['collector'] = $this->collector_model->getCollectorByLimit($limit, $start);
            }
        }
        //  $data['doctors'] = $this->doctor_model->getDoctor();

        foreach ($data['collector'] as $collector) {
            if ($this->ion_auth->in_group(array('admin', 'Accountant', 'Receptionist'))) {
                $options1 = '<a type="button" class="btn btn-info btn-xs btn_width editbutton" title="' . lang('edit') . '" data-toggle="modal" data-id="' . $collector->collection_id . '"><i class="fa fa-edit"> </i> ' . lang('edit') . '</a>';
                 //  $options1 = '<a class="btn btn-info btn-xs btn_width" title="' . lang('edit') . '" href="collector/editCollector?collection_id='.$collector->collection_id.'"><i class="fa fa-edit"> </i> ' . lang('edit') . '</a>';
            }
            $options2 = '';
            if ($this->ion_auth->in_group(array('admin', 'Accountant', 'Receptionist'))) {
                $options3 = '<a class="btn btn-info btn-xs btn_width delete_button" title="' . lang('delete') . '" href="collector/delete?collection_id=' . $collector->collection_id . '" onclick="return confirm(\'Are you sure you want to delete this item?\');"><i class="fa fa-trash-o"> </i> ' . lang('delete') . '</a>';
            }



            if ($this->ion_auth->in_group(array('admin', 'Accountant', 'Receptionist'))) {
                $options4 = '';
                $options5 = '';
                $options6 = '';
            }

            $info[] = array(
                $collector->collection_id,
                $collector->collector_name,
                $collector->collector_ph,
                $collector->collector_address,
                $collector->contact_person,
                $collector->commission_percentage,
                //  $options1 . ' ' . $options2 . ' ' . $options3,
                $options6.' '.$options1 . ' ' . $options2 . ' ' . $options4 . ' ' . $options5 . ' ' . $options3,
                    //  $options2
            );
        }

        if (!empty($data['collector'])) {
            $output = array(
                "draw" => intval($requestData['draw']),
                "recordsTotal" => $this->db->get('collection_center_master')->num_rows(),
                "recordsFiltered" => $this->db->get('collection_center_master')->num_rows(),
                "data" => $info
            );
        } else {
            $output = array(
                // "draw" => 1,
                "recordsTotal" => 0,
                "recordsFiltered" => 0,
                "data" => []
            );
        }

        echo json_encode($output);
    }

}

/* End of file doctor.php */
/* Location: ./application/modules/doctor/controllers/doctor.php */